#include "stdafx.h"

#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>

using namespace kartong;
using namespace helium;

BoundingBox::BoundingBox(GameObject* p_Owner)
{


	m_Owner = p_Owner;
	m_CheckRadius = 50;
	m_Position = Vector3(0.0f, 0.0f, 0.0f);
	m_Volume = Vector3(0.0f, 0.0f, 0.0f);
	m_Collided = false;
	m_Drawable = false;
	m_Drawing = false;
	Debug::write(EDebugLevel::Info, "BoundingBox Created with Owner: " + m_Owner->GetName());

	m_Yaw = 0.0f;
	m_Pitch = 0.0f;


};
BoundingBox::~BoundingBox()
{

};

float BoundingBox::GetCheckRadius()
{
	return m_CheckRadius;
}

void BoundingBox::SetCheckRadius(float p_Radius)
{
	m_CheckRadius = p_Radius;
}
Vector3 BoundingBox::GetPosition()
{
	return m_Position;
};
Vector3 BoundingBox::GetVolume()
{
	return m_Volume;
};

Vector3 BoundingBox::GetHalfVolume()
{
	return m_HalfVol;
};

void BoundingBox::SetPosition(Vector3 p_Position)
{
	m_Position = p_Position;
};
void BoundingBox::SetVolume(Vector3 p_Volume)
{
	if (p_Volume.m_x == 0 || p_Volume.m_y == 0 || p_Volume.m_z == 0)
	{
		Debug::write(EDebugLevel::Error, "Trying to set volume to zero, setting it to 1.0f instead");
		m_Volume = Vector3(1.0f, 1.0f, 1.0f);
		m_HalfVol = m_Volume / 2;
	}
	m_Volume = p_Volume;
	m_HalfVol = m_Volume / 2;

};
Matrix4 BoundingBox::GetTransform()
{
	return m_Transform;
};

bool BoundingBox::CheckIntersection(BoundingBox& p_Box)
{
	/*if (!CheckIfInRange(p_Box.GetPosition()))
		return false;*/
	if (p_Box.GetVolume().m_x == 0 || p_Box.GetVolume().m_x == 0 || p_Box.GetVolume().m_x == 0)
	{
		Debug::write(EDebugLevel::Error, "The BondingBox entered has a volume of zero");
		return false;
	}
	Vector3 otherpos = p_Box.GetPosition();
	Vector3 otherhalf = p_Box.GetHalfVolume();

	/*if (ContainsPoint(p_Box.GetPosition()))
		return true;*/
	if (ContainsPoint(Vector3(otherpos.m_x - otherhalf.m_x, otherpos.m_y - otherhalf.m_y, otherpos.m_z + otherhalf.m_z)) ||
		ContainsPoint(Vector3(otherpos.m_x - otherhalf.m_x, otherpos.m_y - otherhalf.m_y, otherpos.m_z - otherhalf.m_z)) ||
		ContainsPoint(Vector3(otherpos.m_x + otherhalf.m_x, otherpos.m_y - otherhalf.m_y, otherpos.m_z - otherhalf.m_z)) ||
		ContainsPoint(Vector3(otherpos.m_x + otherhalf.m_x, otherpos.m_y - otherhalf.m_y, otherpos.m_z + otherhalf.m_z)) ||
		ContainsPoint(Vector3(otherpos.m_x - otherhalf.m_x, otherpos.m_y + otherhalf.m_y, otherpos.m_z + otherhalf.m_z)) ||
		ContainsPoint(Vector3(otherpos.m_x - otherhalf.m_x, otherpos.m_y + otherhalf.m_y, otherpos.m_z - otherhalf.m_z)) ||
		ContainsPoint(Vector3(otherpos.m_x + otherhalf.m_x, otherpos.m_y + otherhalf.m_y, otherpos.m_z - otherhalf.m_z)) ||
		ContainsPoint(Vector3(otherpos.m_x + otherhalf.m_x, otherpos.m_y + otherhalf.m_y, otherpos.m_z + otherhalf.m_z)))
		return true;
	if (p_Box.ContainsPoint(Vector3(m_Position.m_x - m_HalfVol.m_x, m_Position.m_y - m_HalfVol.m_y, m_Position.m_z + m_HalfVol.m_z)) ||
		p_Box.ContainsPoint(Vector3(m_Position.m_x - m_HalfVol.m_x, m_Position.m_y - m_HalfVol.m_y, m_Position.m_z - m_HalfVol.m_z)) ||
		p_Box.ContainsPoint(Vector3(m_Position.m_x + m_HalfVol.m_x, m_Position.m_y - m_HalfVol.m_y, m_Position.m_z - m_HalfVol.m_z)) ||
		p_Box.ContainsPoint(Vector3(m_Position.m_x + m_HalfVol.m_x, m_Position.m_y - m_HalfVol.m_y, m_Position.m_z + m_HalfVol.m_z)) ||
		p_Box.ContainsPoint(Vector3(m_Position.m_x - m_HalfVol.m_x, m_Position.m_y + m_HalfVol.m_y, m_Position.m_z + m_HalfVol.m_z)) ||
		p_Box.ContainsPoint(Vector3(m_Position.m_x - m_HalfVol.m_x, m_Position.m_y + m_HalfVol.m_y, m_Position.m_z - m_HalfVol.m_z)) ||
		p_Box.ContainsPoint(Vector3(m_Position.m_x + m_HalfVol.m_x, m_Position.m_y + m_HalfVol.m_y, m_Position.m_z - m_HalfVol.m_z)) ||
		p_Box.ContainsPoint(Vector3(m_Position.m_x + m_HalfVol.m_x, m_Position.m_y + m_HalfVol.m_y, m_Position.m_z + m_HalfVol.m_z)))
		return true;
	return false;
};
bool BoundingBox::CheckRayIntersection(Vector3 p_Start, Vector3 p_Direction)
{
	Vector3 vecV1 = m_Position - p_Start;
	float fD = p_Direction.dot(vecV1);
	if (fD < 0.0f)
		return false;
	Vector3 Closest = (p_Start + (p_Direction * fD));

	return ContainsPoint(Closest);
};

bool BoundingBox::ContainsPoint(Vector3 p_Pos)
{

	float fix = 0.01f;
	if (m_Position.m_x - m_HalfVol.m_x + fix <= p_Pos.m_x && m_Position.m_x + m_HalfVol.m_x - fix >= p_Pos.m_x &&
		m_Position.m_y - m_HalfVol.m_y + fix <= p_Pos.m_y && m_Position.m_y + m_HalfVol.m_y - fix >= p_Pos.m_y &&
		m_Position.m_z - m_HalfVol.m_z + fix <= p_Pos.m_z && m_Position.m_z + m_HalfVol.m_z - fix >= p_Pos.m_z)
		return true;

	return false;
}

bool BoundingBox::CheckIfInRange(const Vector3 p_pos)
{
	if ((Math::sqrt(
		(p_pos.m_x - m_Position.m_x * p_pos.m_x - m_Position.m_x) +
		(p_pos.m_y - m_Position.m_y * p_pos.m_y - m_Position.m_x) +
		(p_pos.m_z - m_Position.m_z * p_pos.m_z - m_Position.m_x)) <= m_CheckRadius))
		return true;
	return false;
}

void BoundingBox::Draw(Matrix4 p_view, Matrix4 p_projection)
{
	if (!m_Drawing)
		return;

	m_Drawbox.world = Matrix4::translation(m_Position);
	//p_view = p_view * Matrix4::rotation(0, -m_Pitch, 0, 0.5f);
	//p_view = m_Drawbox.world * Matrix4::translation(m_Position) * Matrix4::rotation(0, -m_Pitch, 0, 1.0f);

	//p_view = p_view * Matrix4::rotation(m_Yaw, m_Pitch, 0, 0.5f);

	//Debug::write(EDebugLevel::Info, std::to_string(m_Position.m_x));
	m_render_system->select_shader(m_Drawbox.shader);
	m_render_system->select_texture(m_Drawbox.texture);
	m_render_system->select_sampler_state(m_Drawbox.sampler);
	m_render_system->select_vertex_format(m_Drawbox.format);
	m_render_system->select_vertex_buffer(m_Drawbox.buffer);
	m_render_system->select_depth_state(m_Drawbox.depth);
	m_render_system->select_rasterizer_state(m_Drawbox.raster);
	m_render_system->select_blend_state(m_Drawbox.blend);
	m_render_system->set_shader_constant_matrix4("projection", p_projection);
	m_render_system->set_shader_constant_matrix4("view", p_view);
	m_render_system->set_shader_constant_matrix4("world", m_Drawbox.world);
	m_render_system->apply();
	m_render_system->draw(helium::system::EDrawMode::TriangleList, 0, m_Drawbox.drawcount);

	//Debug::write(EDebugLevel::Info, "Drawing at pos: " + std::to_string(m_Position.m_x) + "," + std::to_string(m_Position.m_y) + "," + std::to_string(m_Position.m_z));
	//Draw
};

void BoundingBox::MakeDrawable()
{
	if (m_render_system == nullptr)
		m_render_system = helium::ServiceLocator<helium::system::RenderSystem>::get_service();


	m_Drawbox = { 0 };

	m_shader = helium::resource::Shader::create("../data/txt/cube.shader.txt");
	m_Drawbox.shader = m_render_system->create_shader(
		m_shader->get_vertex_source().c_str(),
		m_shader->get_pixel_source().c_str());

	m_format = helium::resource::VertexFormat::create("../data/txt/cube.format.txt");
	m_Drawbox.format = m_render_system->create_vertex_format(
		const_cast<helium::system::VertexFormatDesc*>(m_format->get_desc()),
		m_format->get_count(),
		m_Drawbox.shader);

	m_texture = helium::resource::Texture::create("../data/hitbox.png");
	m_Drawbox.texture = m_render_system->create_texture(
		m_texture->get_format(),
		m_texture->get_type(),
		m_texture->get_width(),
		m_texture->get_height(),
		m_texture->get_depth(),
		m_texture->get_pixel_data());

	m_sampler = helium::resource::Sampler::create("../data/skybox/skybox.sampler.txt");
	m_Drawbox.sampler = m_render_system->create_sampler_state(
		m_sampler->get_filter_mode(),
		m_sampler->get_addr_u(),
		m_sampler->get_addr_v(),
		m_sampler->get_addr_w());

	m_Drawbox.raster = m_render_system->create_rasterizer_state(helium::system::EFillMode::Wireframe, helium::system::ECullMode::None);
	//m_Drawbox.depth = m_render_system->create_depth_state(false, false, helium::system::EDepthMode::Less);

	m_Drawbox.blend = m_render_system->create_blend_state(
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendOp::Add, system::EBlendOp::Add);

	m_Drawbox.world.identity();
	m_Drawbox.material.ambient = 0.1f;
	m_Drawbox.material.diffuse = 0.5f;
	m_Drawbox.material.specular = 0.5f;
	m_Drawbox.material.shininess = 32.0f;

	m_Drawbox.drawcount = 36;
	//Vertex* vertices = new Vertex[skybox.drawcount];
	float x = m_HalfVol.m_x;
	float y = m_HalfVol.m_y;
	float z = m_HalfVol.m_z;

	struct Vertex
	{
		Vector3 position;
		Vector2 texcoord;
		Vector3 normal;
	}
	vertices[] =
	{
		{ Vector3(-x, y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(x, y, -z), Vector2(1.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(x, -y, -z), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(x, -y, -z), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(-x, -y, -z), Vector2(0.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(-x, y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) },

		// right
		{ Vector3(x, y, -z), Vector2(0.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(x, y, z), Vector2(1.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(x, -y, z), Vector2(1.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(x, -y, z), Vector2(1.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(x, -y, -z), Vector2(0.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(x, y, -z), Vector2(0.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f) },

		// back
		{ Vector3(x, y, z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(-x, y, z), Vector2(1.0f, 0.0f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(-x, -y, z), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(-x, -y, z), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(x, -y, z), Vector2(0.0f, 1.0f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(x, y, z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, 1.0f) },

		// left
		{ Vector3(-x, y, z), Vector2(0.0f, 0.0f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-x, y, -z), Vector2(1.0f, 0.0f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-x, -y, -z), Vector2(1.0f, 1.0f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-x, -y, -z), Vector2(1.0f, 1.0f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-x, -y, z), Vector2(0.0f, 1.0f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-x, y, z), Vector2(0.0f, 0.0f), Vector3(-1.0f, 0.0f, 0.0f) },

		// top
		{ Vector3(-x, y, z), Vector2(0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(x, y, z), Vector2(1.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(x, y, -z), Vector2(1.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(x, y, -z), Vector2(1.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(-x, y, -z), Vector2(0.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(-x, y, z), Vector2(0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f) },

		// bottom
		{ Vector3(-x, -y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(x, -y, -z), Vector2(1.0f, 0.0f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(x, -y, z), Vector2(1.0f, 1.0f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(x, -y, z), Vector2(1.0f, 1.0f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(-x, -y, z), Vector2(0.0f, 1.0f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(-x, -y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, -1.0f, 0.0f) },


	};
	m_Drawbox.buffer = m_render_system->create_vertex_buffer(
		helium::system::EBufferMode::Static,
		vertices,
		sizeof(Vertex),
		m_Drawbox.drawcount);

	m_Drawbox.raster = m_render_system->create_rasterizer_state(
		system::EFillMode::Wireframe, system::ECullMode::None);

	m_Drawbox.depth = m_render_system->create_depth_state(
		true, true, system::EDepthMode::Less);

	m_Drawbox.world = m_Drawbox.world * Matrix4::translation(Vector3(0.0f, 0.0f, 0.0f));
	m_Drawable = true;
};

void BoundingBox::DisableDraw()
{
	m_Drawing = false;
};

void BoundingBox::SetDraw()
{
	m_Drawing = true;
};

bool BoundingBox::IsDrawing()
{
	return m_Drawing;
};

bool BoundingBox::IsDrawable()
{
	return m_Drawable;
};

void BoundingBox::SetRotationData(Matrix4 p_Rot, float p_Yaw, float p_Pitch)
{
	m_Rot = p_Rot;
	m_Yaw = p_Yaw;
	m_Pitch = p_Pitch;
};

bool BoundingBox::HasCollided()
{
	return m_Collided;
};
void BoundingBox::SetCollided(const bool& p_Collided)
{
	m_Collided = p_Collided;
};
bool BoundingBox::BeenShot()
{
	return m_BeenShot;
};
void BoundingBox::SetShot(const bool& p_BeenShot)
{
	m_BeenShot = p_BeenShot;
};

