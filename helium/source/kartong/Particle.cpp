#include "stdafx.h"
#include <kartong/Particle.hpp>

#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <kartong/Randomizer.hpp>

#ifndef RandomSpreadSmall 
#define RandomSpreadSmall 0.2
#endif

#ifndef RandomSpreadBig 
#define RandomSpreadBig 0.5
#endif

#ifndef MaxAllowedLifetime 
#define MaxAllowedLifetime 4.0
#endif

using namespace kartong;

Particle::Particle(const std::string &texture, const Vector3 &position, const Vector3 &size, const float &speed, const float &lifetime, EDirection direction) {
	Setposition(position);
	m_Transform.set_position(m_position);
	Load(texture, size);
	m_speed = speed;
	if (lifetime == 0.0f) {
		m_lifetime = (float)Randomizer::GetRandomDouble(RandomSpreadSmall, MaxAllowedLifetime);
	}
	else if (lifetime > 4.0f) {
		m_lifetime = 4.0f;
	}
	m_lifetime = lifetime;
	m_directiontype = direction;
	m_destroy = false;
}

Particle::~Particle() {

}

void Particle::Update(const Vector3 &playerposition, const float &deltatime) {
	m_lifetime -= deltatime;
	//if (m_lifetime <= 0.0f) {
	//	m_destroy = true;
	//}
	LookAtPlayer(playerposition);
	m_Transform.set_rotation(Vector3(1.0f, 0.0f, 0.0f));
}

void Particle::Draw(scene::Camera* p_Cam) {
	m_Transform.recalculate();
	m_element.world = m_Transform.get_transform();

	

	m_render_system->select_shader(m_element.shader);
	m_render_system->select_texture(m_element.texture);
	m_render_system->select_sampler_state(m_element.sampler);
	m_render_system->select_vertex_format(m_element.format);
	m_render_system->select_vertex_buffer(m_element.buffer);
	m_render_system->select_depth_state(m_element.depth);
	m_render_system->select_rasterizer_state(m_element.raster);
	m_render_system->select_blend_state(m_element.blend);
	m_render_system->set_shader_constant_matrix4("projection", p_Cam->get_projection());
	m_render_system->set_shader_constant_matrix4("view", p_Cam->get_view());
	m_render_system->set_shader_constant_matrix4("world", m_element.world);
	m_render_system->set_shader_constant_vector3("direction", sun.direction);
	m_render_system->set_shader_constant_vector3("diffuse", sun.diffuse);
	m_render_system->set_shader_constant_vector3("ambient", sun.ambient);
	m_render_system->set_shader_constant_vector3("eye", p_Cam->get_position());
	m_render_system->set_shader_constant_raw("material", &m_element.material, sizeof(m_element.material));
	m_render_system->apply();
	m_render_system->draw(helium::system::EDrawMode::TriangleList, 0, m_element.drawcount);
}

bool Particle::HasTimedout() {
	return m_destroy;
}

void Particle::Setposition(const Vector3 &pos) {
	m_position = pos;
}

void Particle::Load(const std::string &filename, const Vector3 &size) {
	//m_Scale = Vector3(1.0f, 1.0f, 1.0f);
	
	sun.direction = Vector3(-1.0f, -1.0f, 1.0);
	sun.direction.normalize();
	sun.diffuse = Vector3(1.0f, 1.0f, 1.0f);
	sun.ambient = Vector3(1.0f, 1.0f, 1.0f);
	sun.eye = Vector3(0.0f, 0.0f, -6.0f);
	sun.pad0 = sun.pad1 = sun.pad2 = sun.pad3 = 1.0f;
	//m_Volume = p_Vol;
	//m_Rotation = Vector3(0.0f, 0.0f, 0.0f);
	m_render_system = helium::ServiceLocator<helium::system::RenderSystem>::get_service();

	m_element = { 0 };

	m_shader = helium::resource::Shader::create("../data/txt/cube.shader.txt");
	m_element.shader = m_render_system->create_shader(
		m_shader->get_vertex_source().c_str(),
		m_shader->get_pixel_source().c_str());

	m_format = helium::resource::VertexFormat::create("../data/txt/cube.format.txt");
	m_element.format = m_render_system->create_vertex_format(
		const_cast<helium::system::VertexFormatDesc*>(m_format->get_desc()),
		m_format->get_count(),
		m_element.shader);

	m_texture = helium::resource::Texture::create(filename);
	m_element.texture = m_render_system->create_texture(
		m_texture->get_format(),
		m_texture->get_type(),
		m_texture->get_width(),
		m_texture->get_height(),
		m_texture->get_depth(),
		m_texture->get_pixel_data());

	m_sampler = helium::resource::Sampler::create("../data/txt/cube.sampler.txt");
	m_element.sampler = m_render_system->create_sampler_state(
		m_sampler->get_filter_mode(),
		m_sampler->get_addr_u(),
		m_sampler->get_addr_v(),
		m_sampler->get_addr_w());



	m_element.blend = m_render_system->create_blend_state(
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendOp::Add, system::EBlendOp::Add);

	m_element.world.identity();
	m_element.material.ambient = 0.1f;
	m_element.material.diffuse = 0.5f;
	m_element.material.specular = 0.5f;
	m_element.material.shininess = 32.0f;

	m_element.drawcount = 6;

	float x = size.m_x / 2.0f;
	float y = size.m_y / 2.0f;
	float z = size.m_z / 2.0f;

	struct Vertex
	{
		Vector3 position;
		Vector2 texcoord;
		Vector3 normal;
	}

	vertices[] =
	{
		{ Vector3(-x, y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(x, y, -z), Vector2(1.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(x, -y, -z), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(x, -y, -z), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(-x, -y, -z), Vector2(0.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(-x, y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) },

	};
	m_element.buffer = m_render_system->create_vertex_buffer(
		helium::system::EBufferMode::Static,
		vertices,
		sizeof(Vertex),
		m_element.drawcount);

	m_element.raster = m_render_system->create_rasterizer_state(
		system::EFillMode::Solid, system::ECullMode::None);

	m_element.depth = m_render_system->create_depth_state(
		true, true, system::EDepthMode::Less);

	m_element.world = m_element.world * Matrix4::translation(m_position);

}

void Particle::LookAtPlayer(const Vector3 &playerpos) {
	Vector3 delta = playerpos;
	delta -= m_position;
}

void Particle::SetDirection(EDirection dir) {
	float random = (float)Randomizer::GetRandomDouble(-RandomSpreadSmall, RandomSpreadSmall);
	switch (dir) {
	case UP:
		m_direction = Vector3(random, 1 + random, 0);
		break;
	case DOWN:
		m_direction = Vector3(random, -1 + random, 0);
		break;
	case LEFT:
		m_direction = Vector3(-1 + random, random, 0);
		break;
	case RIGHT:
		m_direction = Vector3(1 + random, random, 0);
		break;
	case RANDOM:
		random = (float)Randomizer::GetRandomDouble(-RandomSpreadBig, RandomSpreadBig);
		m_direction = Vector3(random, random, 0);
		break;
	}
}