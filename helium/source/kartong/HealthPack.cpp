#include "stdafx.h"

#include <kartong/HealthPack.hpp>
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>

using namespace kartong;

HealthPack::HealthPack()
{
	m_CollisionManager = helium::ServiceLocator<kartong::CollisionManager>::get_service();
	SetTag(("Pickup"));
	SetName(("HealthPack"));
	SetType(ObjectType::INTERACTIVE);
	SetPhysicsType(PhysicsType::PASS);

	Vector3 Volume = Vector3(1.0f, 1.0f, 1.0f);
	Vector3 BotVol = Vector3(1.5f, 0.3f, 1.5f);

	m_Cube = Cube::Create("hppack.png", Volume);
	m_Cube->SetPosition(m_Position);
	m_Bot = Cube::Create("hppack_bot.png", BotVol);
	m_Bot->SetPosition(Vector3(m_Position.m_x, m_Position.m_y - 1.7f, m_Position.m_z));

	m_Bounds = m_CollisionManager->CreateNewBox(this, m_Position, Volume, 50);
	m_Bounds->SetVolume(Volume);
	m_Bounds->SetPosition(m_Position);

	m_Active = true;
	m_CountTimer = 0.0f;
	m_ResetWhen = 5.0f;
	m_Scale = 1.0f;

};
HealthPack::~HealthPack()
{

};


HealthPack::Ptr HealthPack::Create()
{
	return HealthPack::Ptr(new HealthPack());

};

void HealthPack::Initialize()
{

};
void HealthPack::Update(const float& p_Deltatime)
{
	m_Bot->SetPosition(Vector3(m_Position.m_x, m_Position.m_y - 1.7f, m_Position.m_z));
	m_Bot->Update(p_Deltatime);
	m_Bounds->SetPosition(m_Position);

	if (!m_Active)
	{
		Debug::write(EDebugLevel::Info, std::to_string(m_CountTimer));
		m_CountTimer += p_Deltatime;
		if (m_CountTimer >= m_ResetWhen)
		{
			m_Active = true;
			m_Scaling = true;
			m_CountTimer = 0.0f;
		}
	}
	if (m_Scaling)
	{
		m_Scale += 1.45 * p_Deltatime;
		if (m_Scale >= 1.0f)
		{
			m_Scale = 1.0f;
			m_Scaling = false;
		}
	}
	

	m_RotateTime += p_Deltatime;

	m_Cube->SetRotation(Vector3(0.0f, m_RotateTime, m_RotateTime));
	m_Cube->SetPosition(m_Position);
	m_Cube->SetScale(Vector3(m_Scale, m_Scale, m_Scale));
	m_Cube->Update(p_Deltatime);
	//m_Bot->SetRotation(Vector3(0.0f, 0.0f, 0.0f), m_Time * 1.0f);

};
void HealthPack::Draw(scene::Camera* m_Cam)
{
	m_Bot->Draw(m_Cam);
	if (m_Active)
		m_Cube->Draw(m_Cam);

};

float HealthPack::GetHealth()
{
	return 50.0f;
};

void HealthPack::SetActive(const bool& p_Active)
{
	m_Active = p_Active;
};
bool HealthPack::IsActive()
{
	return m_Active;
};

void HealthPack::Interact()
{
	m_Active = false;
	m_Scale = 0.0f;
};

//bool HealthPack::RequirementMet()
//{
//	return m_Active;
//};