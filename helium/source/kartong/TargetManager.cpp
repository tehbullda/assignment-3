#pragma once

#include "stdafx.h"
#include <kartong/TargetManager.hpp>
#include <helium/service_locator.hpp>
#include <helium/system/audio_system.hpp>

using namespace kartong;

TargetManager::TargetManager()
{
	m_audio_system = ServiceLocator<system::AudioSystem>::get_service();


	m_Timer = 0.0f;
	m_CurrentWave = 0.0f;
	for (int i = 0; i < 10; i++)
	{
		m_Targets.push_back(new ShootableTestDummy());
	}
	m_Wave1 = GUITexture::Create("wave1.png", Vector2(15.0f, 15.0f));
	m_Wave2 = GUITexture::Create("wave2.png", Vector2(15.0f, 15.0f));
	m_Wave3 = GUITexture::Create("wave3.png", Vector2(15.0f, 15.0f));
	m_audio_system->add_sound("../data/sound/VoiceTroll DSS02_80_2.wav", system::ChannelType::SOUND, system::EAudioType::Buffer);
};
TargetManager::~TargetManager()
{
	for (int i = 0; i < m_Targets.size(); i++)
	{
		if (m_Targets[i])
		{
			delete m_Targets[i];
			m_Targets[i] = nullptr;
		}
	}
	m_Targets.clear();
};

TargetManager::Ptr TargetManager::Create()
{
	return TargetManager::Ptr(new TargetManager());
};

void TargetManager::Initialize()
{
	for (int i = 0; i < m_Targets.size(); i++)
	{
		m_Targets.at(i)->Initialize();
	}
	InitializeWave(m_CurrentWave);
};
void TargetManager::Update(const float& p_Deltatime)
{
	m_Timer += p_Deltatime;
	for (int i = 0; i < m_Targets.size(); i++)
	{
			m_Targets.at(i)->Update(p_Deltatime);

		if (m_Targets.at(i)->m_TargetHit && m_Targets.at(i)->m_Position.m_y >= -50.0f)
		{
			m_audio_system->play("../data/sound/VoiceTroll DSS02_80_2.wav", false);
			Debug::write(EDebugLevel::Info, "Target " + std::to_string(i) + " Shot!");
			m_Targets.at(i)->m_Position = Vector3(0.0f, -100.0f, 0.0f);
		}
	}




	for (int i = 0; i < m_Targets.size(); i++)
	{
		if (!m_Targets.at(i)->m_TargetHit)
			return;
	}
	m_CurrentWave += 1;
	InitializeWave(m_CurrentWave);
};
void TargetManager::Draw(scene::Camera* p_Camera)
{
	for (int i = 0; i < m_Targets.size(); i++)
	{
		m_Targets.at(i)->Draw(p_Camera);
	}
	if (m_CurrentWave == 0)
		m_Wave1->Draw();
	if (m_CurrentWave == 1)
		m_Wave2->Draw();
	if (m_CurrentWave == 2)
		m_Wave3->Draw();
};
void TargetManager::InitializeWave(const int& p_Wave)
{
	for (int i = 0; i < m_Targets.size(); i++)
	{
		m_Targets.at(i)->m_TargetHit = false;
	}


	if (p_Wave == 0)
	{
		m_Targets.at(0)->m_Position = Vector3(-10.0f, 8.0f, -45.0f);
		m_Targets.at(1)->m_Position = Vector3(0.0f, 8.0f, -45.0f);
		m_Targets.at(2)->m_Position = Vector3(10.0f, 8.0f, -45.0f);

		m_Targets.at(3)->m_Position = Vector3(-5.0f, 8.0f, -35.0f);
		m_Targets.at(4)->m_Position = Vector3(0.0f, 10.0f, -35.0f);
		m_Targets.at(5)->m_Position = Vector3(5.0f, 8.0f, -35.0f);

		m_Targets.at(6)->m_Position = Vector3(-15.0f, 14.0f, -15.0f);
		m_Targets.at(7)->m_Position = Vector3(0.0f, 14.0f, -15.0f);
		m_Targets.at(8)->m_Position = Vector3(15.0f, 14.0f, -15.0f);

		m_Targets.at(9)->m_Position = Vector3(0.0f, 8.0f, 0.0f);

	}
	else if (p_Wave == 1)
	{
		m_Targets.at(0)->m_Position = Vector3(10.0f, 8.0f, -23.0f);
		m_Targets.at(0)->SetMovementX(-15.0f, 15.0f);
		m_Targets.at(0)->SetMoveSpeedX(10.0f);
		m_Targets.at(1)->m_Position = Vector3(0.0f, 8.0f, -25.0f);
		m_Targets.at(2)->m_Position = Vector3(-10.0f, 8.0f, -28.0f);
		m_Targets.at(2)->SetMovementX(-15.0f, 15.0f);
		m_Targets.at(2)->SetMoveSpeedX(10.0f);

		m_Targets.at(3)->m_Position = Vector3(-5.0f, 8.0f, -0.0f);
		m_Targets.at(4)->m_Position = Vector3(0.0f, 8.0f, -0.0f);
		m_Targets.at(4)->SetMovementY(7.0f, 15.0f);
		m_Targets.at(4)->SetMoveSpeedY(15.0f);
		m_Targets.at(5)->m_Position = Vector3(5.0f, 8.0f, -0.0f);

		m_Targets.at(6)->m_Position = Vector3(-15.0f, 14.0f, 15.0f);
		m_Targets.at(7)->m_Position = Vector3(0.0f, 14.0f, 15.0f);
		m_Targets.at(8)->m_Position = Vector3(15.0f, 14.0f, 15.0f);

		m_Targets.at(9)->m_Position = Vector3(0.0f, 15.0f, 33.0f);
		m_Targets.at(9)->SetMovementX(-15.0f, 15.0f);
		m_Targets.at(9)->SetMoveSpeedX(10.0f);
	}
	else if (p_Wave == 2)
	{
		m_Targets.at(0)->m_Position = Vector3(-10.0f, 8.0f, 44.0f);
		m_Targets.at(0)->SetMovementX(-15.0f, 15.0f);
		m_Targets.at(0)->SetMovementY(7.0f, 15.0f);
		m_Targets.at(0)->SetMoveSpeedX(15.0f);
		m_Targets.at(0)->SetMoveSpeedY(5.0f);
		m_Targets.at(1)->m_Position = Vector3(0.0f, 8.0f, 45.0f);
		m_Targets.at(1)->SetMovementY(7.0f, 15.0f);
		m_Targets.at(1)->SetMoveSpeedY(15.0f);
		m_Targets.at(2)->m_Position = Vector3(10.0f, 8.0f, 46.0f);
		m_Targets.at(2)->SetMovementX(-15.0f, 15.0f);
		m_Targets.at(2)->SetMovementY(7.0f, 15.0f);
		m_Targets.at(2)->SetMoveSpeedX(15.0f);
		m_Targets.at(2)->SetMoveSpeedY(5.0f);
		m_Targets.at(2)->m_InvetredX = true;

		m_Targets.at(3)->m_Position = Vector3(-18.0f, 8.0f, -15.0f);
		m_Targets.at(3)->SetMovementY(3.0f, 17.0f);
		m_Targets.at(3)->SetMoveSpeedY(10.0f);
		m_Targets.at(4)->m_Position = Vector3(0.0f, 8.0f, -15.0f);
		m_Targets.at(4)->SetMovementX(-3.0f, 3.0f);
		m_Targets.at(4)->SetMoveSpeedX(10.0f);
		m_Targets.at(5)->m_Position = Vector3(18.0f, 8.0f, -15.0f);
		m_Targets.at(5)->SetMovementY(3.0f, 17.0f);
		m_Targets.at(5)->SetMoveSpeedY(10.0f);
		m_Targets.at(5)->m_InvetredY = true;

		m_Targets.at(6)->m_Position = Vector3(-15.0f, 14.0f, -15.0f);
		m_Targets.at(7)->m_Position = Vector3(0.0f, 14.0f, -15.0f);
		m_Targets.at(8)->m_Position = Vector3(15.0f, 14.0f, -15.0f);

		m_Targets.at(9)->m_Position = Vector3(0.0f, 8.0f, 0.0f);
		

	}
	else
	{
		Debug::write(EDebugLevel::Info, "Time: " + std::to_string(m_Timer));
		Debug::write(EDebugLevel::Info, "YO WON HAPPI HAPPI");
	}
};


