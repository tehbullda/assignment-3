#include "stdafx.h"

#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>


using namespace kartong;

Cube::Cube(std::string p_texture, Vector3 p_Vol)
{
	m_Scale = Vector3(1.0f, 1.0f, 1.0f);
	sun.direction = Vector3(-1.0f, -1.0f, 1.0);
	sun.direction.normalize();
	sun.diffuse = Vector3(1.0f, 1.0f, 1.0f);
	sun.ambient = Vector3(1.0f, 1.0f, 1.0f);
	sun.eye = Vector3(0.0f, 0.0f, -6.0f);
	sun.pad0 = sun.pad1 = sun.pad2 = sun.pad3 = 1.0f;

	m_Volume = p_Vol;
	
	m_render_system = helium::ServiceLocator<helium::system::RenderSystem>::get_service();

	m_Cube = { 0 };

	m_shader = helium::resource::Shader::create("../data/txt/cube.shader.txt");
	m_Cube.shader = m_render_system->create_shader(
		m_shader->get_vertex_source().c_str(),
		m_shader->get_pixel_source().c_str());

	m_format = helium::resource::VertexFormat::create("../data/txt/cube.format.txt");
	m_Cube.format = m_render_system->create_vertex_format(
		const_cast<helium::system::VertexFormatDesc*>(m_format->get_desc()),
		m_format->get_count(),
		m_Cube.shader);

	m_texture = helium::resource::Texture::create("../data/" + p_texture);
	m_Cube.texture = m_render_system->create_texture(
		m_texture->get_format(),
		m_texture->get_type(),
		m_texture->get_width(),
		m_texture->get_height(),
		m_texture->get_depth(),
		m_texture->get_pixel_data());

	m_sampler = helium::resource::Sampler::create("../data/txt/cube.sampler.txt");
	m_Cube.sampler = m_render_system->create_sampler_state(
		m_sampler->get_filter_mode(),
		m_sampler->get_addr_u(),
		m_sampler->get_addr_v(),
		m_sampler->get_addr_w());



	m_Cube.blend = m_render_system->create_blend_state(
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendOp::Add, system::EBlendOp::Add);

	m_Cube.world.identity();
	m_Cube.material.ambient = 0.1f;
	m_Cube.material.diffuse = 0.5f;
	m_Cube.material.specular = 0.5f;
	m_Cube.material.shininess = 32.0f;

	m_Cube.drawcount = 36;

	float x = m_Volume.m_x / 2.0f;
	float y = m_Volume.m_y / 2.0f;
	float z = m_Volume.m_z / 2.0f;

	struct Vertex
	{
		Vector3 position;
		Vector2 texcoord;
		Vector3 normal;
	}

	vertices[] =
	{
		{ Vector3(-x, y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(x, y, -z), Vector2(1.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(x, -y, -z), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(x, -y, -z), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(-x, -y, -z), Vector2(0.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) },
		{ Vector3(-x, y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) },

		// right
		{ Vector3(x, y, -z), Vector2(0.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(x, y, z), Vector2(1.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(x, -y, z), Vector2(1.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(x, -y, z), Vector2(1.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(x, -y, -z), Vector2(0.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f) },
		{ Vector3(x, y, -z), Vector2(0.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f) },

		// back
		{ Vector3(x, y, z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(-x, y, z), Vector2(1.0f, 0.0f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(-x, -y, z), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(-x, -y, z), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(x, -y, z), Vector2(0.0f, 1.0f), Vector3(0.0f, 0.0f, 1.0f) },
		{ Vector3(x, y, z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, 1.0f) },

		// left
		{ Vector3(-x, y, z), Vector2(0.0f, 0.0f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-x, y, -z), Vector2(1.0f, 0.0f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-x, -y, -z), Vector2(1.0f, 1.0f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-x, -y, -z), Vector2(1.0f, 1.0f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-x, -y, z), Vector2(0.0f, 1.0f), Vector3(-1.0f, 0.0f, 0.0f) },
		{ Vector3(-x, y, z), Vector2(0.0f, 0.0f), Vector3(-1.0f, 0.0f, 0.0f) },

		// top
		{ Vector3(-x, y, z), Vector2(0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(x, y, z), Vector2(1.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(x, y, -z), Vector2(1.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(x, y, -z), Vector2(1.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(-x, y, -z), Vector2(0.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f) },
		{ Vector3(-x, y, z), Vector2(0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f) },

		// bottom
		{ Vector3(-x, -y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(x, -y, -z), Vector2(1.0f, 0.0f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(x, -y, z), Vector2(1.0f, 1.0f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(x, -y, z), Vector2(1.0f, 1.0f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(-x, -y, z), Vector2(0.0f, 1.0f), Vector3(0.0f, -1.0f, 0.0f) },
		{ Vector3(-x, -y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, -1.0f, 0.0f) },


	};
	m_Cube.buffer = m_render_system->create_vertex_buffer(
		helium::system::EBufferMode::Static,
		vertices,
		sizeof(Vertex),
		m_Cube.drawcount);

	m_Cube.raster = m_render_system->create_rasterizer_state(
		system::EFillMode::Solid, system::ECullMode::Back);

	m_Cube.depth = m_render_system->create_depth_state(
		true, true, system::EDepthMode::Less);

	m_Cube.world = m_Cube.world * Matrix4::translation(m_Position);

	m_Rotation = Vector3(0.0f, 0.0f, 0.0f);
};

Cube::~Cube()
{

};
Cube::Ptr Cube::Create(std::string p_texture, Vector3 p_Vol)
{
	return Cube::Ptr(new Cube(p_texture, p_Vol));
};

void Cube::Initialize()
{
	m_Rotation = Vector3(0.0f, 0.0f, 0.0f);
};
void Cube::Update(const float& p_Deltatime)
{
	
	p_Deltatime;
};
void Cube::Draw(scene::Camera* p_Camera)
{
	//m_Cube.world = Matrix4::translation(m_Position) *  Matrix4::rotation(m_Rotation, m_Radians);
	m_Transform.set_position(m_Position);
	m_Transform.set_rotation(m_Rotation);
	m_Transform.set_scale(m_Scale.m_x);
	m_Transform.recalculate();
	m_Cube.world = m_Transform.get_transform();

	m_render_system->select_shader(m_Cube.shader);
	m_render_system->select_texture(m_Cube.texture);
	m_render_system->select_sampler_state(m_Cube.sampler);
	m_render_system->select_vertex_format(m_Cube.format);
	m_render_system->select_vertex_buffer(m_Cube.buffer);
	m_render_system->select_depth_state(m_Cube.depth);
	m_render_system->select_rasterizer_state(m_Cube.raster);
	m_render_system->select_blend_state(m_Cube.blend);
	m_render_system->set_shader_constant_matrix4("projection", p_Camera->get_projection());
	m_render_system->set_shader_constant_matrix4("view", p_Camera->get_view());
	m_render_system->set_shader_constant_matrix4("world", m_Cube.world);
	m_render_system->set_shader_constant_vector3("direction", sun.direction);
	m_render_system->set_shader_constant_vector3("diffuse", sun.diffuse);
	m_render_system->set_shader_constant_vector3("ambient", sun.ambient);
	m_render_system->set_shader_constant_vector3("eye", p_Camera->get_position());
	m_render_system->set_shader_constant_raw("material", &m_Cube.material, sizeof(m_Cube.material));
	m_render_system->apply();
	m_render_system->draw(helium::system::EDrawMode::TriangleList, 0, m_Cube.drawcount);
};

void Cube::SetPosition(Vector3 p_Pos)
{
	m_Position = p_Pos;
	
};
void Cube::SetVolume(Vector3 p_Vol)
{
	m_Volume = p_Vol;
};
void Cube::SetScale(Vector3 p_Scale)
{
	m_Scale = p_Scale;
};
void Cube::SetRotation(Vector3 p_axis)
{
	m_Rotation = p_axis;

};

Vector3 Cube::GetVolume()
{
	return m_Volume;
};

////////////////
