#include "stdafx.h"
#include <kartong/SolidCube.hpp>
#include <kartong/BoundingBox.hpp>
#include <kartong/CollisionManager.hpp>
#include <helium/service_locator.hpp>
#include <kartong/GameObject.hpp>

using namespace kartong;

SolidCube::SolidCube(const std::string& p_File, const Vector3& p_Volume, const Vector3& p_Pos)
{
	SetType(ObjectType::ENVIRONMENT);
	SetPhysicsType(PhysicsType::STATIC);
	m_Position = p_Pos;
	m_CollisionManager = helium::ServiceLocator<kartong::CollisionManager>::get_service();
	m_Bounds = m_CollisionManager->CreateNewBox(this, m_Position, p_Volume, 150.0f);
	m_Cube = Cube::Create(p_File, p_Volume);
	m_Cube->SetPosition(m_Position);

	m_LongestRadius = m_Bounds->GetHalfVolume().m_x;
	if (m_LongestRadius < m_Bounds->GetHalfVolume().m_y)
		m_LongestRadius = m_Bounds->GetHalfVolume().m_y;
	if (m_LongestRadius < m_Bounds->GetHalfVolume().m_z)
		m_LongestRadius = m_Bounds->GetHalfVolume().m_z;

	obj.BS.m_radius = m_LongestRadius;
};
SolidCube::~SolidCube()
{

};

SolidCube::Ptr SolidCube::Create(const std::string& p_File, const Vector3& p_Volume, const Vector3& p_Pos)
{
	return SolidCube::Ptr(new SolidCube(p_File, p_Volume, p_Pos));
};

void SolidCube::Initialize()
{

};
void SolidCube::Update(const float& p_Deltatime)
{
	p_Deltatime;
	m_Cube->SetPosition(m_Position);
	m_Bounds->SetPosition(m_Position);
	obj.BS.m_center = m_Position;
	
};

void SolidCube::Draw(scene::Camera* p_Camera)
{

	m_Cube->Draw(p_Camera);
};