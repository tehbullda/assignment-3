#include "stdafx.h"

#include <kartong/HUD_Ammo.hpp>
#include <vector>
using namespace kartong;

HUDAmmo::HUDAmmo()
{
	m_TexRound = GUITexture::Create("HUD/Ammo.png", Vector2(0.0f, 0.0f));
	//m_Background = GUI;

	m_Padding = 5.0f;
	m_Current = 0;
	m_Max = 0;

};
HUDAmmo::~HUDAmmo()
{

};

HUDAmmo::Ptr HUDAmmo::Create()
{
	return HUDAmmo::Ptr(new HUDAmmo());
};

void HUDAmmo::Initialize()
{

	m_Positions.clear();
	UpdatePositions();

};
void HUDAmmo::Update(int p_Current, int p_Max)
{
	m_Current = p_Current;
	m_Max = p_Max;
	if (m_Current != p_Current || m_Max != p_Max)
	{
		m_Current = p_Current;
		m_Max = p_Max;	
	}
	Initialize();



};
void HUDAmmo::Draw()
{
	for (unsigned i = 0; i < m_Positions.size(); i++)
	{
		DrawRound(m_Positions.at(i));
	}
};
void HUDAmmo::SetPosition(Vector2 p_Pos)
{
	m_Position = p_Pos;
};
void HUDAmmo::UpdatePositions()
{
	for (int i = 0; i < m_Current; i++)
	{

		float x = m_Position.m_x - ((m_TexRound->GetGlobalSize().m_x + m_Padding) * i);
		float y = m_Position.m_y - m_TexRound->GetGlobalSize().m_x;
		Vector2 test = Vector2(x, y);
		m_Positions.push_back(test);
	}
};
void HUDAmmo::DrawRound(Vector2 p_Pos)
{
	m_TexRound->SetPos(p_Pos);
	m_TexRound->Draw();
};