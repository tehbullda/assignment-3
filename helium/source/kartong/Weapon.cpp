#pragma once
#include "stdafx.h"
#include <Kartong/Weapon.hpp>
#include <helium/service_locator.hpp>
#include <helium/system/audio_system.hpp>

using namespace kartong;
#ifndef PistolMaxAmmo
#define PistolMaxAmmo 12
#endif

#ifndef SniperMaxAmmo
#define SniperMaxAmmo 5
#endif

#ifndef RailgunMaxAmmo
#define RailgunMaxAmmo INT_MAX
#endif

Weapon::Weapon()
{
	m_audio_system = ServiceLocator<system::AudioSystem>::get_service();



	m_Crosshair = GUITexture::Create("crosshair_full.png", Vector2((kartong::global::ResW / 2) - 50, (kartong::global::ResH / 2) - 50));
	m_Crosshair->SetPos(Vector2((global::ResW / 2) - (m_Crosshair->GetGlobalSize().m_x / 2), (global::ResH / 2) - (m_Crosshair->GetGlobalSize().m_y) / 2), 0.002f);
	if (global::ResH == 720.0f)
	{
		m_Weapon = GUITexture::Create("Weapon_720.png", Vector2(0.0f, 0.0f));
		m_SniperScope = GUITexture::Create("scope_720p.png", Vector2(0.0f, 0.0f));
	}
	else
	{
		m_Weapon = GUITexture::Create("Weapon_1080.png", Vector2(0.0f, 0.0f));
		m_SniperScope = GUITexture::Create("scope_1080p.png", Vector2(0.0f, 0.0f));
	}
	m_Weapon->SetPos(Vector2(global::ResW - m_Weapon->GetGlobalSize().m_x - 15, global::ResH - m_Weapon->GetGlobalSize().m_y), 0.001f);

	
	m_SniperIcon = GUITexture::Create("HUD/Sniper.png", Vector2(0.0f, 0.0f));
	m_SniperIcon->SetPos(Vector2(global::ResW - m_SniperIcon->GetGlobalSize().m_x - 15.0f, 15.0f), 0.0001f);

	m_Weapon_Sniper = Model::Create("../data/models/weapon.dae");
	m_Weapon_Sniper->SetScale(Vector3(0.3f, 0.3f, 0.3f));

	m_CurrentWeapon = weapons::SNIPER;

	//Sniper Sounds
	m_audio_system->add_sound("../data/sound/weapons/GunShot PS03_39.wav", system::ChannelType::SOUND, system::EAudioType::Buffer);
	m_audio_system->add_sound("../data/sound/weapons/SteamBurst PS02_84_2.wav", system::ChannelType::SOUND, system::EAudioType::Buffer);
	m_audio_system->add_sound("../data/sound/weapons/DrawerMetal IE01_19_4.wav", system::ChannelType::SOUND, system::EAudioType::Buffer);
};
Weapon::~Weapon()
{

};

void Weapon::Initialize()
{
	m_Reloading = false;
	m_CanShoot = true;
	m_Timer = 0.0f;
	m_AfterEffectTimer = 0.8f;
	m_AfterEffectCounter = 1.5f;
	m_CanPlayAfterSound = false;
	m_ShotFired = false;
};
void Weapon::Update(float p_Deltatime)
{
	m_ShotFired = false;
	if (m_CanPlayAfterSound)
	{
		m_AfterEffectCounter -= p_Deltatime;

		if (m_AfterEffectCounter <= 0.0f)
		{
			Playsound(weaponsoundtypes::AFTER);
			m_CanPlayAfterSound = false;
		}
	}
	if (!m_CanShoot)
		m_Timer -= p_Deltatime;
	if (m_Timer <= 0.0f)
		m_CanShoot = true;
	if (m_Reloading)
	{
		m_ReloadTimer -= p_Deltatime;
		//Debug::write(EDebugLevel::Info, std::to_string(m_ReloadTimer));
	}

	if (m_Reloading && m_ReloadTimer <= 0.0f)
	{
		m_Ammo = m_Stats.MaxAmmo;
		m_Reloading = false;
		Debug::write(EDebugLevel::Info, "...Reloaded!");
	}

};
bool Weapon::Shoot()
{
	//Requirements to shoot
	if (m_Ammo <= 0)
	{
		//Debug::write(EDebugLevel::Info, "No Ammo in Mag!");
		return false;
	}
	if (!m_CanShoot)
		return false;

	//Shooting!
	m_Ammo -= 1;
	m_CanShoot = false;
	m_Timer = m_Stats.FireRate;
	Playsound(weaponsoundtypes::SHOT);
	m_CanPlayAfterSound = true;
	m_AfterEffectCounter = m_AfterEffectTimer;
	m_ShotFired = true;
	Debug::write(EDebugLevel::Info, "Shooting!");
	//Cancel Reload
	if (m_Reloading)
	{
		m_Reloading = false;
		Debug::write(EDebugLevel::Info, "...Reloaded Canceled");
	}
	return true;
};
void Weapon::Reload()
{
	if (!m_Reloading)
	{
		m_Reloading = true;
		m_ReloadTimer = m_Stats.ReloadTime;
		Playsound(weaponsoundtypes::RELOAD);
		Debug::write(EDebugLevel::Info, "Reloading...");
	}
}
void Weapon::Draw(scene::Camera* p_Camera, bool p_Zoomed)
{
	p_Camera;

	if (p_Zoomed)
	{
		DrawScope();
	}
	else
	{
		m_Crosshair->Draw();
		m_Weapon->Draw();
		DrawIcon();
	}

	//m_Weapon_Sniper->Draw(p_Camera);
};

void Weapon::SetDamage(const float p_Damage)
{
	m_Stats.Damage = p_Damage;
};
void Weapon::SetRange(const float p_Range)
{
	m_Stats.Range = p_Range;
};
void Weapon::SetMaxAmmo(const int p_Ammo)
{
	m_Stats.MaxAmmo = p_Ammo;
	m_Ammo = p_Ammo;
};
void Weapon::SetReloadTime(const float p_Time)
{
	m_Stats.ReloadTime = p_Time;
};
void Weapon::SetFirerate(const float p_Firerate)
{
	m_Stats.FireRate = p_Firerate;
};

float Weapon::GetDamage()
{
	return m_Stats.Damage;
};
float Weapon::GetRange()
{
	return m_Stats.Range;
};
int Weapon::GetMaxAmmo()
{
	return m_Stats.MaxAmmo;
};

int Weapon::GetMaxAmmo(weapons::Weapons type) {
	switch (type) {
	case weapons::SNIPER:
		return SniperMaxAmmo;
	case weapons::RAILGUN:
		return RailgunMaxAmmo;
	case weapons::PISTOL:
		return PistolMaxAmmo;
	default:
		return 0;
	}
}
int Weapon::GetCurrentammo()
{
	return m_Ammo;
};
float Weapon::GetReloadTime()
{
	return m_Stats.ReloadTime;
};
float Weapon::GetFirerate()
{
	return m_Stats.FireRate;
};

void Weapon::SetPosition(helium::Vector3 p_Position)
{
	m_Weapon_Sniper->SetPosition(p_Position);
};
void Weapon::SetRotation(helium::Vector3 p_Rotation)
{
	m_Weapon_Sniper->SetRotation(p_Rotation);
};
void Weapon::SetScale(helium::Vector3 p_Scale)
{
	m_Weapon_Sniper->SetScale(p_Scale);
};

void Weapon::Playsound(int p_Type)
{

	if (p_Type == weaponsoundtypes::SHOT)
	{
		if (m_CurrentWeapon == weapons::SNIPER)
			m_audio_system->play("../data/sound/weapons/GunShot PS03_39.wav");
		if (m_CurrentWeapon == weapons::RAILGUN)
			m_audio_system->play("../data/sound/weapons/GunShot PS03_39.wav");
		if (m_CurrentWeapon == weapons::PISTOL)
			m_audio_system->play("../data/sound/weapons/GunShot PS03_39.wav");
	}
	if (p_Type == weaponsoundtypes::AFTER)
	{

		if (m_CurrentWeapon == weapons::SNIPER)
			m_audio_system->play("../data/sound/weapons/SteamBurst PS02_84_2.wav");
		if (m_CurrentWeapon == weapons::RAILGUN)
			m_audio_system->play("../data/sound/weapons/GunShot PS03_39.wav");
		if (m_CurrentWeapon == weapons::PISTOL)
			m_audio_system->play("../data/sound/weapons/GunShot PS03_39.wav");
	}

	if (p_Type == weaponsoundtypes::RELOAD)
	{
		if (m_CurrentWeapon == weapons::SNIPER)
			m_audio_system->play("../data/sound/weapons/DrawerMetal IE01_19_4.wav");
		if (m_CurrentWeapon == weapons::RAILGUN)
			m_audio_system->play("../data/sound/weapons/GunShot PS03_39.wav");
		if (m_CurrentWeapon == weapons::PISTOL)
			m_audio_system->play("../data/sound/weapons/GunShot PS03_39.wav");
	}
};

void Weapon::SetWeapon(const int& p_Weapon)
{

	if (p_Weapon == weapons::SNIPER)
	{
		m_CurrentWeapon = p_Weapon;
		SetDamage(100.0f);
		SetFirerate(1.2f);
		SetMaxAmmo(10);
		SetRange(300.0f);
		SetReloadTime(2.4f);
	}
	if (p_Weapon == weapons::RAILGUN)
	{
		m_CurrentWeapon = p_Weapon;
		SetDamage(500.0f);
		SetFirerate(1.2f);
		SetMaxAmmo(1);
		SetRange(300.0f);
		SetReloadTime(2.1f);
	}
	if (p_Weapon == weapons::PISTOL)
	{
		m_CurrentWeapon = p_Weapon;
		SetDamage(25.0f);
		SetFirerate(0.3f);
		SetMaxAmmo(12);
		SetRange(150.0f);
		SetReloadTime(1.4f);
	}
};

void Weapon::DrawScope()
{
	if (m_CurrentWeapon == weapons::SNIPER)
		m_SniperScope->Draw();
	if (m_CurrentWeapon == weapons::RAILGUN)
		m_SniperScope->Draw();
	if (m_CurrentWeapon == weapons::PISTOL)
		m_SniperScope->Draw();

}
void Weapon::DrawIcon()
{
	if (m_CurrentWeapon == weapons::SNIPER)
		m_SniperIcon->Draw();
	if (m_CurrentWeapon == weapons::RAILGUN)
		m_SniperIcon->Draw();
	if (m_CurrentWeapon == weapons::PISTOL)
		m_SniperIcon->Draw();

}
bool Weapon::ShotFired()
{
	return m_ShotFired;
};