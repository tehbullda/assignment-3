#include "stdafx.h"
#include <kartong/CollisionManager.hpp>
#include <helium/service_locator.hpp>
#include <kartong/Randomizer.hpp>
using namespace kartong;
using namespace helium;
ShootableTestDummy::ShootableTestDummy()
{
	m_CollisionManager = helium::ServiceLocator<kartong::CollisionManager>::get_service();
	//m_Cube->SetRotation(Vector3(0.0f, 0.0f, 0.0f));
	SetTag(("Dummy"));
	SetName(("Bob"));
	SetType(ObjectType::INTERACTIVE);
	SetPhysicsType(PhysicsType::STATIC);
	m_XMin = 0.0f;
	m_XMax = 0.0f;
	m_YMin = 0.0f;
	m_YMax = 0.0f;
	m_MoveSpeedX = 0.0f;
	m_MoveSpeedY = 0.0f;
	m_InvetredX = false;
	m_InvetredY = false;
	m_TargetHit = false;

};
ShootableTestDummy::~ShootableTestDummy()
{
	if (m_Bounds) {
		m_Bounds = nullptr;
	}
};
ShootableTestDummy::Ptr ShootableTestDummy::Create()
{
	return ShootableTestDummy::Ptr(new ShootableTestDummy());
};
void ShootableTestDummy::Initialize()
{
	m_Cube = Cube::Create("target.png", Vector3(3.0f, 3.0f, 1.0f));
	m_Cube->SetPosition(m_Position);
	m_Bounds = m_CollisionManager->CreateNewBox(this, m_Position, Vector3(3.0f, 3.0f, 1.0f), 100.0f);
};
void ShootableTestDummy::Update(const float& p_Deltatime)
{
	//m_Cube->SetPosition(m_Position);
	//m_Bounds->SetPosition(m_Position);
	m_Deltatime = p_Deltatime;
	if (m_Bounds->BeenShot())
	{
		m_TargetHit = true;
		m_Bounds->SetShot(false);
	}
	if (!m_TargetHit)
		UpdateMovement();
	m_Cube->SetPosition(m_Position);
	m_Bounds->SetPosition(m_Position);

};
void ShootableTestDummy::Draw(scene::Camera* p_Cam)
{
	m_Cube->Draw(p_Cam);

};

void ShootableTestDummy::UpdateMovement()
{
	if (m_MoveSpeedX || (m_XMin == 0.0f && m_XMax == 0.0f))
	{
		if (!m_InvetredX)
			m_Position.m_x = m_Position.m_x += m_MoveSpeedX * m_Deltatime;
		else
			m_Position.m_x = m_Position.m_x -= m_MoveSpeedX * m_Deltatime;

		if (m_Position.m_x >= m_XMax)
			m_InvetredX = true;
		if (m_Position.m_x <= m_XMin)
			m_InvetredX = false;
	}
	if (m_MoveSpeedY || (m_YMin == 0.0f && m_YMax == 0.0f))
	{
		if (!m_InvetredY)
			m_Position.m_y = m_Position.m_y += m_MoveSpeedY * m_Deltatime;
		else
			m_Position.m_y = m_Position.m_y -= m_MoveSpeedY * m_Deltatime;

		if (m_Position.m_y >= m_YMax)
			m_InvetredY = true;
		if (m_Position.m_y <= m_YMin)
			m_InvetredY = false;
	}
};

void ShootableTestDummy::SetMovementX(const float& p_Min, const float& p_Max)
{
	m_XMin = p_Min;
	m_XMax = p_Max;
};
void ShootableTestDummy::SetMovementY(const float& p_Min, const float& p_Max)
{
	m_YMin = p_Min;
	m_YMax = p_Max;
};

void ShootableTestDummy::SetMoveSpeedX(const float& p_MoveSpeed)
{
	m_MoveSpeedX = p_MoveSpeed;
};
void ShootableTestDummy::SetMoveSpeedY(const float& p_MoveSpeed)
{
	m_MoveSpeedY = p_MoveSpeed;
};
