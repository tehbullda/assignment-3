#include "stdafx.h"


#include <kartong/Model.hpp>
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>

using namespace kartong;

Model::Model(std::string p_File)
{
	//m_Scale = Vector3(1.0f, 1.0f, 1.0f);

	sun.direction = Vector3(-1.0f, -1.0f, 1.0);
	sun.direction.normalize();
	sun.diffuse = Vector3(1.0f, 1.0f, 1.0f);
	sun.ambient = Vector3(1.0f, 1.0f, 1.0f);
	sun.eye = Vector3(0.0f, 0.0f, -6.0f);
	sun.pad0 = sun.pad1 = sun.pad2 = sun.pad3 = 1.0f;


	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(p_File,
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		//aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	if (!scene)
	{
		printf("Import FAILED");
	}

	//for (uint32_t i = 0; i < scene->mNumMeshes; i++) {
	//	for (uint32_t j = 0; j < scene->mMeshes[i]->mNumVertices; j++) {
	//		scene->mMeshes[i]->mVertices[j]; //Here be vertices
	//	}

	//}
	


	m_Rotation = Vector3(0.0f, 0.0f, 0.0f);
	m_render_system = helium::ServiceLocator<helium::system::RenderSystem>::get_service();

	m_Model = { 0 };

	m_shader = helium::resource::Shader::create("../data/txt/cube.shader.txt");
	m_Model.shader = m_render_system->create_shader(
		m_shader->get_vertex_source().c_str(),
		m_shader->get_pixel_source().c_str());

	m_format = helium::resource::VertexFormat::create("../data/txt/cube.format.txt");
	m_Model.format = m_render_system->create_vertex_format(
		const_cast<helium::system::VertexFormatDesc*>(m_format->get_desc()),
		m_format->get_count(),
		m_Model.shader);

	m_texture = helium::resource::Texture::create("../data/whitetex.png");
	m_Model.texture = m_render_system->create_texture(
		m_texture->get_format(),
		m_texture->get_type(),
		m_texture->get_width(),
		m_texture->get_height(),
		m_texture->get_depth(),
		m_texture->get_pixel_data());

	m_sampler = helium::resource::Sampler::create("../data/txt/cube.sampler.txt");
	m_Model.sampler = m_render_system->create_sampler_state(
		m_sampler->get_filter_mode(),
		m_sampler->get_addr_u(),
		m_sampler->get_addr_v(),
		m_sampler->get_addr_w());

	m_Model.raster = m_render_system->create_rasterizer_state(helium::system::EFillMode::Wireframe, helium::system::ECullMode::None);

	m_Model.blend = m_render_system->create_blend_state(
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendMode::One, system::EBlendMode::One,
		system::EBlendOp::Add, system::EBlendOp::Add);

	m_Model.world.identity();
	m_Model.material.ambient = 0.1f;
	m_Model.material.diffuse = 0.5f;
	m_Model.material.specular = 0.5f;
	m_Model.material.shininess = 32.0f;

	//Testing with an object with only one mesh first
	

	/*float x = m_Volume.m_x / 2.0f;
	float y = m_Volume.m_y / 2.0f;
	float z = m_Volume.m_z / 2.0f;*/

	struct Vertex
	{
		Vector3 position;
		Vector2 texcoord;
		Vector3 normal;
	};


	Vertex* vertices;
	int numofvertices = 0;
	int current = 0;

	m_LowestPoints = { 0.0f, 0.0f, 0.0f };
	m_HighestPoints = { 0.0f, 0.0f, 0.0f };

	for (uint32_t i = 0; i < scene->mNumMeshes; i++)
	{
		numofvertices += scene->mMeshes[i]->mNumVertices;
	}


	m_Model.drawcount = numofvertices;
	

		vertices = new Vertex[numofvertices];
		scene->mMeshes[0]->mTextureCoords[0]->z;
		//scene->
		for (uint32_t i = 0; i < scene->mNumMeshes; i++) {
			for (uint32_t j = 0; j < scene->mMeshes[i]->mNumVertices; j++) {
				vertices[current] = {
					Vector3(
					(float)scene->mMeshes[i]->mVertices[j].x,
					(float)scene->mMeshes[i]->mVertices[j].y,
					(float)scene->mMeshes[i]->mVertices[j].z),
					Vector2(
					0.0f,
					0.0f),
					Vector3(
					scene->mMeshes[i]->mNormals[j].x,
					scene->mMeshes[i]->mNormals[j].y,
					scene->mMeshes[i]->mNormals[j].z) };

				//High and low check for bounds
				if (scene->mMeshes[i]->mVertices[j].x > m_HighestPoints.m_x)
					m_HighestPoints.m_x = scene->mMeshes[i]->mVertices[j].x;
				if (scene->mMeshes[i]->mVertices[j].y > m_HighestPoints.m_y)
					m_HighestPoints.m_y = scene->mMeshes[i]->mVertices[j].y;
				if (scene->mMeshes[i]->mVertices[j].z > m_HighestPoints.m_z)
					m_HighestPoints.m_z = scene->mMeshes[i]->mVertices[j].z;

				if (scene->mMeshes[i]->mVertices[j].x < m_LowestPoints.m_x)
					m_LowestPoints.m_x = scene->mMeshes[i]->mVertices[j].x;
				if (scene->mMeshes[i]->mVertices[j].y < m_LowestPoints.m_y)
					m_LowestPoints.m_y = scene->mMeshes[i]->mVertices[j].y;
				if (scene->mMeshes[i]->mVertices[j].z < m_LowestPoints.m_z)
					m_LowestPoints.m_z = scene->mMeshes[i]->mVertices[j].z;

				current++;
			}
		}
	
	//{ Vector3(-x, y, -z), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) },







	m_Model.buffer = m_render_system->create_vertex_buffer(
		helium::system::EBufferMode::Static,
		vertices,
		sizeof(Vertex),
		m_Model.drawcount);

	delete[] vertices;

	m_Model.raster = m_render_system->create_rasterizer_state(
		system::EFillMode::Solid, system::ECullMode::Back);

	m_Model.depth = m_render_system->create_depth_state(
		true, true, system::EDepthMode::Less);

	m_Model.world = m_Model.world * Matrix4::translation(m_Position);
};

Model::~Model()
{
	if (m_Importer != nullptr)
	{
		delete m_Importer;
		m_Importer = nullptr;
	}

};
Model::Ptr Model::Create(std::string p_File)
{
	return Model::Ptr(new Model(p_File));
};

void Model::Initialize()
{

};
void Model::Update(const float& p_Deltatime)
{

	p_Deltatime;
};
void Model::Draw(scene::Camera* p_Camera)
{
	//m_Model.world = Matrix4::translation(m_Position) *  Matrix4::rotation(m_Rotation, m_Radians);
	m_Transform.set_rotation(m_Rotation);
	m_Transform.set_position(m_Position);
	m_Transform.recalculate();
	m_Model.world = m_Transform.get_transform();

	m_render_system->select_shader(m_Model.shader);
	m_render_system->select_texture(m_Model.texture);
	m_render_system->select_sampler_state(m_Model.sampler);
	m_render_system->select_vertex_format(m_Model.format);
	m_render_system->select_vertex_buffer(m_Model.buffer);
	m_render_system->select_depth_state(m_Model.depth);
	m_render_system->select_rasterizer_state(m_Model.raster);
	m_render_system->select_blend_state(m_Model.blend);
	m_render_system->set_shader_constant_matrix4("projection", p_Camera->get_projection());
	m_render_system->set_shader_constant_matrix4("view", p_Camera->get_view());
	m_render_system->set_shader_constant_matrix4("world", m_Model.world);
	m_render_system->set_shader_constant_vector3("direction", sun.direction);
	m_render_system->set_shader_constant_vector3("diffuse", sun.diffuse);
	m_render_system->set_shader_constant_vector3("ambient", sun.ambient);
	m_render_system->set_shader_constant_vector3("eye", p_Camera->get_position());
	m_render_system->set_shader_constant_raw("material", &m_Model.material, sizeof(m_Model.material));
	m_render_system->apply();
	m_render_system->draw(helium::system::EDrawMode::TriangleList, 0, m_Model.drawcount);
};

void Model::SetPosition(Vector3 p_Pos)
{
	m_Position = p_Pos;

};
void Model::SetVolume(Vector3 p_Vol)
{
	m_Volume = p_Vol;
};
void Model::SetScale(Vector3 p_Scale)
{
	m_Scale = p_Scale;
};
void Model::SetRotation(Vector3 p_axis)
{
	m_Rotation = p_axis;
};
