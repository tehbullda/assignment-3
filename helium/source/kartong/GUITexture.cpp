#include "stdafx.h"

#include <helium/service_locator.hpp>
//#include <helium/system/audio_system.hpp>
//#include <helium/system/collision_system.hpp>
#include <helium/system/render_system.hpp>
//#include <helium/gameplay/abstract_game.hpp>
//
//#include <helium/os_event.hpp>
//#include <helium/os_event_dispatcher.hpp>
//#include <helium/scene/camera.hpp>
//
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>

namespace kartong
{

	using namespace helium;

	GUITexture::GUITexture(const std::string& p_texture, const helium::Vector2 p_Pos, bool isText)
	{
		m_Position = p_Pos;
		m_render_system = helium::ServiceLocator<helium::system::RenderSystem>::get_service();


		//element = { 0 };

		m_shader = helium::resource::Shader::create("../data/gui/gui.shader.txt");
		element.shader = m_render_system->create_shader(
			m_shader->get_vertex_source().c_str(),
			m_shader->get_pixel_source().c_str());

		m_texture = helium::resource::Texture::create("../data/gui/" + p_texture);
		element.texture = m_render_system->create_texture(
			m_texture->get_format(),
			m_texture->get_type(),
			m_texture->get_width(),
			m_texture->get_height(),
			m_texture->get_depth(),
			m_texture->get_pixel_data());

		m_sampler = helium::resource::Sampler::create("../data/gui/gui.sampler.txt");
		element.sampler = m_render_system->create_sampler_state(
			m_sampler->get_filter_mode(),
			m_sampler->get_addr_u(),
			m_sampler->get_addr_v(),
			m_sampler->get_addr_w());

		m_format = helium::resource::VertexFormat::create("../data/gui/gui.vertexformat.txt");
		element.format = m_render_system->create_vertex_format(
			const_cast<helium::system::VertexFormatDesc*>(m_format->get_desc()),
			m_format->get_count(),
			element.shader);

		//element.depth = m_render_system->create_depth_state(false, false, helium::system::EDepthMode::Always);

		//Vertex* vertices = new Vertex[skybox.drawcount];
		//m_texture->get_width();

		element.x = 0.0f;
		element.y = 0.0f;
		element.w = m_texture->get_width();
		element.h = m_texture->get_height();

		//		float v = 1.0f;

		struct Vertex
		{
			Vector3 position;
			Vector2 texcoord;
		}
		vertices[] =
		{
			{ Vector3(element.x, element.y, 0.5f), Vector2(0.0f, 0.0f) },
			{ Vector3(element.x + element.w, element.y, 0.5f), Vector2(1.0f, 0.0f) },
			{ Vector3(element.x + element.w, element.y + element.h, 0.5f), Vector2(1.0f, 1.0f) },
			{ Vector3(element.x, element.y + element.h, 0.5f), Vector2(0.0f, 1.0f) },
		};
		if (!isText) {
			element.buffer = m_render_system->create_vertex_buffer(
				helium::system::EBufferMode::Static,
				vertices, sizeof(Vertex),
				sizeof(vertices) / sizeof(vertices[0]));

			short indices[] = { 0, 1, 2, 2, 3, 0 };
			element.index = m_render_system->create_index_buffer(
				helium::system::EIndexBufferType::Int16,
				sizeof(indices) / sizeof(indices[0]), indices);
			element.drawcount = 6;
		}
		else {
			element.buffer = m_render_system->create_vertex_buffer(system::EBufferMode::Dynamic, 0, sizeof(Vertex), 4 * 4096);
			element.index = m_render_system->create_index_buffer(system::EIndexBufferType::Int16, 6 * 4096, 0, system::EBufferMode::Dynamic);
		}
		element.blend = m_render_system->create_blend_state(
			helium::system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha,
			helium::system::EBlendMode::One, system::EBlendMode::One,
			helium::system::EBlendOp::Add, system::EBlendOp::Add);

		element.depth = m_render_system->create_depth_state(false, false, system::EDepthMode::Greater);
		element.world = Matrix4::translation(Vector3(m_Position.m_x, m_Position.m_y, 0.0f));



		element.raster = m_render_system->create_rasterizer_state(helium::system::EFillMode::Solid, helium::system::ECullMode::None);

		m_Size.m_x = element.w;
		m_Size.m_y = element.h;
		m_Scale.m_x = 1.0f;
		m_Scale.m_y = 1.0f;
		m_StandardWorld = element.world;
		m_Dirty = true;
	};
	GUITexture::~GUITexture()
	{

	};

	GUITexture::Ptr GUITexture::Create(const std::string& p_texture, const helium::Vector2 p_Pos)
	{
		return GUITexture::Ptr(new GUITexture(p_texture, p_Pos));
	};

	void GUITexture::Draw()
	{
		if (m_Dirty)
			element.world = Matrix4::scale(Vector3(m_Scale.m_x, m_Scale.m_y, 1.0f)) * Matrix4::translation(Vector3(m_Position.m_x, m_Position.m_y, m_Depth));
		m_Dirty = false;

		Matrix4 ortho = Matrix4::orthogonal(kartong::global::ResW, kartong::global::ResH, 0.0f, 1.0f);

		m_render_system->select_shader(element.shader);
		m_render_system->select_texture(element.texture);
		m_render_system->select_sampler_state(element.sampler);
		m_render_system->select_vertex_format(element.format);
		m_render_system->select_vertex_buffer(element.buffer);
		m_render_system->select_index_buffer(element.index);
		m_render_system->select_blend_state(element.blend);
		m_render_system->select_rasterizer_state(element.raster);
		m_render_system->set_shader_constant_matrix4("projection", ortho);
		m_render_system->set_shader_constant_matrix4("world", element.world);
		m_render_system->apply();

		m_render_system->drawi(system::EDrawMode::TriangleList, 0, element.drawcount);
	};

	void GUITexture::SetPos(const helium::Vector2 p_Pos, float depth)
	{
		//element.world = Matrix4::translation(Vector3(p_Pos.m_x, p_Pos.m_y, depth));
		m_Position = p_Pos;
		m_Depth = depth;
		m_Dirty = true;
	};
	void GUITexture::SetNewTexture(const std::string& p_Texture)	{
		p_Texture;
	};
	void GUITexture::SetScale(Vector2 p_Scale)
	{
		//element.world = m_StandardWorld * Matrix4::scale(Vector3(p_Scale.m_x, p_Scale.m_y, 1.0f));
		m_Scale = p_Scale;
		m_Dirty = true;
	};

	Vector2 GUITexture::GetPos()
	{
		return m_Position;
	};
	Hitbox2D GUITexture::GetHitbox() {
		return Hitbox2D(m_Position.m_x, m_Position.m_y, element.w, element.h);
	};
	Vector2 GUITexture::GetGlobalSize()
	{
		return Vector2(m_Size.m_x * m_Scale.m_y, m_Size.m_y * m_Scale.m_y);
	};
	Vector2 GUITexture::GetLocalSize()
	{
		return m_Size;
	};
	Vector2 GUITexture::GetScale()
	{
		return m_Scale;
	};
};
