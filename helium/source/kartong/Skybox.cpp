#include "stdafx.h"

#include <helium/service_locator.hpp>
//#include <helium/system/audio_system.hpp>
//#include <helium/system/collision_system.hpp>
#include <helium/system/render_system.hpp>
//#include <helium/gameplay/abstract_game.hpp>
//
//#include <helium/os_event.hpp>
//#include <helium/os_event_dispatcher.hpp>
//#include <helium/scene/camera.hpp>
//
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>

namespace kartong
{
	
	using namespace helium;

	Skybox::Skybox(const std::string& p_texture)
	{
		m_render_system = helium::ServiceLocator<helium::system::RenderSystem>::get_service();

		skybox = { 0 };

		m_shader = helium::resource::Shader::create("../data/skybox/skybox.shader.txt");
		skybox.shader = m_render_system->create_shader(
			m_shader->get_vertex_source().c_str(),
			m_shader->get_pixel_source().c_str());

		m_format = helium::resource::VertexFormat::create("../data/skybox/skybox.vertexformat.txt");
		skybox.format = m_render_system->create_vertex_format(
			const_cast<helium::system::VertexFormatDesc*>(m_format->get_desc()),
			m_format->get_count(),
			skybox.shader);

		m_texture = helium::resource::Texture::create("../data/skybox/" + p_texture);
		skybox.texture = m_render_system->create_texture(
			m_texture->get_format(),
			m_texture->get_type(),
			m_texture->get_width(),
			m_texture->get_height(),
			m_texture->get_depth(),
			m_texture->get_pixel_data());

		m_sampler = helium::resource::Sampler::create("../data/skybox/skybox.sampler.txt");
		skybox.sampler = m_render_system->create_sampler_state(
			m_sampler->get_filter_mode(),
			m_sampler->get_addr_u(),
			m_sampler->get_addr_v(),
			m_sampler->get_addr_w());

		skybox.raster = m_render_system->create_rasterizer_state(helium::system::EFillMode::Solid, helium::system::ECullMode::None);
		skybox.depth = m_render_system->create_depth_state(false, false, helium::system::EDepthMode::Always);

		skybox.drawcount = 30;
		//Vertex* vertices = new Vertex[skybox.drawcount];
		float v = 1.0f;
		//float texMin = 253.0f / 1024.0f;
		//float texMax = 1.0f - texMin;

		//float t = 1.0f / 3.0f;
		float t = 0.333333333f;
		float mt = 1.0f - t;
		struct Vertex
		{
			Vector3 position;
			Vector2 texcoord;
		}
		vertices[] =
		{
			// front
			//{ Vector3(-v, v, -v), Vector2(texMin, texMax) }, //24.6%, 0.24609375 - 0.75390625
			//{ Vector3(v, v, -v), Vector2(texMax, texMax) },
			//{ Vector3(v, -v, -v), Vector2(texMax, 1.0f) },
			//{ Vector3(v, -v, -v), Vector2(texMax, 1.0f) },
			//{ Vector3(-v, -v, -v), Vector2(texMin, 1.0f) },
			//{ Vector3(-v, v, -v), Vector2(texMin, texMax) },

			//// right
			//{ Vector3(v, v, -v), Vector2(texMax, texMax) },
			//{ Vector3(v, v, v), Vector2(texMax, texMin) },
			//{ Vector3(v, -v, v), Vector2(1.0f, texMin) },
			//{ Vector3(v, -v, v), Vector2(1.0f, texMin) },
			//{ Vector3(v, -v, -v), Vector2(1.0f, texMax) },
			//{ Vector3(v, v, -v), Vector2(texMax, texMax) },

			//// back
			//{ Vector3(v, v, v), Vector2(texMax, texMin) },
			//{ Vector3(-v, v, v), Vector2(texMin, texMin) },
			//{ Vector3(-v, -v, v), Vector2(texMin, 0.0f) },
			//{ Vector3(-v, -v, v), Vector2(texMin, 0.0f) },
			//{ Vector3(v, -v, v), Vector2(texMax, 0.0f) },
			//{ Vector3(v, v, v), Vector2(texMax, texMin) },

			//// left
			//{ Vector3(-v, v, v), Vector2(texMin, texMin) },
			//{ Vector3(-v, v, -v), Vector2(texMin, texMax) },
			//{ Vector3(-v, -v, -v), Vector2(0.0f, texMax) },
			//{ Vector3(-v, -v, -v), Vector2(0.0f, texMax) },
			//{ Vector3(-v, -v, v), Vector2(0.0f, texMin) },
			//{ Vector3(-v, v, v), Vector2(texMin, texMin) },

			//// top
			//{ Vector3(-v, v, v), Vector2(texMin, texMin) },
			//{ Vector3(v, v, v), Vector2(texMax, texMin) },
			//{ Vector3(v, v, -v), Vector2(texMax, texMax) },
			//{ Vector3(v, v, -v), Vector2(texMax, texMax) },
			//{ Vector3(-v, v, -v), Vector2(texMin, texMax) },
			//{ Vector3(-v, v, v), Vector2(texMin, texMin) },

			//// bottom
			//{ Vector3(-v, -v, -v), Vector2(0.0f, 0.0f) },
			//{ Vector3(v, -v, -v), Vector2(1.0f, 0.0f) },
			//{ Vector3(v, -v, v), Vector2(1.0f, 1.0f) },
			//{ Vector3(v, -v, v), Vector2(1.0f, 1.0f) },
			//{ Vector3(-v, -v, v), Vector2(0.0f, 1.0f) },
			//{ Vector3(-v, -v, -v), Vector2(0.0f, 0.0f) },

			{ Vector3(-v, v, -v), Vector2(t, t) }, //24.6%, 0.24609375 - 0.75390625
			{ Vector3(v, v, -v), Vector2(mt, t) },
			{ Vector3(v, -v, -v), Vector2(mt, mt) },
			{ Vector3(v, -v, -v), Vector2(mt, mt) },
			{ Vector3(-v, -v, -v), Vector2(t, mt) },
			{ Vector3(-v, v, -v), Vector2(t, t) },

			// right
			{ Vector3(v, v, -v), Vector2(mt, t) },
			{ Vector3(v, v, v), Vector2(1.0f, t) },
			{ Vector3(v, -v, v), Vector2(1.0f, mt) },
			{ Vector3(v, -v, v), Vector2(1.0f, mt) },
			{ Vector3(v, -v, -v), Vector2(mt, mt) },
			{ Vector3(v, v, -v), Vector2(mt, t) },

			// back
			{ Vector3(v, v, v), Vector2(t, mt) },
			{ Vector3(-v, v, v), Vector2(mt - 0.00141, mt) },
			{ Vector3(-v, -v, v), Vector2(mt - 0.00141, 1) },
			{ Vector3(-v, -v, v), Vector2(mt - 0.00141, 1) },
			{ Vector3(v, -v, v), Vector2(t, 1) },
			{ Vector3(v, v, v), Vector2(t, mt) },

			// left
			{ Vector3(-v, v, v), Vector2(0.0f, t) },
			{ Vector3(-v, v, -v), Vector2(t, t) },
			{ Vector3(-v, -v, -v), Vector2(t, mt) },
			{ Vector3(-v, -v, -v), Vector2(t, mt) },
			{ Vector3(-v, -v, v), Vector2(0.0f, mt) },
			{ Vector3(-v, v, v), Vector2(0.0f, t) },

			// top
			{ Vector3(-v, v, v), Vector2(t, 0) },
			{ Vector3(v, v, v), Vector2(mt - 0.00147, 0) },
			{ Vector3(v, v, -v), Vector2(mt - 0.00147, t) },
			{ Vector3(v, v, -v), Vector2(mt - 0.00147, t) },
			{ Vector3(-v, v, -v), Vector2(t, t) },
			{ Vector3(-v, v, v), Vector2(t, 0) },
		};




		skybox.buffer = m_render_system->create_vertex_buffer(
			helium::system::EBufferMode::Static,
			vertices,
			sizeof(Vertex),
			skybox.drawcount);
		
	};
	Skybox::~Skybox()
	{

	};

	Skybox::Ptr Skybox::Create(const std::string& p_texture)
	{
		return Skybox::Ptr(new Skybox(p_texture));
	};

	void Skybox::Draw(Matrix4 p_view, Matrix4 p_projection)
	{
		Matrix4 view = p_view;
		view.m._41 = 0.0f;
		view.m._42 = 0.0f;
		view.m._43 = 0.0f;
		m_render_system->select_shader(skybox.shader);
		m_render_system->select_texture(skybox.texture);
		m_render_system->select_sampler_state(skybox.sampler);
		m_render_system->select_vertex_format(skybox.format);
		m_render_system->select_vertex_buffer(skybox.buffer);
		m_render_system->select_depth_state(skybox.depth);
		m_render_system->select_rasterizer_state(skybox.raster);
		m_render_system->set_shader_constant_matrix4("projection", p_projection);
		m_render_system->set_shader_constant_matrix4("view", view);
		m_render_system->apply();
		m_render_system->draw(helium::system::EDrawMode::TriangleList, 0, skybox.drawcount);
	};
};