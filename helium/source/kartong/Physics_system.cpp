#include "stdafx.h"
#include <kartong/Physics_system.hpp>
namespace kartong {
	using namespace helium;
	

	const float Physics_system::m_gravity = 9.82f;
	const float Physics_system::m_friction = 0.9f;
	Physics_system::Physics_system() {
	}

	Physics_system::~Physics_system() {

	}

	Vector3 Physics_system::Apply_gravity(Vector3 movement, float p_Deltatime) {
		return Vector3(movement.m_x, movement.m_y - m_gravity * p_Deltatime, movement.m_z);
	}

	Vector3 Physics_system::Apply_friction(Vector3 movement) {
		return movement * m_friction;
	}
}