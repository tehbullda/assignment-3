#include "stdafx.h"


namespace kartong
{
	namespace input
	{
		Input::Input()
		{
			m_XB = input::XBOXKeyBindings::Create();
			m_KBM = input::KBMKeyBindings::Create();
		};
		Input::~Input()
		{

		};
		Input::Ptr Input::Create()
		{
			return Input::Ptr(new Input());
		};

		void Input::KBM_BindKey(int p_bind, int p_KeyCode)
		{
			m_KBM->BindKey(p_bind, p_KeyCode);
		};
		void Input::KBM_ClearBind(int p_KeyCode)
		{
			m_KBM->ClearBind(p_KeyCode);
		};
		void Input::KBM_ClearAll()
		{
			m_KBM->ClearAll();
		};
		void Input::KBM_SetDefaultBinds()
		{
			KBM_BindKey(input::actions::UP, 'W');
			KBM_BindKey(input::actions::DOWN, 'S');
			KBM_BindKey(input::actions::LEFT, 'A');
			KBM_BindKey(input::actions::RIGHT, 'D');
			KBM_BindKey(input::actions::RELOAD, 'R');
			KBM_BindKey(input::actions::PAUSE, VK_ESCAPE);
			KBM_BindKey(input::actions::JUMP, VK_SPACE);
			KBM_BindKey(input::actions::SHOOT, VK_LBUTTON);
			KBM_BindKey(input::actions::CROUCH, VK_CONTROL);
			KBM_BindKey(input::actions::SIGHT, VK_RBUTTON);
			helium::Debug::write(helium::EDebugLevel::Info, "KBM-Binds are set to Default");
		};
		void Input::XB_SetControllerScheme(int p_Scheme)
		{
			m_XB->SetControllerScheme(p_Scheme);
		};
		int Input::XB_GetControllerScheme()
		{
			return m_XB->GetControllerScheme();
		};

		float Input::GetAxis(int p_Axis)
		{
			return m_XB->GetAxis(p_Axis);
		};
		void Input::Refresh()
		{
			m_KBM->Refresh();
			m_XB->Refresh();
		};

		bool Input::GetAction(int p_Action)
		{
			if (m_KBM->GetAction(p_Action) || m_XB->GetAction(p_Action))
				return true;
			return false;
		};
		bool Input::GetActionOnce(int p_Action)
		{
			if (m_KBM->GetActionOnce(p_Action) || m_XB->GetActionOnce(p_Action))
				return true;
			return false;
		};
		bool Input::GetActionReleased(int p_Action)
		{
			if (m_KBM->GetActionReleased(p_Action))
				return true;
			return false;
		};

		//-------------------
		//XBOX---------------
		//-------------------
		XBOXKeyBindings::XBOXKeyBindings()
		{
			m_xcontroller = input::XboxController::Create(0.05f, 0.05f);
		};
		XBOXKeyBindings::~XBOXKeyBindings()
		{

		};
		XBOXKeyBindings::Ptr XBOXKeyBindings::Create()
		{
			return XBOXKeyBindings::Ptr(new XBOXKeyBindings());
		}

		void XBOXKeyBindings::BindAxis(int p_bind, WORD p_button)
		{
			m_Binds.insert(std::pair<int, WORD>(p_bind, p_button));
		};
		void XBOXKeyBindings::BindButton(int p_bind, WORD p_button)
		{
			m_Binds.insert(std::pair<int, WORD>(p_bind, p_button));
		};
		void XBOXKeyBindings::Refresh()
		{
			m_xcontroller->Refresh();
		};

		int XBOXKeyBindings::GetControllerScheme()
		{
			return m_CurrentScheme;
		}
		bool XBOXKeyBindings::GetAction(int p_Key)
		{
			if (m_xcontroller->CheckConnection())
				//if (m_xcontroller->GetPort() == 0)
				return m_xcontroller->IsPressed(m_Binds.find(p_Key)->second);
			return false;
		};
		bool XBOXKeyBindings::GetActionOnce(int p_Key)
		{
			if (m_xcontroller->CheckConnection())
				//if (m_xcontroller->GetPort() == 1)
				return m_xcontroller->IsPressedOnce(m_Binds.find(p_Key)->second);
			return false;

		};
		float XBOXKeyBindings::GetAxis(int p_Axis)
		{
			if (m_xcontroller->CheckConnection())
				//if (m_xcontroller->GetPort() == 1)
				switch (p_Axis)
			{
				case axes::LX:
					return (m_xcontroller->m_LeftStickX);
				case axes::LY:
					return (m_xcontroller->m_LeftStickY);
				case axes::RX:
					return (m_xcontroller->m_RightStickX);
				case axes::RY:
					if (!InvertRY)
						return (m_xcontroller->m_RightStickY);
					return -(m_xcontroller->m_RightStickY);
				case axes::LT:
					return (m_xcontroller->m_LeftTrigger);
					break;
				case axes::RT:
					return (m_xcontroller->m_RightTrigger);
				default:
					return 0.0f;
			}
			return 0.0f;
		};

		void XBOXKeyBindings::SetControllerScheme(int p_Scheme)
		{
			m_CurrentScheme = p_Scheme;
			switch (p_Scheme)
			{
			case NORMAL:
				m_Binds.clear();
				BindButton(actions::JUMP, XINPUT_GAMEPAD_A);
				BindButton(actions::CROUCH, XINPUT_GAMEPAD_B);
				BindButton(actions::SHOOT, XINPUT_GAMEPAD_RIGHT_SHOULDER);
				BindButton(actions::SIGHT, XINPUT_GAMEPAD_LEFT_SHOULDER);
				BindButton(actions::UP, XINPUT_GAMEPAD_DPAD_UP);
				BindButton(actions::DOWN, XINPUT_GAMEPAD_DPAD_DOWN);
				BindButton(actions::LEFT, XINPUT_GAMEPAD_DPAD_LEFT);
				BindButton(actions::RIGHT, XINPUT_GAMEPAD_DPAD_RIGHT);
				BindButton(actions::RELOAD, XINPUT_GAMEPAD_X);
				BindButton(actions::PAUSE, XINPUT_GAMEPAD_START);
				InvertRY = false;
				helium::Debug::write(helium::EDebugLevel::Info, "XBOXController: NORMAL");
				break;
			case SPECIAL:
				m_Binds.clear();
				BindButton(actions::JUMP, XINPUT_GAMEPAD_Y);
				BindButton(actions::CROUCH, XINPUT_GAMEPAD_B);
				BindButton(actions::SHOOT, XINPUT_GAMEPAD_RIGHT_SHOULDER);
				BindButton(actions::SIGHT, XINPUT_GAMEPAD_LEFT_SHOULDER);
				BindButton(actions::UP, XINPUT_GAMEPAD_DPAD_UP);
				BindButton(actions::DOWN, XINPUT_GAMEPAD_DPAD_DOWN);
				BindButton(actions::LEFT, XINPUT_GAMEPAD_DPAD_LEFT);
				BindButton(actions::RIGHT, XINPUT_GAMEPAD_DPAD_RIGHT);
				BindButton(actions::RELOAD, XINPUT_GAMEPAD_X);
				BindButton(actions::PAUSE, XINPUT_GAMEPAD_START);
				InvertRY = false;
				helium::Debug::write(helium::EDebugLevel::Info, "XBOXController: SPECIAL");
				break;
			case NORMALINV:
				m_Binds.clear();
				BindButton(actions::JUMP, XINPUT_GAMEPAD_A);
				BindButton(actions::CROUCH, XINPUT_GAMEPAD_B);
				BindButton(actions::SHOOT, XINPUT_GAMEPAD_RIGHT_SHOULDER);
				BindButton(actions::SIGHT, XINPUT_GAMEPAD_LEFT_SHOULDER);
				BindButton(actions::UP, XINPUT_GAMEPAD_DPAD_UP);
				BindButton(actions::DOWN, XINPUT_GAMEPAD_DPAD_DOWN);
				BindButton(actions::LEFT, XINPUT_GAMEPAD_DPAD_LEFT);
				BindButton(actions::RIGHT, XINPUT_GAMEPAD_DPAD_RIGHT);
				BindButton(actions::RELOAD, XINPUT_GAMEPAD_X);
				BindButton(actions::PAUSE, XINPUT_GAMEPAD_START);
				InvertRY = true;
				helium::Debug::write(helium::EDebugLevel::Info, "XBOXController: NORMALINV");
				break;
			case SPECIALINV:
				BindButton(actions::JUMP, XINPUT_GAMEPAD_Y);
				BindButton(actions::CROUCH, XINPUT_GAMEPAD_B);
				BindButton(actions::SHOOT, XINPUT_GAMEPAD_RIGHT_SHOULDER);
				BindButton(actions::SIGHT, XINPUT_GAMEPAD_LEFT_SHOULDER);
				BindButton(actions::UP, XINPUT_GAMEPAD_DPAD_UP);
				BindButton(actions::DOWN, XINPUT_GAMEPAD_DPAD_DOWN);
				BindButton(actions::LEFT, XINPUT_GAMEPAD_DPAD_LEFT);
				BindButton(actions::RIGHT, XINPUT_GAMEPAD_DPAD_RIGHT);
				BindButton(actions::RELOAD, XINPUT_GAMEPAD_X);
				BindButton(actions::PAUSE, XINPUT_GAMEPAD_START);
				InvertRY = true;
				helium::Debug::write(helium::EDebugLevel::Info, "XBOXController: SPECIALINV");
				break;
			default:
				break;
			}
		};
		//-------------------
		//KBM----------------
		//-------------------
		KBMKeyBindings::KBMKeyBindings()
		{
			for (int i = 0; i < 256; i++)
			{
				current[i] = false;
				previous[i] = false;
			}
		};
		KBMKeyBindings::~KBMKeyBindings()
		{

		};
		KBMKeyBindings::Ptr KBMKeyBindings::Create()
		{
			return KBMKeyBindings::Ptr(new KBMKeyBindings());
		}
		void KBMKeyBindings::BindKey(int p_bind, int p_KeyCode)
		{
			m_Binds.insert(std::pair<int, WORD>(p_bind, p_KeyCode));
		}
		void KBMKeyBindings::ClearBind(int p_KeyCode)
		{
			std::map<int, WORD> temp;
			auto it = m_Binds.begin();
			while (it != m_Binds.end())
			{
				if (it->second != p_KeyCode)
					temp.insert(std::pair<int, int>(it->first, it->second));
				if (it->second == p_KeyCode)
					helium::Debug::write(helium::EDebugLevel::Info, "Bind Cleared");
				it++;
			}
			m_Binds = temp;
			temp.clear();
		};
		void KBMKeyBindings::ClearAll()
		{
			m_Binds.clear();
		};
		bool KBMKeyBindings::GetAction(int p_Key)
		{
			if (GetAsyncKeyState(m_Binds.find(p_Key)->second))
				return true;
			return false;
		};
		bool KBMKeyBindings::GetActionOnce(int p_Key)
		{
			if (current[m_Binds.find(p_Key)->second])
				if (!previous[m_Binds.find(p_Key)->second])
					return true;
			return false;
		};
		bool KBMKeyBindings::GetActionReleased(int p_Key)
		{
			if (!current[m_Binds.find(p_Key)->second])
				if (previous[m_Binds.find(p_Key)->second])
					return true;
			return false;
		};

		void KBMKeyBindings::Refresh()
		{
			for (int i = 0; i < 256; i++)
			{
				previous[i] = current[i];

				if (GetAsyncKeyState(i))
					current[i] = true;
				else
					current[i] = false;
			}
		}
	};
};