#include "stdafx.h"
#include <kartong/Particle_system.hpp>
#include <kartong/Particle.hpp>

using namespace kartong;

ParticleSystem::Ptr ParticleSystem::create(const std::string &particledirectory) {
	return ParticleSystem::Ptr(new ParticleSystem(particledirectory));
}

ParticleSystem::ParticleSystem(const std::string &particledirectory) {
	m_directory = particledirectory;
}

ParticleSystem::~ParticleSystem() {
	for (uint32_t i = 0; i < m_particles.size(); i++) {
		delete m_particles[i];
	}
	m_particles.clear();
}

void ParticleSystem::Update(const Vector3 &playerposition, const float &deltatime) {
	for (uint32_t i = 0; i < m_particles.size(); i++) {
		if (m_particles[i]->HasTimedout()) {
			delete m_particles[i];
			m_particles.erase(m_particles.begin() + i);
			--i; //To not skip a particle we move back a step
		}
		else {
			m_particles[i]->Update(playerposition, deltatime);
		}
	}
}

void ParticleSystem::Draw(scene::Camera* p_Cam) {
	for (uint32_t i = 0; i < m_particles.size(); i++) {
		m_particles[i]->Draw(p_Cam);
	}
}

void ParticleSystem::CreateParticle(const std::string &texture, const Vector3 &position, const Vector3 &size, const float &speed, const float &lifetime, EDirection direction) {
	m_particles.push_back(new Particle(m_directory + texture, position, size, speed, lifetime, direction));
}