// application.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/os_event.hpp>
#include <helium/example/example_game.hpp>
#include <kartong/state_menu.hpp>
#include <application.hpp>

using namespace helium;

Application::Application()
{
	m_running = false;
}

Application::~Application()
{
}

bool Application::initialize(HWND window_handle, int width, int height)
{
	// create os event dispatcher and set service locator
	// this is used by all other systems to receive imporant events
	m_event_dispatcher = OSEventDispatcher::create();
	ServiceLocator<OSEventDispatcher>::set_service(m_event_dispatcher.get());

	// create core systems
	m_render_system = system::RenderSystem::create(window_handle, width, height);
	m_audio_system = system::AudioSystem::create();
	m_collision_system = system::CollisionSystem::create();
	m_CollisionManager = kartong::CollisionManager::Create();
	
	m_resource_cache = resource::ResourceCache::create();
	//m_state_manager = gameplay::StateManager::create();
	//m_state_manager->set_factory(gameplay::StateFactory::create());
	//uint32_t string_hash[] =
	//{
	//	String::hash32("menu", 4),
	//	//String::hash32("game", 4)
	//};
	//gameplay::StateFactory *factory = m_state_manager->get_factory();
	//factory->attach<StateMenu>(string_hash[0]);
	//factory->attach<example::Game>(string_hash[1]);
	// default settings
	m_resource_cache->set_directory("../data/");

	// service locators
	ServiceLocator<system::RenderSystem>::set_service(m_render_system.get());
	ServiceLocator<system::AudioSystem>::set_service(m_audio_system.get());
	ServiceLocator<system::CollisionSystem>::set_service(m_collision_system.get());
	ServiceLocator<resource::ResourceCache>::set_service(m_resource_cache.get());
	ServiceLocator<kartong::CollisionManager>::set_service(m_CollisionManager.get());
	
	// game
	m_game = example::Game::create();
	m_game->initialize();
	//m_state_manager->set_state(String::hash32("menu", 4));

	// attach this to messages
	m_event_dispatcher->attach(this);

	Debug::write(EDebugLevel::Info, "application: initialization complete.");

	return m_running = true;
}

void Application::shutdown()
{
	// cleanup
	m_game->shutdown();
	m_event_dispatcher->detach(this);

	Debug::write(EDebugLevel::Info, "application: shutdown complete.");
}

bool Application::process()
{
	m_event_dispatcher->process();
	m_game->process();
	return m_running;
}

// private
void Application::notify(OSEvent* event)
{
	if (event->m_type == EOSEventType::Close)
		m_running = false;
}
