// message_dispatcher.cpp

#include "stdafx.h"
#include <helium/message.hpp>
#include <helium/message_dispatcher.hpp>

namespace helium
{
	// static 
	MessageDispatcher::Ptr MessageDispatcher::create()
	{
		return MessageDispatcher::Ptr(new MessageDispatcher);
	}

	// private
	MessageDispatcher::MessageDispatcher()
	{
	}

	// public
	MessageDispatcher::~MessageDispatcher()
	{
	}

	void MessageDispatcher::notify(Message* message)
	{
		(void)message;
	}
}
