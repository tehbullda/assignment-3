// render_system.cpp

#include "stdafx.h"
#include <cassert>
#include <d3d11.h>
#pragma comment(lib, "d3d11.lib")
#pragma comment (lib, "dxguid.lib")
#include <D3DCompiler.h>
#pragma comment (lib, "d3dcompiler.lib")
#include <helium/system/render_system.hpp>

#define SAFE_RELEASE(x) if(x) { x->Release(); x = nullptr; }

#pragma warning(disable:4100) // unreferenced formal parameter

namespace helium
{
	namespace system
	{
		// render system resources
		struct Constant
		{
#if !defined(NDEBUG)
			std::string m_name;
#endif
			uint32_t m_hash;
			char* m_vs_data;
			char* m_ps_data;
			int m_vs_index;
			int m_ps_index;
			uint32_t m_size;
		};

		struct Sampler
		{
#if !defined(NDEBUG)
			std::string m_name;
#endif
			uint32_t m_hash;
			int m_vs_index;
			int m_ps_index;
		};

		struct RenderSystem::Shader
		{
			ID3D11VertexShader* m_vertex_shader;
			ID3D11PixelShader* m_pixel_shader;
			ID3DBlob* m_signature;

			uint32_t m_num_vs_constants;
			ID3D11Buffer** m_vs_constants;
			uint32_t m_num_ps_constants;
			ID3D11Buffer** m_ps_constants;

			uint32_t m_num_constants;
			Constant* m_constants;

			uint32_t m_num_samplers;
			Sampler* m_samplers;

			uint32_t m_num_textures;
			Sampler* m_textures;

			char** m_vs_mem;
			char** m_ps_mem;
			bool* m_vs_dirty;
			bool* m_ps_dirty;
		};

		struct RenderSystem::Texture
		{
			union
			{
				ID3D11Texture1D* texture1d;
				ID3D11Texture2D* texture2d;
				ID3D11Texture3D* texture3d;
			} m_texture;
			ETextureType m_type;
			ID3D11ShaderResourceView* m_srv;
			DXGI_FORMAT m_format;
			uint32_t m_width;
			uint32_t m_height;
			uint32_t m_depth;
			uint32_t m_components;
		};

		struct RenderSystem::VertexFormat
		{
			ID3D11InputLayout* m_input_layout;
			uint32_t m_vertex_size;
		};

		struct RenderSystem::VertexBuffer
		{
			ID3D11Buffer* m_buffer;
			uint32_t m_vertex_size;
			uint32_t m_vertex_count;
		};

		struct RenderSystem::IndexBuffer
		{
			ID3D11Buffer* m_buffer;
			uint32_t m_index_size;
			uint32_t m_index_count;
		};

		struct RenderSystem::SamplerState
		{
			ID3D11SamplerState* m_state;
		};

		struct RenderSystem::BlendState
		{
			ID3D11BlendState* m_state;
		};

		struct RenderSystem::DepthState
		{
			ID3D11DepthStencilState* m_state;
		};

		struct RenderSystem::RasterizerState
		{
			ID3D11RasterizerState* m_state;
		};

		// static 
		RenderSystem::Ptr RenderSystem::create(HWND window_handle, int width, int height)
		{
			return RenderSystem::Ptr(new RenderSystem(window_handle, width, height));
		}

		// private
		RenderSystem::RenderSystem(HWND window_handle, int width, int height)
			: m_swap_chain(nullptr)
			, m_device(nullptr)
			, m_context(nullptr)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: initialize");
			m_window = window_handle;
			// create swapchain, device and context
			DWORD flags = D3D11_CREATE_DEVICE_SINGLETHREADED;
#if !defined(NDEBUG)
			flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

			D3D_FEATURE_LEVEL requested_feature_level = D3D_FEATURE_LEVEL_11_0;
			D3D_FEATURE_LEVEL feature_level;

			DXGI_SWAP_CHAIN_DESC sd;
			memset(&sd, 0, sizeof(sd));
			sd.BufferDesc.Width = width;
			sd.BufferDesc.Height = height;
			sd.BufferDesc.RefreshRate.Denominator = 1;
			sd.BufferDesc.RefreshRate.Numerator = 60;
			sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
			sd.SampleDesc.Count = 1;
			sd.SampleDesc.Quality = 0;
			sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			sd.BufferCount = 1;
			sd.OutputWindow = window_handle;
			sd.Windowed = TRUE;
			sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

			HRESULT hr = D3D11CreateDeviceAndSwapChain(
				NULL,
				D3D_DRIVER_TYPE_HARDWARE,
				NULL,
				flags,
				&requested_feature_level,
				1,
				D3D11_SDK_VERSION,
				&sd,
				&m_swap_chain,
				&m_device,
				&feature_level,
				&m_context);
			if (hr != S_OK)
			{
				Debug::fatal("rendersystem: could not create device and swap chain!");
			}
			Debug::write(EDebugLevel::Info, "  created device and swap chain");

			ID3D11Resource* resource = nullptr;
			hr = m_swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&resource);
			if (hr != S_OK)
			{
				Debug::fatal("rendersystem: could not get back buffer!");
			}
			Debug::write(EDebugLevel::Info, "  back buffer aquired");

			hr = m_device->CreateRenderTargetView(resource, NULL, &m_rtv);
			if (hr != S_OK)
			{
				Debug::fatal("rendersystem: could not create render target view!");
			}
			resource->Release();
			Debug::write(EDebugLevel::Info, "  created render target view");

			// depth stencil
			D3D11_TEXTURE2D_DESC desc;
			desc.Width = width;
			desc.Height = height;
			desc.MipLevels = 1;
			desc.ArraySize = 1;
			desc.Format = DXGI_FORMAT_D32_FLOAT;
			desc.SampleDesc.Count = 1;
			desc.SampleDesc.Quality = 0;
			desc.Usage = D3D11_USAGE_DEFAULT;
			desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			desc.CPUAccessFlags = 0;
			desc.MiscFlags = 0;

			hr = m_device->CreateTexture2D(&desc, NULL, &m_depth_buffer);
			if (hr != S_OK)
			{
				Debug::fatal("rendersystem: could not create depth stencil buffer!");
			}
			Debug::write(EDebugLevel::Info, "  created depth stencil buffer");

			hr = m_device->CreateDepthStencilView(m_depth_buffer, NULL, &m_dsv);
			if (hr != S_OK)
			{
				Debug::fatal("rendersystem: could not create depth stencil view!");
			}
			Debug::write(EDebugLevel::Info, "  created depth stencil view");

			m_context->OMSetRenderTargets(1, &m_rtv, m_dsv);

			D3D11_VIEWPORT vp;
			vp.Width = width;
			vp.Height = height;
			vp.TopLeftX = 0;
			vp.TopLeftY = 0;
			vp.MinDepth = 0.0f;
			vp.MaxDepth = 1.0;
			m_context->RSSetViewports(1, &vp);
			Debug::write(EDebugLevel::Info, "  set viewport");

			// reset internal state
			m_selected_shader = m_current_shader = (uint32_t)INVALID_RESOURCE;
			m_selected_vertex_format = m_current_vertex_format = (uint32_t)INVALID_RESOURCE;
			m_selected_vertex_buffer = m_current_vertex_buffer = (uint32_t)INVALID_RESOURCE;
			m_selected_index_buffer = m_current_index_buffer = (uint32_t)INVALID_RESOURCE;
			m_selected_blend_state = m_current_blend_state = (uint32_t)INVALID_RESOURCE;
			m_selected_depth_state = m_current_depth_state = (uint32_t)INVALID_RESOURCE;
			m_selected_rasterizer_state = m_current_rasterizer_state = (uint32_t)INVALID_RESOURCE;
			for (uint32_t i = 0; i < MAX_TEXTURE_UNITS; i++)
			{
				m_selected_texture[i] = m_current_texture[i] = (uint32_t)INVALID_RESOURCE;
				m_selected_sampler_state[i] = m_current_sampler_state[i] = (uint32_t)INVALID_RESOURCE;
			}
			m_current_topology = EDrawMode::Invalid;
		}

		// public
		RenderSystem::~RenderSystem()
		{
			// remove all resources
			for (unsigned i = 0; i < m_rasterizer_state.size(); i++)
			{
			}
			m_rasterizer_state.clear();

			for (unsigned i = 0; i < m_depth_states.size(); i++)
			{
			}
			m_depth_states.clear();

			for (unsigned i = 0; i < m_blend_states.size(); i++)
			{
			}
			m_blend_states.clear();

			for (unsigned i = 0; i < m_sampler_states.size(); i++)
			{
			}
			m_sampler_states.clear();

			for (unsigned i = 0; i < m_index_buffers.size(); i++)
			{
			}
			m_index_buffers.clear();

			for (unsigned i = 0; i < m_vertex_buffers.size(); i++)
			{
				SAFE_RELEASE(m_vertex_buffers[i].m_buffer);
				m_vertex_buffers[i].m_vertex_count = 0;
				m_vertex_buffers[i].m_vertex_size = 0;
			}
			m_vertex_buffers.clear();

			for (unsigned i = 0; i < m_vertex_formats.size(); i++)
			{
				SAFE_RELEASE(m_vertex_formats[i].m_input_layout);
				m_vertex_formats[i].m_vertex_size = 0;
			}
			m_vertex_formats.clear();

			for (unsigned i = 0; i < m_textures.size(); i++)
			{
			}
			m_textures.clear();

			for (unsigned i = 0; i < m_shaders.size(); i++)
			{
				if (!m_shaders[i].m_signature)
					continue;

				SAFE_RELEASE(m_shaders[i].m_vertex_shader);
				SAFE_RELEASE(m_shaders[i].m_pixel_shader);
				SAFE_RELEASE(m_shaders[i].m_signature);

				delete[] m_shaders[i].m_constants;
				m_shaders[i].m_constants = nullptr;

				delete[] m_shaders[i].m_samplers;
				m_shaders[i].m_samplers = nullptr;

				delete[] m_shaders[i].m_textures;
				m_shaders[i].m_textures = nullptr;

				for (unsigned k = 0; k < m_shaders[i].m_num_vs_constants; k++)
				{
					SAFE_RELEASE(m_shaders[i].m_vs_constants[k]);
					delete[] m_shaders[i].m_vs_mem[k];
					m_shaders[i].m_vs_mem[k] = nullptr;
				}
				delete[] m_shaders[i].m_vs_mem;
				m_shaders[i].m_vs_mem = nullptr;
				delete[] m_shaders[i].m_vs_constants;
				m_shaders[i].m_vs_constants = nullptr;
				delete[] m_shaders[i].m_vs_dirty;
				m_shaders[i].m_vs_dirty = nullptr;

				for (unsigned k = 0; k < m_shaders[i].m_num_ps_constants; k++)
				{
					SAFE_RELEASE(m_shaders[i].m_ps_constants[k]);
					delete[] m_shaders[i].m_ps_mem[k];
					m_shaders[i].m_ps_mem[k] = nullptr;
				}
				delete[] m_shaders[i].m_ps_mem;
				m_shaders[i].m_ps_mem = nullptr;
				delete[] m_shaders[i].m_ps_constants;
				m_shaders[i].m_ps_constants = nullptr;
				delete[] m_shaders[i].m_ps_dirty;
				m_shaders[i].m_ps_dirty = nullptr;
			}
			m_shaders.clear();

			SAFE_RELEASE(m_dsv);
			SAFE_RELEASE(m_depth_buffer);
			SAFE_RELEASE(m_rtv);
			SAFE_RELEASE(m_context);
			SAFE_RELEASE(m_device);
			SAFE_RELEASE(m_swap_chain);

			Debug::write(EDebugLevel::Info, "rendersystem: shutdown");
		}

		void RenderSystem::clear()
		{
			static FLOAT color[] = { 0.055f, 0.055f, 0.055f, 1.0f };
			m_context->ClearRenderTargetView(m_rtv, color);
			m_context->ClearDepthStencilView(m_dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
		}

		void RenderSystem::present()
		{
			m_swap_chain->Present(0, 0);

			reset_state();
		}

		HWND RenderSystem::get_window() {
			return m_window;
		}

		bool RenderSystem::get_fullscreen() {
			BOOL fullscreen;
			IDXGIOutput *output;
			m_swap_chain->GetFullscreenState(&fullscreen, &output);
			return fullscreen == TRUE ? true : false;
		}

		//Credit to @Enfisk for these methods
		void RenderSystem::update_vertex_buffer(const uint32_t &p_vertex_buffer_id, const void* data, const uint32_t &bytesize)
		{
			//Debug::write(EDebugLevel::Info, "rendersystem: update vertex buffer");
			VertexBuffer &vertex_buffer = m_vertex_buffers[p_vertex_buffer_id];
			D3D11_BUFFER_DESC desc;
			vertex_buffer.m_buffer->GetDesc(&desc);

			if (desc.Usage != D3D11_USAGE_DYNAMIC)
			{
				Debug::write(EDebugLevel::Error, "rendersystem: requested vertex buffer is not dynamic");
				return;
			}

			D3D11_MAPPED_SUBRESOURCE sr;

			HRESULT hr = m_context->Map(vertex_buffer.m_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &sr);

			if (hr != S_OK)
			{
				Debug::write(EDebugLevel::Error, "rendersystem: couldn't map vertexbuffer");
				return;
			}

			memcpy(sr.pData, data, bytesize);

			m_context->Unmap(vertex_buffer.m_buffer, 0);
		}

		void RenderSystem::update_index_buffer(const uint32_t &p_index_buffer_id, const void* data, const uint32_t &data_size)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: update index buffer");
			IndexBuffer &index_buffer = m_index_buffers[p_index_buffer_id];
			index_buffer.m_index_count = (data_size / index_buffer.m_index_size);
			D3D11_BUFFER_DESC desc;
			index_buffer.m_buffer->GetDesc(&desc);

			if (desc.Usage != D3D11_USAGE_DYNAMIC)
			{
				Debug::write(EDebugLevel::Error, "rendersystem: requested index buffer is not dynamic");
				return;
			}

			D3D11_MAPPED_SUBRESOURCE sr;

			HRESULT hr = m_context->Map(index_buffer.m_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &sr);

			if (hr != S_OK)
			{
				Debug::write(EDebugLevel::Error, "rendersystem: couldn't map indexbuffer");
				return;
			}

			memcpy(sr.pData, data, data_size);

			m_context->Unmap(index_buffer.m_buffer, 0);
		}



		void RenderSystem::select_shader(uint32_t shader_id)
		{
			m_selected_shader = shader_id;
		}

		void RenderSystem::select_texture(uint32_t texture_id, uint32_t unit)
		{
			assert(unit < MAX_TEXTURE_UNITS);
			m_selected_texture[unit] = texture_id;
		}

		void RenderSystem::select_vertex_format(uint32_t vertex_format_id)
		{
			m_selected_vertex_format = vertex_format_id;
		}

		void RenderSystem::select_vertex_buffer(uint32_t vertex_buffer_id)
		{
			m_selected_vertex_buffer = vertex_buffer_id;
		}

		void RenderSystem::select_index_buffer(uint32_t index_buffer_id)
		{
			m_selected_index_buffer = index_buffer_id;
		}

		void RenderSystem::select_sampler_state(uint32_t sampler_state_id, uint32_t unit)
		{
			assert(unit < MAX_TEXTURE_UNITS);
			m_selected_sampler_state[unit] = sampler_state_id;
		}

		void RenderSystem::select_blend_state(uint32_t blend_state_id)
		{
			m_selected_blend_state = blend_state_id;
		}

		void RenderSystem::select_depth_state(uint32_t depth_state_id)
		{
			m_selected_depth_state = depth_state_id;
		}

		void RenderSystem::select_rasterizer_state(uint32_t rasterizer_state_id)
		{
			m_selected_rasterizer_state = rasterizer_state_id;
		}

		void RenderSystem::set_shader_constant_float(const char* name, const float value)
		{
			set_shader_constant_raw(String::hash32(name, static_cast<uint32_t>(strlen(name))), &value, sizeof(float));
		}

		void RenderSystem::set_shader_constant_vector2(const char* name, const Vector2& value)
		{
			set_shader_constant_raw(String::hash32(name, static_cast<uint32_t>(strlen(name))), &value, sizeof(float)* 2);
		}

		void RenderSystem::set_shader_constant_vector3(const char* name, const Vector3& value)
		{
			set_shader_constant_raw(String::hash32(name, static_cast<uint32_t>(strlen(name))), &value, sizeof(float)* 3);
		}

		void RenderSystem::set_shader_constant_vector4(const char* name, const Vector4& value)
		{
			set_shader_constant_raw(String::hash32(name, static_cast<uint32_t>(strlen(name))), &value, sizeof(float)* 4);
		}

		void RenderSystem::set_shader_constant_matrix4(const char* name, const Matrix4& value)
		{
			set_shader_constant_raw(String::hash32(name, static_cast<uint32_t>(strlen(name))), &value, sizeof(float)* 16);
		}

		void RenderSystem::set_shader_constant_raw(const char* name, const void* value, uint32_t size)
		{
			set_shader_constant_raw(String::hash32(name, static_cast<uint32_t>(strlen(name))), value, size);
		}

		void RenderSystem::set_shader_constant_raw(const uint32_t hash, const void* value, uint32_t size)
		{
			assert(m_selected_shader != INVALID_RESOURCE);

			Shader& shader = m_shaders[m_selected_shader];
			assert(shader.m_vertex_shader);

			for (unsigned i = 0; i < shader.m_num_constants; i++)
			{
				if (shader.m_constants[i].m_hash == hash)
				{
					if (shader.m_constants[i].m_vs_data)
					{
						if (memcmp(shader.m_constants[i].m_vs_data, value, size) != 0)
						{
							memcpy(shader.m_constants[i].m_vs_data, value, size);
							shader.m_vs_dirty[shader.m_constants[i].m_vs_index] = true;
						}
					}
					if (shader.m_constants[i].m_ps_data)
					{
						if (memcmp(shader.m_constants[i].m_ps_data, value, size) != 0)
						{
							memcpy(shader.m_constants[i].m_ps_data, value, size);
							shader.m_ps_dirty[shader.m_constants[i].m_ps_index] = true;
						}
					}
					break;
				}
			}
		}

		void RenderSystem::apply()
		{
			apply_shader();
			apply_texture_and_sampler();
			apply_vertex_format();
			apply_vertex_buffer();
			apply_index_buffer();
			apply_blend_state();
			apply_depth_state();
			apply_rasterizer_state();
		}

		static const D3D11_PRIMITIVE_TOPOLOGY modes[] = {
			D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
			D3D11_PRIMITIVE_TOPOLOGY_LINELIST,
			D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,
			D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
			D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,
			D3D_PRIMITIVE_TOPOLOGY_UNDEFINED,
		};
		void RenderSystem::draw(EDrawMode mode, uint32_t start, uint32_t count)
		{
			if (m_current_topology != mode)
			{
				m_current_topology = mode;
				m_context->IASetPrimitiveTopology(modes[(uint32_t)m_current_topology]);
			}
			m_context->Draw(count, start);
		}

		void RenderSystem::drawi(EDrawMode mode, uint32_t start, uint32_t count)
		{
			if (m_current_topology != mode)
			{
				m_current_topology = mode;
				m_context->IASetPrimitiveTopology(modes[(uint32_t)mode]);
			}
			m_context->DrawIndexed(count, start, 0);
		}

#pragma warning(push)
#pragma warning(disable:4701)
		uint32_t RenderSystem::create_shader(const char* vertex_src, const char* pixel_src)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: create shader");

			//HRESULT hr = S_OK;
			ID3DBlob* shblob = nullptr, *error = nullptr;
			ID3D11ShaderReflection* vs_ref = nullptr, *ps_ref = nullptr;

			Shader shader;
			memset(&shader, 0, sizeof(shader));

			if (D3DCompile(
				vertex_src,
				strlen(vertex_src),
				NULL, NULL, NULL,
				"main", "vs_5_0",
				D3DCOMPILE_ENABLE_STRICTNESS
#if !defined(NDEBUG)
				| D3DCOMPILE_DEBUG
#endif
				, 0,
				&shblob,
				&error) == S_OK)
			{
				if (m_device->CreateVertexShader(
					shblob->GetBufferPointer(),
					shblob->GetBufferSize(),
					NULL,
					&shader.m_vertex_shader) == S_OK)
				{
					D3DReflect(shblob->GetBufferPointer(), shblob->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&vs_ref);
					D3DGetInputSignatureBlob(shblob->GetBufferPointer(), shblob->GetBufferSize(), &shader.m_signature);
				}
			}
			else
			{
				Debug::write(EDebugLevel::Error, "rendersystem: could not compile vertex shader source");
				Debug::fatal(String::format("vertex shader error: %s", (char*)error->GetBufferPointer()));
			}
			SAFE_RELEASE(shblob);
			SAFE_RELEASE(error);

			if (D3DCompile(
				pixel_src,
				strlen(pixel_src),
				NULL, NULL, NULL,
				"main", "ps_5_0",
				D3DCOMPILE_ENABLE_STRICTNESS
#if !defined(NDEBUG)
				| D3DCOMPILE_DEBUG
#endif
				, 0,
				&shblob,
				&error) == S_OK)
			{
				if (m_device->CreatePixelShader(
					shblob->GetBufferPointer(),
					shblob->GetBufferSize(),
					NULL,
					&shader.m_pixel_shader) == S_OK)
				{
					D3DReflect(shblob->GetBufferPointer(), shblob->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&ps_ref);
				}
			}
			else
			{
				Debug::write(EDebugLevel::Error, "rendersystem: could not compile pixel shader source");
				Debug::fatal(String::format("pixel shader error: %s", (char*)error->GetBufferPointer()));
			}
			SAFE_RELEASE(shblob);
			SAFE_RELEASE(error);

			D3D11_SHADER_DESC vs_desc;
			if (vs_ref)
			{
				vs_ref->GetDesc(&vs_desc);
				if (vs_desc.ConstantBuffers > 0)
				{
					shader.m_num_vs_constants = vs_desc.ConstantBuffers;
					shader.m_vs_constants = new ID3D11Buffer*[shader.m_num_vs_constants];
					shader.m_vs_mem = new char*[shader.m_num_vs_constants];
					shader.m_vs_dirty = new bool[shader.m_num_vs_constants];
				}
			}

			D3D11_SHADER_DESC ps_desc;
			if (ps_ref)
			{
				ps_ref->GetDesc(&ps_desc);
				if (ps_desc.ConstantBuffers > 0)
				{
					shader.m_num_ps_constants = ps_desc.ConstantBuffers;
					shader.m_ps_constants = new ID3D11Buffer*[shader.m_num_ps_constants];
					shader.m_ps_mem = new char*[shader.m_num_ps_constants];
					shader.m_ps_dirty = new bool[shader.m_num_ps_constants];
				}
			}

			D3D11_BUFFER_DESC cb_desc;
			cb_desc.Usage = D3D11_USAGE_DEFAULT;
			cb_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			cb_desc.CPUAccessFlags = 0;
			cb_desc.MiscFlags = 0;

			std::vector<Constant> constants;
			for (unsigned i = 0; i < shader.m_num_vs_constants; i++)
			{
				D3D11_SHADER_BUFFER_DESC sb_desc;
				vs_ref->GetConstantBufferByIndex(i)->GetDesc(&sb_desc);

				cb_desc.ByteWidth = sb_desc.Size;
				m_device->CreateBuffer(&cb_desc, NULL, &shader.m_vs_constants[i]);
				shader.m_vs_mem[i] = new char[sb_desc.Size];
				for (unsigned k = 0; k < sb_desc.Variables; k++)
				{
					D3D11_SHADER_VARIABLE_DESC var_desc;
					vs_ref->GetConstantBufferByIndex(i)->GetVariableByIndex(k)->GetDesc(&var_desc);

					Constant constant;
#if !defined(NDEBUG)
					constant.m_name = var_desc.Name;
#endif
					constant.m_hash = String::hash32(var_desc.Name, (uint32_t)strlen(var_desc.Name));
					constant.m_vs_data = shader.m_vs_mem[i] + var_desc.StartOffset;
					constant.m_vs_index = i;
					constant.m_ps_data = nullptr;
					constant.m_ps_index = -1;
					constant.m_size = var_desc.Size;
					constants.push_back(constant);
				}
				shader.m_vs_dirty[i] = false;
			}

			unsigned num_constants = (uint32_t)constants.size();
			for (unsigned i = 0; i < shader.m_num_ps_constants; i++)
			{
				D3D11_SHADER_BUFFER_DESC sb_desc;
				ps_ref->GetConstantBufferByIndex(i)->GetDesc(&sb_desc);

				cb_desc.ByteWidth = sb_desc.Size;
				m_device->CreateBuffer(&cb_desc, NULL, &shader.m_ps_constants[i]);
				shader.m_ps_mem[i] = new char[sb_desc.Size];
				for (unsigned k = 0; k < sb_desc.Variables; k++)
				{
					D3D11_SHADER_VARIABLE_DESC var_desc;
					ps_ref->GetConstantBufferByIndex(i)->GetVariableByIndex(k)->GetDesc(&var_desc);

					int index = -1;
					for (unsigned j = 0; j < num_constants; j++)
					{
#if !defined(NDEBUG)
						if (strcmp(constants[j].m_name.c_str(), var_desc.Name) == 0)
#else	
						uint32_t hash = String::hash32(var_desc.Name, (uint32_t)strlen(var_desc.Name));
						if (constants[j].m_hash == hash)
#endif
						{
							index = j;
							break;
						}
					}
					if (index != -1)
					{
						constants[index].m_ps_data = shader.m_ps_mem[i] + var_desc.StartOffset;
						constants[index].m_ps_index = i;
					}
					else
					{
						Constant constant;
#if !defined(NDEBUG)
						constant.m_name = var_desc.Name;
#endif
						constant.m_hash = String::hash32(var_desc.Name, (uint32_t)strlen(var_desc.Name));
						constant.m_vs_data = nullptr;
						constant.m_vs_index = -1;
						constant.m_ps_data = shader.m_ps_mem[i] + var_desc.StartOffset;
						constant.m_ps_index = i;
						constant.m_size = var_desc.Size;
						constants.push_back(constant);
					}
				}
			}

			shader.m_num_constants = (uint32_t)constants.size();
			if (shader.m_num_constants > 0)
			{
				shader.m_constants = new Constant[shader.m_num_constants];
				for (unsigned i = 0; i < shader.m_num_constants; i++)
					shader.m_constants[i] = constants[i];
			}
			constants.clear();

			int num_resources = 0;
			num_resources += vs_ref ? vs_desc.BoundResources : 0;
			num_resources += ps_ref ? ps_desc.BoundResources : 0;
			if (num_resources > 0)
			{
				std::vector<Sampler> samplers, textures;

				for (unsigned i = 0; i < vs_desc.BoundResources; i++)
				{
					D3D11_SHADER_INPUT_BIND_DESC sib_desc;
					vs_ref->GetResourceBindingDesc(i, &sib_desc);
					if (sib_desc.Type == D3D_SIT_SAMPLER)
					{
						Sampler sampler;
#if !defined(NDEBUG)
						sampler.m_name = sib_desc.Name;
#endif
						sampler.m_hash = String::hash32(sib_desc.Name, (uint32_t)strlen(sib_desc.Name));
						sampler.m_vs_index = i;
						sampler.m_ps_index = -1;
						samplers.push_back(sampler);
					}
					else if (sib_desc.Type == D3D_SIT_TEXTURE)
					{
						Sampler sampler;
#if !defined(NDEBUG)
						sampler.m_name = sib_desc.Name;
#endif
						sampler.m_hash = String::hash32(sib_desc.Name, (uint32_t)strlen(sib_desc.Name));
						sampler.m_vs_index = i;
						sampler.m_ps_index = -1;
						textures.push_back(sampler);
					}
				}

				unsigned max_textures = (uint32_t)textures.size();
				unsigned max_samplers = (uint32_t)samplers.size();
				for (unsigned i = 0; i < ps_desc.BoundResources; i++)
				{
					D3D11_SHADER_INPUT_BIND_DESC sib_desc;
					ps_ref->GetResourceBindingDesc(i, &sib_desc);
					if (sib_desc.Type == D3D_SIT_SAMPLER)
					{
						int index = -1;
						for (unsigned k = 0; k < max_samplers; k++)
						{
#if !defined(NDEBUG)
							if (strcmp(samplers[k].m_name.c_str(), sib_desc.Name) == 0)
#else
							uint32_t hash = String::hash32(sib_desc.Name, (uint32_t)strlen(sib_desc.Name));
							if (samplers[k].m_hash == hash)
#endif
							{
								index = k;
								break;
							}
						}

						if (index != -1)
						{
							samplers[index].m_ps_index = sib_desc.BindPoint;
						}
						else
						{
							Sampler sampler;
#if !defined(NDEBUG)
							sampler.m_name = sib_desc.Name;
#else
							sampler.m_hash = String::hash32(sib_desc.Name, (uint32_t)strlen(sib_desc.Name));
#endif
							sampler.m_vs_index = -1;
							sampler.m_ps_index = sib_desc.BindPoint;
							samplers.push_back(sampler);
						}
					}
					else if (sib_desc.Type == D3D_SIT_TEXTURE)
					{
						int index = -1;
						for (unsigned k = 0; k < max_textures; k++)
						{
#if !defined(NDEBUG)
							if (strcmp(textures[k].m_name.c_str(), sib_desc.Name) == 0)
#else
							uint32_t hash = String::hash32(sib_desc.Name, (uint32_t)strlen(sib_desc.Name));
							if (textures[k].m_hash == hash)
#endif
							{
								index = k;
								break;
							}
						}

						if (index != -1)
						{
							textures[index].m_ps_index = sib_desc.BindPoint;
						}
						else
						{
							Sampler sampler;
#if !defined(NDEBUG)
							sampler.m_name = sib_desc.Name;
#else
							sampler.m_hash = String::hash32(sib_desc.Name, (uint32_t)(uint32_t)strlen(sib_desc.Name));
#endif
							sampler.m_vs_index = -1;
							sampler.m_ps_index = sib_desc.BindPoint;
							textures.push_back(sampler);
						}
					}
				}

				shader.m_num_samplers = (uint32_t)samplers.size();
				if (shader.m_num_samplers > 0)
				{
					shader.m_samplers = new Sampler[shader.m_num_samplers];
					for (unsigned i = 0; i < shader.m_num_samplers; i++)
					{
						shader.m_samplers[i] = samplers[i];
					}
				}
				samplers.clear();

				shader.m_num_textures = (uint32_t)textures.size();
				if (shader.m_num_textures > 0)
				{
					shader.m_textures = new Sampler[shader.m_num_textures];
					for (unsigned i = 0; i < shader.m_num_textures; i++)
					{
						shader.m_textures[i] = textures[i];
					}
				}
				textures.clear();
			}

			SAFE_RELEASE(vs_ref);
			SAFE_RELEASE(ps_ref);

			// find unused element in shader array
			unsigned index = (uint32_t)-1;
			for (unsigned i = 0; i < (uint32_t)m_shaders.size(); i++)
			{
				if (!m_shaders[i].m_signature && m_shaders[i].m_vertex_shader)
				{
					index = i;
					break;
				}
			}
			if (index != -1)
			{
				m_shaders[index] = shader;
				return index;
			}

			m_shaders.push_back(shader);
			return (uint32_t)m_shaders.size() - 1;
		}
#pragma warning(pop)

		void RenderSystem::destroy_shader(uint32_t shader_id)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: destroy shader");

			assert(shader_id != INVALID_RESOURCE);

			Shader& shader = m_shaders[shader_id];
			SAFE_RELEASE(shader.m_vertex_shader);
			SAFE_RELEASE(shader.m_pixel_shader);

			delete[] shader.m_constants;
			shader.m_constants = nullptr;

			delete[] shader.m_textures;
			shader.m_textures = nullptr;

			delete[] shader.m_samplers;
			shader.m_samplers = nullptr;

			for (unsigned i = 0; i < shader.m_num_vs_constants; i++)
			{
				SAFE_RELEASE(shader.m_vs_constants[i]);
				delete[] shader.m_vs_mem[i];
				shader.m_vs_mem[i] = nullptr;
			}
			delete[] shader.m_vs_constants;
			shader.m_vs_constants = nullptr;
			delete[] shader.m_vs_dirty;
			shader.m_vs_dirty = nullptr;

			for (unsigned i = 0; i < shader.m_num_ps_constants; i++)
			{
				SAFE_RELEASE(shader.m_ps_constants[i]);
				delete[] shader.m_ps_mem[i];
				shader.m_ps_mem[i] = nullptr;
			}
			delete[] shader.m_ps_constants;
			shader.m_ps_constants = nullptr;
			delete[] shader.m_ps_dirty;
			shader.m_ps_dirty = nullptr;

			SAFE_RELEASE(shader.m_signature);
		}

		uint32_t RenderSystem::create_texture(ETextureFormat format, ETextureType type, uint32_t width, uint32_t height, uint32_t depth, const void* data)
		{
			const DXGI_FORMAT formats[] =
			{
				DXGI_FORMAT_R8_UNORM,
				DXGI_FORMAT_R8G8_UNORM,
				DXGI_FORMAT_UNKNOWN,
				DXGI_FORMAT_R8G8B8A8_UNORM,
				DXGI_FORMAT_D24_UNORM_S8_UINT,
				DXGI_FORMAT_D32_FLOAT,
			};

			Debug::write(EDebugLevel::Info, "rendersystem: create texture");

			const uint32_t sizes[] = { 1, 2, 0, 4, 4, 4 };
			uint32_t components = sizes[(uint32_t)format];

			Texture texture = { 0 };
			texture.m_width = width;
			texture.m_height = height;
			texture.m_depth = depth;
			texture.m_components = components;

			D3D11_SUBRESOURCE_DATA srdata;
			srdata.pSysMem = data;
			srdata.SysMemPitch = width * components;
			srdata.SysMemSlicePitch = 0;

			ID3D11Resource* resource = nullptr;

			if (type == ETextureType::Texture1D)
			{
				D3D11_TEXTURE1D_DESC desc;
				desc.Width = width;
				desc.Format = formats[(uint32_t)format];
				desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
				desc.Usage = D3D11_USAGE_DEFAULT;
				desc.MipLevels = 1;
				desc.ArraySize = 1;
				desc.CPUAccessFlags = 0;
				desc.MiscFlags = 0;

				ID3D11Texture1D* tex = nullptr;
				HRESULT hr = m_device->CreateTexture1D(&desc, &srdata, &tex);
				if (hr != S_OK)
				{
					Debug::fatal("rendersystem: could not create texture 1d");
					return (uint32_t)INVALID_RESOURCE;
				}

				texture.m_texture.texture1d = tex;
				texture.m_format = desc.Format;
				resource = tex;
			}
			else if (type == ETextureType::Texture2D)
			{
				D3D11_TEXTURE2D_DESC desc;
				desc.Width = width;
				desc.Height = height;
				desc.Format = formats[(uint32_t)format];
				desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
				desc.Usage = D3D11_USAGE_DEFAULT;
				desc.MipLevels = 1;
				desc.ArraySize = 1;
				desc.SampleDesc.Count = 1;
				desc.SampleDesc.Quality = 0;
				desc.CPUAccessFlags = 0;
				desc.MiscFlags = 0;

				ID3D11Texture2D* tex = nullptr;
				HRESULT hr = m_device->CreateTexture2D(&desc, &srdata, &tex);
				if (hr != S_OK)
				{
					Debug::fatal("rendersystem: could not create texture 2d");
					return (uint32_t)INVALID_RESOURCE;
				}

				texture.m_texture.texture2d = tex;
				texture.m_format = desc.Format;
				resource = tex;
			}
			else if (type == ETextureType::Texture3D)
			{
				D3D11_TEXTURE3D_DESC desc;
				desc.Width = width;
				desc.Height = height;
				desc.Depth = depth;
				desc.Format = formats[(uint32_t)format];
				desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
				desc.Usage = D3D11_USAGE_DEFAULT;
				desc.MipLevels = 1;
				desc.CPUAccessFlags = 0;
				desc.MiscFlags = 0;

				ID3D11Texture3D* tex = nullptr;
				HRESULT hr = m_device->CreateTexture3D(&desc, &srdata, &tex);
				if (hr != S_OK)
				{
					Debug::fatal("rendersystem: could not create texture 3d");
					return (uint32_t)INVALID_RESOURCE;
				}

				texture.m_texture.texture3d = tex;
				texture.m_format = desc.Format;
				resource = tex;
			}

			ID3D11ShaderResourceView* srv = nullptr;
			D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
			srv_desc.Format = formats[(uint32_t)format];
			switch (type)
			{
			case ETextureType::Texture1D:
				srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
				srv_desc.Texture1D.MipLevels = 1;
				srv_desc.Texture1D.MostDetailedMip = 0;
				break;
			case ETextureType::Texture2D:
				srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				srv_desc.Texture2D.MipLevels = 1;
				srv_desc.Texture2D.MostDetailedMip = 0;
				break;
			case ETextureType::Texture3D:
				srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
				srv_desc.Texture3D.MipLevels = 1;
				srv_desc.Texture3D.MostDetailedMip = 0;
				break;
			}

			HRESULT hr = m_device->CreateShaderResourceView(resource, &srv_desc, &srv);
			if (hr != S_OK)
			{
				Debug::write(EDebugLevel::Error, "rendersystem: could not create shader resource view from texture");
				return (uint32_t)INVALID_RESOURCE;
			}

			texture.m_srv = srv;

			// find unused element in texture array
			unsigned index = (uint32_t)-1;
			for (unsigned i = 0; i < m_textures.size(); i++)
			{
				if (!m_textures[i].m_srv)
				{
					index = i;
					break;
				}
			}
			if (index != -1)
			{
				m_textures[index] = texture;
				return index;
			}

			m_textures.push_back(texture);
			return (uint32_t)m_textures.size() - 1;
		}

		void RenderSystem::destroy_texture(uint32_t texture_id)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: destroy texture");
			assert(texture_id != INVALID_RESOURCE);

			Texture &texture = m_textures[texture_id];
			SAFE_RELEASE(texture.m_texture.texture1d);
			SAFE_RELEASE(texture.m_srv);
			texture.m_format = DXGI_FORMAT_UNKNOWN;
			texture.m_width = 0;
			texture.m_height = 0;
			texture.m_depth = 0;
			texture.m_components = 0;
		}

		uint32_t RenderSystem::create_vertex_format(VertexFormatDesc* vertex_format_desc, uint32_t count, uint32_t shader_id)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: create vertex format");

			assert(shader_id != INVALID_RESOURCE);

			const DXGI_FORMAT formats[][4] = {
				{ DXGI_FORMAT_R32_FLOAT, DXGI_FORMAT_R32G32_FLOAT, DXGI_FORMAT_R32G32B32_FLOAT, DXGI_FORMAT_R32G32B32A32_FLOAT },
				{ DXGI_FORMAT_R16_FLOAT, DXGI_FORMAT_R16G16_FLOAT, DXGI_FORMAT_UNKNOWN, DXGI_FORMAT_R16G16B16A16_FLOAT },
				{ DXGI_FORMAT_R8_UNORM, DXGI_FORMAT_R8G8_UNORM, DXGI_FORMAT_UNKNOWN, DXGI_FORMAT_R8G8B8A8_UNORM }
			};
			const int sizes[] = { sizeof(float), sizeof(short), sizeof(char) };
			const char* semantics[] =
			{
				NULL,
				"POSITION",
				"TEXCOORD",
				"NORMAL",
				"TANGENT",
			};

			VertexFormat format;
			format.m_input_layout = nullptr;
			format.m_vertex_size = 0;

			int index[MAX_FORMAT_DESC] = { 0 };
			D3D11_INPUT_ELEMENT_DESC il_desc[MAX_FORMAT_DESC];
			for (unsigned int i = 0; i < count; i++) {
				il_desc[i].Format = formats[(uint32_t)vertex_format_desc[i].type][vertex_format_desc[i].size - 1];
				il_desc[i].AlignedByteOffset = format.m_vertex_size;
				il_desc[i].SemanticName = semantics[(uint32_t)vertex_format_desc[i].format];
				il_desc[i].SemanticIndex = index[(uint32_t)vertex_format_desc[i].format]++;
				il_desc[i].InputSlot = vertex_format_desc[i].index;
				il_desc[i].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
				il_desc[i].InstanceDataStepRate = 0;

				format.m_vertex_size += vertex_format_desc[i].size * sizes[(uint32_t)vertex_format_desc[i].type];
			};

			Shader& shader = m_shaders[shader_id];
			HRESULT hr = m_device->CreateInputLayout(il_desc, count,
				shader.m_signature->GetBufferPointer(),
				shader.m_signature->GetBufferSize(),
				&format.m_input_layout);
			if (hr != S_OK)
			{
				return (uint32_t)INVALID_RESOURCE;
			}


			unsigned idx = (uint32_t)-1;
			for (unsigned i = 0; i < m_vertex_formats.size(); i++)
			{
				if (m_vertex_formats[i].m_vertex_size == 0 && !m_vertex_formats[i].m_input_layout)
				{
					idx = i;
					break;
				}
			}
			if (idx != -1)
			{
				m_vertex_formats[idx] = format;
				return idx;
			}

			m_vertex_formats.push_back(format);
			return (uint32_t)m_vertex_formats.size() - 1;
		}

		void RenderSystem::destroy_vertex_format(uint32_t vertex_format_id)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: destroy vertex format");

			assert(vertex_format_id != INVALID_RESOURCE);

			VertexFormat &format = m_vertex_formats[vertex_format_id];
			SAFE_RELEASE(format.m_input_layout);
			format.m_vertex_size = 0;
		}

		uint32_t RenderSystem::create_vertex_buffer(EBufferMode mode,
			const void* data, uint32_t size, uint32_t count)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: create vertex buffer");

			const D3D11_USAGE usage[] = { D3D11_USAGE_IMMUTABLE, D3D11_USAGE_DYNAMIC, D3D11_USAGE_DEFAULT };

			VertexBuffer vertex_buffer;
			vertex_buffer.m_vertex_size = size;
			vertex_buffer.m_vertex_count = count;

			D3D11_BUFFER_DESC desc;
			desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			desc.ByteWidth = size * count;
			desc.CPUAccessFlags = mode == EBufferMode::Dynamic ? D3D11_CPU_ACCESS_WRITE : 0;
			desc.MiscFlags = 0;
			desc.StructureByteStride = 0;
			desc.Usage = usage[(uint32_t)mode];
			if (data)
			{
				D3D11_SUBRESOURCE_DATA sr_data;
				sr_data.pSysMem = data;
				sr_data.SysMemPitch = 0;
				sr_data.SysMemSlicePitch = 0;

				HRESULT hr = m_device->CreateBuffer(&desc, &sr_data, &vertex_buffer.m_buffer);
				if (hr != S_OK)
				{
					Debug::write(EDebugLevel::Warning, "could not create vertex buffer");
					return (uint32_t)INVALID_RESOURCE;
				}
			}
			else
			{

				HRESULT hr = m_device->CreateBuffer(&desc, 0, &vertex_buffer.m_buffer);
				if (hr != S_OK)
				{
					Debug::write(EDebugLevel::Warning, "could not create vertex buffer");
					return (uint32_t)INVALID_RESOURCE;
				}
			}

			// find unused element in array
			unsigned index = (uint32_t)-1;
			for (unsigned i = 0; i < m_vertex_buffers.size(); i++)
			{
				if (!m_vertex_buffers[i].m_buffer)
				{
					index = i;
					break;
				}
			}
			if (index != -1)
			{
				m_vertex_buffers[index] = vertex_buffer;
				return index;
			}

			m_vertex_buffers.push_back(vertex_buffer);
			return (uint32_t)(m_vertex_buffers.size() - 1);
		}

		void RenderSystem::destroy_vertex_buffer(uint32_t vertex_buffer_id)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: destroy vertex buffer");

			assert(vertex_buffer_id != INVALID_RESOURCE);

			VertexBuffer &vertex_buffer = m_vertex_buffers[vertex_buffer_id];
			SAFE_RELEASE(vertex_buffer.m_buffer);
			vertex_buffer.m_vertex_count = 0;
			vertex_buffer.m_vertex_size = 0;
		}

		uint32_t RenderSystem::create_index_buffer(const EIndexBufferType type,
			uint32_t count, const void* data, const EBufferMode mode)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: create index buffer");

			const D3D11_USAGE usage[] = { D3D11_USAGE_IMMUTABLE, D3D11_USAGE_DYNAMIC, D3D11_USAGE_DEFAULT };

			D3D11_BUFFER_DESC desc;
			desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			desc.ByteWidth = count * (type == EIndexBufferType::Int16 ? sizeof(short) : sizeof(int));
			desc.CPUAccessFlags = mode == EBufferMode::Dynamic ? D3D11_CPU_ACCESS_WRITE : 0;
			desc.MiscFlags = 0;
			desc.StructureByteStride = 0;
			desc.Usage = usage[(uint32_t)mode];

			ID3D11Buffer* ibuffer;

			if (data)
			{
				D3D11_SUBRESOURCE_DATA srdata;
				srdata.pSysMem = data;
				srdata.SysMemPitch = 0;
				srdata.SysMemSlicePitch = 0;

				HRESULT hr = m_device->CreateBuffer(&desc, &srdata, &ibuffer);
				if (hr != S_OK)
				{
					Debug::write(EDebugLevel::Error, "rendersystem: could not create index buffer");
					return (uint32_t)INVALID_RESOURCE;
				}
			}
			else
			{
				HRESULT hr = m_device->CreateBuffer(&desc, 0, &ibuffer);
				if (hr != S_OK)
				{
					Debug::write(EDebugLevel::Error, "rendersystem: could not create index buffer");
					return (uint32_t)INVALID_RESOURCE;
				}
			}

			IndexBuffer buffer = { 0 };
			buffer.m_buffer = ibuffer;
			buffer.m_index_size = (type == EIndexBufferType::Int16 ? sizeof(short) : sizeof(int));
			buffer.m_index_count = count;

			unsigned index = (uint32_t)-1;
			for (unsigned i = 0; i < m_index_buffers.size(); i++)
			{
				if (!m_index_buffers[i].m_buffer)
				{
					index = i;
					break;
				}
			}
			if (index != -1)
			{
				m_index_buffers[index] = buffer;
				return index;
			}

			m_index_buffers.push_back(buffer);
			return (uint32_t)m_index_buffers.size() - 1;
		}

		void RenderSystem::destroy_index_buffer(uint32_t index_buffer_id)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: destroy index buffer");

			assert(index_buffer_id != INVALID_RESOURCE);

			IndexBuffer& buffer = m_index_buffers[index_buffer_id];
			SAFE_RELEASE(buffer.m_buffer);
			buffer.m_index_size = 0;
			buffer.m_index_count = 0;
		}

		uint32_t RenderSystem::create_sampler_state(ESamplerFilterMode filter,
			ESamplerAddressMode addr_u, ESamplerAddressMode addr_v, ESamplerAddressMode addr_w)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: create sampler state");

			const D3D11_FILTER filters[] =
			{
				D3D11_FILTER_MIN_MAG_MIP_POINT,
				D3D11_FILTER_MIN_MAG_MIP_LINEAR,
			};

			const D3D11_TEXTURE_ADDRESS_MODE mode[] =
			{
				D3D11_TEXTURE_ADDRESS_WRAP,
				D3D11_TEXTURE_ADDRESS_CLAMP,
				D3D11_TEXTURE_ADDRESS_MIRROR,
			};


			D3D11_SAMPLER_DESC desc;
			desc.AddressU = mode[(uint32_t)addr_u];
			desc.AddressV = mode[(uint32_t)addr_v];
			desc.AddressW = mode[(uint32_t)addr_w];
			desc.BorderColor[0] = 0.0f;
			desc.BorderColor[1] = 0.0f;
			desc.BorderColor[2] = 0.0f;
			desc.BorderColor[3] = 0.0f;
			desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
			desc.Filter = filters[(uint32_t)filter];
			desc.MaxAnisotropy = 1;
			desc.MaxLOD = FLT_MAX;
			desc.MinLOD = 0.0f;
			desc.MipLODBias = 0.0f;

			SamplerState sampler = { 0 };
			HRESULT hr = m_device->CreateSamplerState(&desc, &sampler.m_state);
			if (hr != S_OK)
			{
				Debug::write(EDebugLevel::Error, "rendersystem: could not create sampler state");
				return (uint32_t)INVALID_RESOURCE;
			}

			// check for unused element
			unsigned index = (uint32_t)-1;
			for (unsigned i = 0; i < m_sampler_states.size(); i++)
			{
				if (!m_sampler_states[i].m_state)
				{
					index = i;
					break;
				}
			}
			if (index != -1)
			{
				m_sampler_states[index] = sampler;
				return index;
			}

			m_sampler_states.push_back(sampler);
			return (uint32_t)m_sampler_states.size() - 1;
		}

		void RenderSystem::destroy_sampler_state(uint32_t sampler_state_id)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: destroy sampler state");
			assert(sampler_state_id != INVALID_RESOURCE);

			SamplerState &state = m_sampler_states[sampler_state_id];
			SAFE_RELEASE(state.m_state);
		}

		uint32_t RenderSystem::create_blend_state(EBlendMode src_color, EBlendMode dest_color,
			EBlendMode src_alpha, EBlendMode dest_alpha,
			EBlendOp src_op, EBlendOp dest_op, uint32_t mask, bool atoc)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: create blend state");

			const D3D11_BLEND_OP ops[] = {
				D3D11_BLEND_OP_ADD,
				D3D11_BLEND_OP_SUBTRACT,
				D3D11_BLEND_OP_REV_SUBTRACT,
			};

			const D3D11_BLEND modes[] = {
				D3D11_BLEND_ZERO,
				D3D11_BLEND_ONE,
				D3D11_BLEND_SRC_COLOR,
				D3D11_BLEND_INV_SRC_COLOR,
				D3D11_BLEND_DEST_COLOR,
				//D3D11_BLEND_INV_DEST_COLOR,
				D3D11_BLEND_SRC_ALPHA,
				D3D11_BLEND_INV_SRC_ALPHA,
				D3D11_BLEND_DEST_ALPHA,
				D3D11_BLEND_INV_DEST_ALPHA,
				D3D11_BLEND_SRC_ALPHA_SAT,
			};

			bool enable = src_color != EBlendMode::One || dest_color != EBlendMode::One ||
				src_alpha != EBlendMode::One || dest_alpha != EBlendMode::One;

			D3D11_BLEND_DESC desc;
			desc.AlphaToCoverageEnable = atoc ? TRUE : FALSE;
			desc.IndependentBlendEnable = FALSE;
			desc.RenderTarget[0].BlendEnable = enable ? TRUE : FALSE;
			desc.RenderTarget[0].BlendOp = ops[(uint32_t)src_op];
			desc.RenderTarget[0].BlendOpAlpha = ops[(uint32_t)dest_op];
			desc.RenderTarget[0].SrcBlend = modes[(uint32_t)src_color];
			desc.RenderTarget[0].DestBlend = modes[(uint32_t)dest_color];
			desc.RenderTarget[0].SrcBlendAlpha = modes[(uint32_t)src_alpha];
			desc.RenderTarget[0].DestBlendAlpha = modes[(uint32_t)dest_alpha];
			desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			ID3D11BlendState* blend_state;
			HRESULT hr = m_device->CreateBlendState(&desc, &blend_state);
			if (hr != S_OK)
			{
				Debug::write(EDebugLevel::Error, "rendersystem: could not create blend state");
				return (uint32_t)INVALID_RESOURCE;
			}

			BlendState blend = { 0 };
			blend.m_state = blend_state;

			unsigned index = (uint32_t)-1;
			for (unsigned i = 0; i < m_blend_states.size(); i++)
			{
				if (!m_blend_states[i].m_state)
				{
					index = i;
					break;
				}
			}
			if (index != -1)
			{
				m_blend_states[index] = blend;
				return index;
			}

			m_blend_states.push_back(blend);
			return (uint32_t)m_blend_states.size() - 1;
		}

		void RenderSystem::destroy_blend_state(uint32_t blend_state_id)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: destroy blend state");

			assert(blend_state_id != INVALID_RESOURCE);

			BlendState& state = m_blend_states[blend_state_id];
			SAFE_RELEASE(state.m_state);
		}

		uint32_t RenderSystem::create_depth_state(bool write, bool read, EDepthMode mode)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: create depth state");

			const D3D11_COMPARISON_FUNC modes[] = {
				D3D11_COMPARISON_NEVER,
				D3D11_COMPARISON_LESS,
				D3D11_COMPARISON_EQUAL,
				D3D11_COMPARISON_LESS_EQUAL,
				D3D11_COMPARISON_GREATER,
				D3D11_COMPARISON_NOT_EQUAL,
				D3D11_COMPARISON_GREATER_EQUAL,
				D3D11_COMPARISON_ALWAYS,
			};

			D3D11_DEPTH_STENCIL_DESC desc = { 0 };
			desc.DepthEnable = read ? TRUE : FALSE;
			desc.DepthWriteMask = write ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
			desc.DepthFunc = modes[(uint32_t)mode];
			desc.StencilEnable = FALSE;
			desc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
			desc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
			desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
			desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
			desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
			desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
			desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;

			ID3D11DepthStencilState* depth_state;
			HRESULT hr = m_device->CreateDepthStencilState(&desc, &depth_state);
			if (hr != S_OK)
			{
				Debug::write(EDebugLevel::Error, "rendersystem: could not create depth state");
				return (uint32_t)INVALID_RESOURCE;
			}

			DepthState depth = { 0 };
			depth.m_state = depth_state;

			uint32_t index = (uint32_t)-1;
			for (unsigned i = 0; i < m_depth_states.size(); i++)
			{
				if (!m_depth_states[i].m_state)
				{
					index = i;
					break;
				}
			}
			if (index != -1)
			{
				m_depth_states[index] = depth;
				return index;
			}

			m_depth_states.push_back(depth);
			return (uint32_t)m_depth_states.size() - 1;
		}

		void RenderSystem::destroy_depth_state(uint32_t depth_state_id)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: destroy depth state");

			assert(depth_state_id != INVALID_RESOURCE);

			DepthState& state = m_depth_states[depth_state_id];
			SAFE_RELEASE(state.m_state);
		}

		uint32_t RenderSystem::create_rasterizer_state(EFillMode fill_mode, ECullMode cull_mode)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: create rasterizer state");

			const D3D11_CULL_MODE cullmodes[] =
			{
				D3D11_CULL_NONE,
				D3D11_CULL_BACK,
				D3D11_CULL_FRONT
			};

			const D3D11_FILL_MODE fillmodes[] =
			{
				D3D11_FILL_SOLID,
				D3D11_FILL_WIREFRAME
			};

			D3D11_RASTERIZER_DESC desc;
			desc.FillMode = fillmodes[(uint32_t)fill_mode];
			desc.CullMode = cullmodes[(uint32_t)cull_mode];
			desc.FrontCounterClockwise = FALSE;
			desc.DepthBias = 0;
			desc.SlopeScaledDepthBias = 0.0f;
			desc.DepthBiasClamp = 0.0f;
			desc.DepthClipEnable = TRUE;
			desc.ScissorEnable = FALSE;
			desc.MultisampleEnable = FALSE;
			desc.AntialiasedLineEnable = FALSE;

			ID3D11RasterizerState* rasterizer_state;
			HRESULT hr = m_device->CreateRasterizerState(&desc, &rasterizer_state);
			if (hr != S_OK)
			{
				Debug::write(EDebugLevel::Error, "rendersystem: could not create rasterizer state");
				return (uint32_t)INVALID_RESOURCE;
			}

			RasterizerState rasterizer = { 0 };
			rasterizer.m_state = rasterizer_state;

			unsigned index = (uint32_t)-1;
			for (unsigned i = 0; i < m_rasterizer_state.size(); i++)
			{
				if (!m_rasterizer_state[i].m_state)
				{
					index = i;
					break;
				}
			}
			if (index != -1)
			{
				m_rasterizer_state[index] = rasterizer;
				return index;
			}

			m_rasterizer_state.push_back(rasterizer);
			return (uint32_t)m_rasterizer_state.size() - 1;
		}

		void RenderSystem::destroy_rasterizer_state(uint32_t rasterizer_state_id)
		{
			Debug::write(EDebugLevel::Info, "rendersystem: destroy rasterizer state");

			assert(rasterizer_state_id != INVALID_RESOURCE);

			RasterizerState& state = m_rasterizer_state[rasterizer_state_id];
			SAFE_RELEASE(state.m_state);
		}

		// private
		void RenderSystem::apply_shader()
		{
			if (m_selected_shader != m_current_shader)
			{
				m_current_shader = m_selected_shader;
				if (m_current_shader == INVALID_RESOURCE)
				{
					m_context->VSSetShader(NULL, NULL, 0);
					m_context->PSSetShader(NULL, NULL, 0);
					return;
				}

				Shader& shader = m_shaders[m_current_shader];
				m_context->VSSetShader(shader.m_vertex_shader, NULL, 0);
				m_context->PSSetShader(shader.m_pixel_shader, NULL, 0);
				if (shader.m_num_vs_constants > 0)
					m_context->VSSetConstantBuffers(0, shader.m_num_vs_constants, shader.m_vs_constants);
				if (shader.m_num_ps_constants > 0)
					m_context->PSSetConstantBuffers(0, shader.m_num_ps_constants, shader.m_ps_constants);
			}

			// apply shader constants
			if (m_current_shader == INVALID_RESOURCE)
				return;

			Shader& shader = m_shaders[m_current_shader];
			for (unsigned i = 0; i < shader.m_num_vs_constants; i++)
			{
				if (shader.m_vs_dirty[i])
				{
					shader.m_vs_dirty[i] = false;
					m_context->UpdateSubresource(shader.m_vs_constants[i], 0, NULL, shader.m_vs_mem[i], 0, 0);
				}
			}

			for (unsigned i = 0; i < shader.m_num_ps_constants; i++)
			{
				if (shader.m_ps_dirty[i])
				{
					shader.m_ps_dirty[i] = false;
					m_context->UpdateSubresource(shader.m_ps_constants[i], 0, NULL, shader.m_ps_mem[i], 0, 0);
				}
			}
		}

		void RenderSystem::apply_texture_and_sampler()
		{
			unsigned num_srv = 0, num_smpl = 0;
			ID3D11ShaderResourceView* srv[MAX_TEXTURE_UNITS] = { nullptr };
			ID3D11SamplerState* smpl[MAX_TEXTURE_UNITS] = { nullptr };

			for (uint32_t i = 0; i < MAX_TEXTURE_UNITS; i++)
			{
				if (m_selected_texture[i] != m_current_texture[i])
				{
					m_current_texture[i] = m_selected_texture[i];
					if (m_current_texture[i] != INVALID_RESOURCE)
						srv[num_srv++] = m_textures[m_current_texture[i]].m_srv;
				}
				if (m_selected_sampler_state[i] != m_current_sampler_state[i])
				{
					m_current_sampler_state[i] = m_selected_sampler_state[i];
					if (m_current_sampler_state[i] != INVALID_RESOURCE)
						smpl[num_smpl++] = m_sampler_states[m_current_sampler_state[i]].m_state;
				}
			}

			if (num_srv > 0)
				m_context->PSSetShaderResources(0, num_srv, srv);
			if (num_smpl > 0)
				m_context->PSSetSamplers(0, num_smpl, smpl);
		}

		void RenderSystem::apply_vertex_format()
		{
			if (m_selected_vertex_format == m_current_vertex_format)
				return;

			m_current_vertex_format = m_selected_vertex_format;
			if (m_current_vertex_format == INVALID_RESOURCE)
			{
				m_context->IASetInputLayout(NULL);
				return;
			}
			m_context->IASetInputLayout(m_vertex_formats[m_current_vertex_format].m_input_layout);
		}

		void RenderSystem::apply_vertex_buffer()
		{
			if (m_selected_vertex_buffer == m_current_vertex_buffer)
				return;

			if (m_current_vertex_buffer != m_selected_vertex_buffer) {
				m_current_vertex_buffer = m_selected_vertex_buffer;
				unsigned int stride[1] = { 0 };
				unsigned int offset[1] = { 0 };
				if (m_current_vertex_buffer == INVALID_RESOURCE) {
					ID3D11Buffer* none[] = { nullptr };
					m_context->IASetVertexBuffers(0, 1, none, stride, offset);
					return;
				};
				stride[0] = m_vertex_formats[m_current_vertex_buffer].m_vertex_size;
				m_context->IASetVertexBuffers(0, 1, &m_vertex_buffers[m_current_vertex_buffer].m_buffer, stride, offset);
			};
		}

		void RenderSystem::apply_index_buffer()
		{
			if (m_selected_index_buffer == m_current_index_buffer)
				return;

			m_current_index_buffer = m_selected_index_buffer;
			if (m_current_index_buffer == INVALID_RESOURCE)
			{
				m_context->IASetIndexBuffer(nullptr, DXGI_FORMAT_UNKNOWN, 0);
				return;
			}

			IndexBuffer& buffer = m_index_buffers[m_current_index_buffer];
			m_context->IASetIndexBuffer(buffer.m_buffer, buffer.m_index_size == 2 ? DXGI_FORMAT_R16_UINT : DXGI_FORMAT_R32_UINT, 0);
		}

		void RenderSystem::apply_blend_state()
		{
			if (m_selected_blend_state == m_current_blend_state)
				return;

			const FLOAT f[] = { 0.0f, 0.0f, 0.0f, 0.0f };

			m_current_blend_state = m_selected_blend_state;
			if (m_current_blend_state == INVALID_RESOURCE)
			{
				m_context->OMSetBlendState(nullptr, f, 0xffffffff);
				return;
			}

			BlendState& state = m_blend_states[m_current_blend_state];
			m_context->OMSetBlendState(state.m_state, f, 0xffffffff);
		}

		void RenderSystem::apply_depth_state()
		{
			if (m_selected_depth_state == m_current_depth_state)
				return;

			m_current_depth_state = m_selected_depth_state;
			if (m_current_depth_state == INVALID_RESOURCE)
			{
				m_context->OMSetDepthStencilState(nullptr, 0);
				return;
			}

			DepthState& state = m_depth_states[m_current_depth_state];
			m_context->OMSetDepthStencilState(state.m_state, 0);
		}

		void RenderSystem::apply_rasterizer_state()
		{
			if (m_selected_rasterizer_state == m_current_rasterizer_state)
				return;

			m_current_rasterizer_state = m_selected_rasterizer_state;
			if (m_current_rasterizer_state == INVALID_RESOURCE)
			{
				m_context->RSSetState(nullptr);
				return;
			}

			RasterizerState& state = m_rasterizer_state[m_current_rasterizer_state];
			m_context->RSSetState(state.m_state);
		}

		void RenderSystem::reset_state()
		{
			m_selected_shader = (uint32_t)INVALID_RESOURCE;
			m_selected_vertex_format = (uint32_t)INVALID_RESOURCE;
			m_selected_vertex_buffer = (uint32_t)INVALID_RESOURCE;
			m_selected_index_buffer = (uint32_t)INVALID_RESOURCE;
			m_selected_blend_state = (uint32_t)INVALID_RESOURCE;
			m_selected_depth_state = (uint32_t)INVALID_RESOURCE;
			m_selected_rasterizer_state = (uint32_t)INVALID_RESOURCE;

			for (uint32_t i = 0; i < MAX_TEXTURE_UNITS; i++)
			{
				m_selected_texture[i] = (uint32_t)INVALID_RESOURCE;
				m_selected_sampler_state[i] = (uint32_t)INVALID_RESOURCE;
			}

			m_current_topology = EDrawMode::Invalid;
		}
	}
}
