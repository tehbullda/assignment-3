// vertex_format.cpp

#include "stdafx.h"
#include <locale>
#include <sstream>
#include <helium/system/render_system.hpp>
#include <helium/resource/vertex_format.hpp>

namespace helium
{
	namespace resource
	{
		static system::EVertexAttributeFormat get_format(std::string& format)
		{
			std::transform(format.begin(), format.end(), format.begin(), ::tolower);
			if (format.compare("generic") == 0)
				return system::EVertexAttributeFormat::Generic;
			else if (format.compare("position") == 0)
				return system::EVertexAttributeFormat::Position;
			else if (format.compare("texcoord") == 0)
				return system::EVertexAttributeFormat::TexCoord;
			else if (format.compare("normal") == 0)
				return system::EVertexAttributeFormat::Normal;
			else if (format.compare("tangent") == 0)
				return system::EVertexAttributeFormat::Tangent;
			return system::EVertexAttributeFormat::Invalid;
		}

		static system::EVertexAttributeType get_type(std::string& type)
		{
			std::transform(type.begin(), type.end(), type.begin(), ::tolower);
			if (type.compare("float") == 0)
				return system::EVertexAttributeType::Float;
			else if (type.compare("short") == 0)
				return system::EVertexAttributeType::Short;
			else if (type.compare("byte") == 0)
				return system::EVertexAttributeType::Byte;
			return system::EVertexAttributeType::Invalid;
		}


		// static 
		VertexFormat::Ptr VertexFormat::create(const std::string& filename)
		{
			std::ifstream stream;
			stream.open(filename.c_str());
			if (!stream.is_open())
			{
				Debug::write(EDebugLevel::Error, String::format("vertexformat: could not find file %s", filename.c_str()));
				return VertexFormat::Ptr(nullptr);
			}

			unsigned idx = 0;
			system::VertexFormatDesc desc[32] = { 0 };

			int index = 0, size = 0;
			std::string line, format, type;
			while (!stream.eof())
			{
				std::getline(stream, line);

				if (line.length() > 0)
				{
					if (line.find("//") < line.find_first_not_of('/'))
						continue;

					std::stringstream ss(line);
					ss >> index;
					ss >> format;
					ss >> type;
					ss >> size;

					desc[idx].index = index;
					desc[idx].format = get_format(format);
					desc[idx].type = get_type(type);
					desc[idx].size = size;
					idx++;
				}
			}
			stream.close();

			Debug::write(EDebugLevel::Info, String::format("vertexformat: %s", filename.c_str()));

			VertexFormat* vertex_format = new VertexFormat;
			vertex_format->m_count = idx;
			vertex_format->m_desc = new system::VertexFormatDesc[idx];
			for (unsigned i = 0; i < idx; i++)
				vertex_format->m_desc[i] = desc[i];
			return VertexFormat::Ptr(vertex_format);
		}

		// private
		VertexFormat::VertexFormat()
		{
			m_count = 0;
			m_desc = nullptr;
		}

		// public
		VertexFormat::~VertexFormat()
		{
			if (m_desc)
			{
				delete[] m_desc;
				m_desc = nullptr;
			}
		}

		uint32_t VertexFormat::get_count() const
		{
			return m_count;
		}

		const system::VertexFormatDesc* VertexFormat::get_desc() const
		{
			return m_desc;
		}
	}
}
