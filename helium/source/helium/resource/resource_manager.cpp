// resource_manager.cpp

#include "stdafx.h"
#include <helium/system/render_system.hpp>
#include <helium/resource/resource_manager.hpp>

namespace helium
{
	namespace resource
	{
		// static 
		ResourceManager::Ptr ResourceManager::create()
		{
			return ResourceManager::Ptr(new ResourceManager);
		}

		// private
		ResourceManager::ResourceManager()
		{
		}

		// public
		ResourceManager::~ResourceManager()
		{
		}

		void ResourceManager::set_directory(const std::string& directory)
		{
			m_directory = directory;
		}

		bool ResourceManager::has_resource(const std::string& filename, const std::string& group)
		{
			auto it = m_groups.find(group);
			if (it == m_groups.end())
			{
				Debug::write(EDebugLevel::Warning, String::format("resourcemanager: trying to access group that doesn't exist: %s", group.c_str()));
				return false;
			}

			return it->second.m_resources.find(filename) != it->second.m_resources.end();
		}

		void ResourceManager::unload_resource(const std::string& filename, const std::string& group)
		{
			auto it = m_groups.find(group);
			if (it == m_groups.end())
			{
				Debug::write(EDebugLevel::Warning, 
					String::format("resourcemanager: trying to access group that doesn't exist: %s", 
					group.c_str()));
				return;
			}

			auto res = it->second.m_resources.find(filename);
			if (res == it->second.m_resources.end())
			{
				Debug::write(EDebugLevel::Warning, 
					String::format("resourcemanager: trying to access resource that doesn't exist: %s [%s]", 
					filename.c_str(), group.c_str()));
				return;
			}

			res->second->release();
			it->second.m_resources.erase(res);

			Debug::write(EDebugLevel::Info, 
				String::format("resourcemanager: releasing resource: %s [%s]", 
				filename.c_str(), group.c_str()));
		}

		void ResourceManager::unload_all_resources(const std::string& group)
		{
			auto it = m_groups.find(group);
			if (it == m_groups.end())
			{
				Debug::write(EDebugLevel::Warning, 
					String::format("resourcemanager: trying to access group that doesn't exist: %s", 
					group.c_str()));
				return;
			}

			Debug::write(EDebugLevel::Warning, 
				String::format("resourcemanager: releasing whole group: [%s]", 
				group.c_str()));
			auto res = it->second.m_resources.begin();
			while (res != it->second.m_resources.end())
			{
				res->second->release();

				Debug::write(EDebugLevel::Info, 
					String::format("  resource: %s", 
					res->first.c_str()));
				++res;
			}
			it->second.m_resources.clear();
		}
	}
}
