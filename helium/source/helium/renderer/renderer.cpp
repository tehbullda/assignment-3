// renderer.cpp

#include "stdafx.h"
#include <helium/system/render_system.hpp>
#include <helium/renderer/renderer.hpp>

namespace helium
{
	namespace renderer
	{
		Renderer::Renderer(system::RenderSystem* render_system)
		{
			m_render_system = render_system;
		}
	}
}