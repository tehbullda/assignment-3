// message.cpp

#include "stdafx.h"
#include <helium/message.hpp>

namespace helium
{
	Message::Message()
	{
		m_type = EMsgType::Invalid;
	}

	Message::Message(EMsgType type)
	{
		m_type = type;
	}
}
