// time.cpp

#include "stdafx.h"
#include <chrono>
#include <helium/time.hpp>

namespace helium
{
	static int64_t get_tick()
	{
		using namespace std::chrono;
		auto now = high_resolution_clock::now().time_since_epoch();
		auto tick = duration_cast<microseconds>(now).count();
		return tick;
	}

	// static
	Time Time::now()
	{
		return Time(get_tick());
	}

	// public
	Time::Time()
		: m_tick(0)
	{
	}

	Time::Time(const Time& rhs)
		: m_tick(rhs.m_tick)
	{
	}

	Time::Time(const int64_t& rhs)
		: m_tick(rhs)
	{
	}

	void Time::operator=(const int64_t& rhs)
	{
		m_tick = rhs;
	}

	Time& Time::operator=(const Time& rhs)
	{
		m_tick = rhs.m_tick;
		return *this;
	}

	Time Time::operator+(const Time& rhs)
	{
		return Time(m_tick + rhs.m_tick);
	}

	Time Time::operator-(const Time& rhs)
	{
		return Time(m_tick - rhs.m_tick);
	}

	double Time::as_seconds() const
	{
		return static_cast<double>(m_tick) * 0.000001;
	}

	double Time::as_milliseconds() const
	{
		return static_cast<double>(m_tick) * 0.001;
	}

	double Time::as_microseconds() const
	{
		return static_cast<double>(m_tick);
	}
}
