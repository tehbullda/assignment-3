// shader_unit.cpp

#include "stdafx.h"
#include <helium/resource/shader.hpp>
#include <helium/scene/camera.hpp>
#include <helium/scene/shader_unit.hpp>

namespace helium
{
	namespace scene
	{
		// static 
		ShaderUnit::Ptr ShaderUnit::create()
		{
			return ShaderUnit::Ptr(new ShaderUnit);
		}

		// private
		ShaderUnit::ShaderUnit()
		{
			m_shader = (uint32_t)-1;
			m_vertex_format = (uint32_t)-1;
		}

		// public
		ShaderUnit::~ShaderUnit()
		{
		}

		uint32_t ShaderUnit::get_shader() const
		{
			return m_shader;
		}

		uint32_t ShaderUnit::get_vertex_format() const
		{
			return m_vertex_format;
		}

		bool ShaderUnit::has_shader_parameter(const std::string& name) const
		{
			uint32_t hash = String::hash32(name.c_str(), (uint32_t)name.length());
			return m_parameters.find(hash) != m_parameters.end();
		}

		void ShaderUnit::set_shader_parameter(const std::string& name, uint32_t size, const void* buffer)
		{
			uint32_t hash = String::hash32(name.c_str(), (uint32_t)name.length());
			auto it = m_parameters.find(hash);
			if (it == m_parameters.end())
				Debug::fatal(String::format("shaderunit: could not find shader parameter %s", name.c_str()));
			it->second->set_size(size);
			it->second->set_buffer(buffer);
		}

		const ShaderParameter* ShaderUnit::get_shader_parameter(const std::string& name) const
		{
			uint32_t hash = String::hash32(name.c_str(), (uint32_t)name.length());
			auto it = m_parameters.find(hash);
			if (it == m_parameters.end())
			{
				Debug::fatal(String::format("shaderunit: could not find shader parameter %s", name.c_str()));
				return nullptr;
			}
			return it->second.get();
		}

		// private
		void ShaderUnit::on_camera_change(Camera* camera)
		{
			if (!camera)
			{
				release_parameter("projection");
				release_parameter("view");
				return;
			}

			// note(tommi): checking if we have camera parameters _should_ be redundant
			set_shader_parameter("projection", sizeof(Matrix4), &camera->get_projection());
			set_shader_parameter("view", sizeof(Matrix4), &camera->get_view());
		}

		void ShaderUnit::release_parameter(const char* name)
		{
			uint32_t hash = String::hash32(name, (uint32_t)strlen(name));
			auto it = m_parameters.find(hash);
			if (it == m_parameters.end())
			{
				Debug::write(EDebugLevel::Warning, String::format("shaderunit: could not find shader parameter for deletion %s", name));
				return;
			}
			// note(tommi): since we use smart pointers we don't have to
			//   worry about de-allocation of dynamic memory
			m_parameters.erase(it);
		}
	}
}
