// techinque.cpp

#include "stdafx.h"
#include <cassert>
#include <helium/scene/technique.hpp>

namespace helium
{
	namespace scene
	{
		// static 
		uint32_t Technique::ms_id = 1; 

		Technique::Ptr Technique::create()
		{
			return Technique::Ptr(new Technique);
		}

		// private
		Technique::Technique()
		{
			m_id = ms_id++;
			m_depth = (uint32_t)-1;
		}

		// public
		Technique::~Technique()
		{
		}

		bool Technique::operator==(const Technique& rhs)
		{
			(void)rhs;
			return false;
		}

		bool Technique::operator!=(const Technique& rhs)
		{
			(void)rhs;
			return false;
		}

		uint32_t Technique::get_id() const
		{
			return m_id;
		}

		bool Technique::has_lighting_property() const
		{
			return m_lighting_property.get() != nullptr;
		}

		void Technique::set_lighting_property(LightingProperty::Ptr&& lighting_property)
		{
			m_lighting_property = std::move(lighting_property);
		}

		const LightingProperty* Technique::get_lighting_property() const
		{
			return m_lighting_property.get();
		}

		bool Technique::has_shader_unit() const
		{
			return m_shader_unit.get() != nullptr;
		}

		void Technique::set_shader_unit(ShaderUnit::Ptr&& shader_unit)
		{
			m_shader_unit = std::move(shader_unit);
		}

		const ShaderUnit* Technique::get_shader_unit() const
		{
			return m_shader_unit.get();
		}

		bool Technique::has_textures() const
		{
			return !m_texture_units.empty();
		}

		uint32_t Technique::get_texture_unit_count() const
		{
			return (uint32_t)m_texture_units.size();
		}

		void Technique::add_texture_unit(TextureUnit::Ptr&& texture_unit)
		{
			m_texture_units.push_back(std::move(texture_unit));
		}

		const TextureUnit* Technique::get_texture_unit(uint32_t index) const
		{
			assert(index < m_texture_units.size());
			return m_texture_units[index].get();
		}

		void Technique::set_depth_state(uint32_t state)
		{
			m_depth = state;
		}

		uint32_t Technique::get_depth_state() const
		{
			return m_depth;
		}
	}
}
