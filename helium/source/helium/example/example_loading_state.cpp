// example_loading_state.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/resource/resource_cache.hpp>
#include <helium/system/render_system.hpp>
#include <helium/example/example_shared_resources.hpp>
#include <helium/example/example_loading_state.hpp>

#include <helium/resource/font.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>
#include <helium/resource/resource_creator.hpp>

namespace helium
{
	namespace example
	{
		ExampleLoadingState::ExampleLoadingState()
		{
			m_font = nullptr;
		}

		ExampleLoadingState::~ExampleLoadingState()
		{
		}

		void ExampleLoadingState::initialize()
		{
			resource::ResourceCache* rescache = ServiceLocator<resource::ResourceCache>::get_service();

			m_font = rescache->load_resource<resource::Font>("font.fontdesc.txt", "example");
			
			resource::Shader* shader = rescache->load_resource<resource::Shader>("font.shader.txt", "example");
			resource::ResourceCreator<resource::Shader>::create(shader);

			resource::VertexFormat* format = rescache->load_resource<resource::VertexFormat>("font.vertexformat.txt", "example");
			resource::ResourceCreator<resource::VertexFormat>::create(format);
		}

		void ExampleLoadingState::shutdown()
		{
		}

		bool ExampleLoadingState::update()
		{
			return true;
		}

		void ExampleLoadingState::draw()
		{
		}

		uint32_t ExampleLoadingState::next()
		{
			return String::hash32("examplescene", 12);
		}
	}
}
