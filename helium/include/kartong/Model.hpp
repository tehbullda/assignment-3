#pragma once

#include "stdafx.h"

#include <helium/scene/transform.hpp>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#pragma comment(lib, "assimp.lib")

namespace kartong
{
	class Model
	{
	public:

		Model(std::string p_File);
		~Model();

		typedef std::unique_ptr<Model> Ptr;
		static Ptr Create(std::string p_File);


		void Initialize();
		void Update(const float& p_Deltatime);
		void Draw(scene::Camera* p_Camera);

		void SetPosition(Vector3 p_Pos);
		void SetVolume(Vector3 p_Vol);
		void SetScale(Vector3 p_Scale);
		void SetRotation(Vector3 p_axis);


		Vector3 m_HighestPoints;
		Vector3 m_LowestPoints;
	private:
		Vector3 m_Position;
		Vector3 m_Rotation;
		Vector3 m_Scale;
		Vector3 m_PrevPos;
		Vector3 m_Volume;

		Object m_Model;

		double m_Time;

		


		helium::system::RenderSystem* m_render_system;
		helium::resource::Shader::Ptr		m_shader;
		helium::resource::VertexFormat::Ptr m_format;
		helium::resource::Texture::Ptr		m_texture;
		helium::resource::Sampler::Ptr		m_sampler;

		Assimp::Importer* m_Importer;

		scene::Transform m_Transform;

	};
}