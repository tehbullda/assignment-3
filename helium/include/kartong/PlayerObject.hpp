#pragma once

#include "stdafx.h"

#include <helium/scene/camera.hpp>
#include <kartong/BoundingBox.hpp>
#include <kartong/CollisionManager.hpp>
#include <kartong/HUD_Ammo.hpp>

//class ObjectGame;


namespace kartong
{
	using namespace helium;


	class PlayerObject : public GameObject
	{
	public:
		PlayerObject(input::Input* p_Input, const int& p_ID);
		~PlayerObject();

		typedef std::unique_ptr<PlayerObject> Ptr;
		/*static Ptr Create(input::Input* p_Input, const int& p_ID);*/
	private:// Actions
		void UpdateMovement(const float& p_Deltatime);
		void UpdateDirection(const float& p_Deltatime);

		void MovePlayerX(const float& p_Val);
		void MovePlayerY(const float& p_Val);
		void MovePlayerZ(const float& p_Val);

		

		void UpdatePhysics();
		void UpdateWeapon();
		void ToggleCrouch();
		void UpdateJump();
		void UpdateCollision();
		bool IsGrounded();

	public:
		void Initialize();
		void Update(const float& p_Deltatime);
		void PostUpdate();

		void Draw(scene::Camera* p_Camera);

		void SetPosition(Vector3 p_Pos);

		scene::Camera* GetPlayerCamera();

		bool PlayerMoved();
		bool PlayerShot();
		Vector3 GetDirection();

		void ResetStats();


		//7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7
		//7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7
		//7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7/7

	private:
		struct Stats
		{
			int hp;
			float movespeed;
			float jumpheight;
			float jumpforce;
		} m_Current, m_Max;

		struct gravity
		{
			float force;
			float direction; //IF WE WANT TO GET FANCY
		};


	private:
		input::Input* m_Input;
		Mouse::Ptr m_Mouse;

	private:
		float m_pitch;
		float m_yaw;
		float m_roll;

		Vector3 m_right;
		Vector3 m_up;
		Vector3 m_forward;

		Vector3 m_prevPos;

		Vector3 m_UpMove;
		Vector3 m_RightMove;
		Vector3 m_ForwardMove;

		Matrix4 m_view;
		Matrix4 m_projection;

		float m_Sense;
		float m_Mouseyaw;
		float m_Mousepitch;

		void MoveForward(float amount);
		void MoveSidestep(float amount);
		void MoveElevate(float amount);
		void RotateX(float amount);
		void RotateY(float amount);
		void RotateZ(float amount);

		bool m_Dirty;

		RECT m_WindowRect;
		Vector2 m_MousePos;
		Vector2 m_PrevMousePos;


		CollisionManager* m_CollisionManager;
	public:
		void UpdateView();
		float GetYaw();
		float GetPitch();

		scene::Camera* m_PlayerCamera;
		scene::Camera* m_ZoomCamera;
		helium::system::RenderSystem* m_Rendersystem;
		Matrix4 m_MovementForward();
		bool m_standing;
		bool m_Zoomed;
		BoundingBox* m_Bounds;
		

	private:
		BoundingBox* m_Collided; //This will contain the box we collided with
		Weapon* m_TestWeapon;
		HUDAmmo::Ptr m_AmmoHUD;



	private://Physics
		bool m_Grounded;
		
		float m_DeltaTime;
		
		//Make pure virtual weapon class then make weapons
	};
};