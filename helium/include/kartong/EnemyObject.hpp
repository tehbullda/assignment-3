#pragma once

#include "stdafx.h"

namespace kartong
{
	class EnemyObject : public GameObject
	{
	public:
		EnemyObject(const int &id);
		EnemyObject(const int &id, const std::string &name);
		~EnemyObject();

		void Initialize();
		void Update(const float &p_DeltaTime);
		void Draw(helium::scene::Camera *camera);

		void SetDirection(const Vector3 &dir);
	private:
		BoundingBox *m_Bounds;
		float m_DeltaTime;
		Cube *m_box;
		Vector3 m_direction;
	};
}