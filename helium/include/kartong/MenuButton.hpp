#pragma once

#include <kartong/GUITexture.hpp>
namespace kartong {
	enum State {
		Normal,
		Hover,
		Pressed
	};

	class MenuButton {
	public:
		MenuButton(const std::string identifier,const std::string &texture, const std::string &hovertexture, const helium::Vector2 &pos);
		~MenuButton();

		void Update(helium::Vector2 mousePos);

		bool ButtonPressed();
		std::string GetIdentifier();

		void Draw();

	private:
		void SetState(State state);

	private:
		State m_state;
		GUITexture::Ptr m_texture;
		GUITexture::Ptr m_hovertexture;
		helium::Hitbox2D m_hitbox;
		helium::Mouse::Ptr m_mouse;
		bool m_buttondown;

		std::string m_identifier;
	};
}