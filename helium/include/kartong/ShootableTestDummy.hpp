#pragma once

#include "stdafx.h"

namespace kartong
{
	class ShootableTestDummy : public GameObject
	{
	public:
		ShootableTestDummy();
		~ShootableTestDummy();

		typedef std::unique_ptr<ShootableTestDummy> Ptr;
		static Ptr Create();

		void Initialize();
		void Update(const float& p_Deltatime);
		void Draw(scene::Camera* p_Cam);

		void SetMovementX(const float& p_Min, const float& p_Max);
		void SetMovementY(const float& p_Min, const float& p_Max);

		void SetMoveSpeedX(const float& p_MoveSpeed);
		void SetMoveSpeedY(const float& p_MoveSpeed);
		bool m_TargetHit;
		bool m_InvetredX;
		bool m_InvetredY;

	private:
		void UpdateMovement();

		CollisionManager* m_CollisionManager;
		Cube::Ptr m_Cube;
		BoundingBox* m_Bounds;

		float m_Deltatime;
		float m_MoveSpeedX;
		float m_MoveSpeedY;

		float m_XMin;
		float m_XMax;
		float m_YMin;
		float m_YMax;


		bool TargetHit;

	};
}