#pragma once

#include "stdafx.h"
#include <kartong/SolidPart.hpp>
#include <kartong/Model.hpp>
#include <kartong/Cube.hpp>
#include <kartong/Ground.hpp>
#include <kartong/SolidCube.hpp>
#include <kartong/ShootableTestDummy.hpp>
#include <kartong/TargetManager.hpp>

namespace kartong
{
	class Level
	{
	public:
		Level(scene::Camera* p_Camera);
		~Level();

		typedef std::unique_ptr<Level> Ptr;
		static Ptr Create(scene::Camera* p_Camera);

		void Initialize();
		void Update(const float& p_Deltatime);
		void Draw();

	private:
		void InitializeBasicGeometry();
		void InitializeInteractives();
		void InitializeExtras();
		void UpdateBasicGeometry();
		void UpdateInteractives();
		void UpdateExtras();
		void DrawBasicGeometry();
		void DrawInteractives();
		void DrawExtras();

	private:
		std::vector<GameObject*> m_Objects;
		std::vector<GameObject*> m_visibles;
		scene::Camera* m_Camera;
		float m_DeltaTime;
		//Level

		kartong::Skybox::Ptr m_Skybox;
		//basic Geometry
		
		SolidCube::Ptr m_Ground;
		SolidCube::Ptr m_Wall1;
		SolidCube::Ptr m_Wall2;
		SolidCube::Ptr m_Wall3;
		SolidCube::Ptr m_Wall4;
		SolidCube::Ptr m_Deskthing;
		SolidCube::Ptr m_Pillar1;
		SolidCube::Ptr m_Pillar2;
		SolidCube::Ptr m_Pillar3;
		SolidCube::Ptr m_Pillar4;
		SolidCube::Ptr m_BackPillar;
		Arc::Ptr m_Arc_Top;
		Arc::Ptr m_Arc_Bot;
		Arc::Ptr m_Arc_Left;
		Arc::Ptr m_Arc_Right;

		

		Stairs::Ptr m_Stairs;

		
		

		//Interactives
		kartong::HealthPack::Ptr m_TestHPPack;
		//ShootableTestDummy::Ptr m_TestTarget;
		TargetManager::Ptr m_Targets;
		//Extras
	};
}