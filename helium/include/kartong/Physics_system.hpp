#pragma once

namespace kartong {


	class Physics_system {
	public:
		Physics_system();
		~Physics_system();

		static Vector3 Apply_gravity(Vector3 movement, float p_Deltatime);
		static Vector3 Apply_friction(Vector3 movement);
	public:
		static const float m_gravity;
		static const float m_friction;
	};
}