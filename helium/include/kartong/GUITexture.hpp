#pragma once

#include "stdafx.h"

namespace kartong
{
	struct  UIElement
	{
		int shader;
		int texture;
		int sampler;
		int format;
		int buffer;
		int blend;
		int index;
		int depth;
		int raster;
		int drawcount;
		float x, y, w, h;
		helium::Matrix4 world;
	};


	class GUITexture
	{
	public:
		GUITexture(const std::string& p_texture, const helium::Vector2 p_Pos, bool isText = false);
		~GUITexture();

		typedef std::unique_ptr<GUITexture> Ptr;
		static Ptr Create(const std::string& p_texture, const helium::Vector2 p_Pos);

		void Draw();

		void SetPos(const helium::Vector2 p_Pos, float depth = 0.0f);
		void SetNewTexture(const std::string& p_Texture);
		void SetScale(helium::Vector2 p_Scale);

		helium::Vector2 GetPos();
		helium::Hitbox2D GetHitbox();
		helium::Vector2 GetGlobalSize();
		helium::Vector2 GetLocalSize();
		helium::Vector2 GetScale();

	protected:

		helium::system::RenderSystem* m_render_system;
		helium::resource::Shader::Ptr		m_shader;
		helium::resource::VertexFormat::Ptr m_format;
		helium::resource::Texture::Ptr		m_texture;
		helium::resource::Sampler::Ptr		m_sampler;

		bool m_Dirty;
		helium::Vector2 m_Position;
		helium::Vector2 m_Scale;
		helium::Vector2 m_Size;
		helium::Hitbox2D m_hitbox;
		helium::Matrix4 m_StandardWorld;
		float m_Depth;

		UIElement element;

	};
};