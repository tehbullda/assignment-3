#pragma once

#include <helium/system/render_system.hpp>
#include <helium/resource/font.hpp>

namespace kartong {
	struct Vertex {
		Vector3 position;
		Vector2 texcoord;
	};
	class TextObject : GUITexture {
		struct Text {
			helium::resource::Font::Ptr m_font;
			std::string m_string;
		};
	public:
		TextObject(const std::string &desc, const std::string &texture, helium::Vector3 pos);
		~TextObject();
		void Update();
		void UpdateBuffers();
		void Draw();
		void SetText(const std::string &text);
		void SetPosition(Vector3 pos);

	private:
		helium::system::RenderSystem *m_render_system;
		Matrix4 m_transform;
		Text m_text;
		bool m_dirty;
	};
}