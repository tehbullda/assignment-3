#pragma once

#include "stdafx.h"
//#include <kartong/Cube.hpp>

namespace kartong
{

	using namespace helium;
	class BoundingBox
	{
	public:

		BoundingBox(GameObject* p_Owner);
		~BoundingBox();

		float GetCheckRadius();
		Vector3 GetPosition();
		Vector3 GetVolume();
		Vector3 GetHalfVolume();
		Matrix4 GetTransform();

		void SetCheckRadius(float p_Radius);
		void SetPosition(Vector3 p_Position);
		void SetVolume(Vector3 p_Volume);
		Matrix4 SetTransform;

		bool CheckIfInRange(const Vector3 p_pos);
		bool CheckIntersection(BoundingBox& p_Box);
		bool CheckRayIntersection(Vector3 p_Start, Vector3 p_Direction);
		bool ContainsPoint(const Vector3 p_Pos);

		void MakeDrawable();
		void SetDraw();
		void DisableDraw();
		void Draw(Matrix4 p_view, Matrix4 p_projection);
		bool IsDrawing();
		bool IsDrawable();
		bool HasCollided();
		void SetCollided(const bool& p_Collided);
		bool BeenShot();
		void SetShot(const bool& p_BeenShot);
	private:
		bool m_Drawable;
		bool m_Drawing;
	public:
		Vector3 m_Position;
		GameObject* m_Owner;
	private:
		Vector3 m_Volume;
		Vector3 m_HalfVol;
		Matrix4 m_Transform;
		float m_CheckRadius;
		bool m_Collided;
		bool m_BeenShot;

		
		
		Object m_Drawbox;
		helium::system::RenderSystem* m_render_system;
		helium::resource::Shader::Ptr		m_shader;
		helium::resource::VertexFormat::Ptr m_format;
		helium::resource::Texture::Ptr		m_texture;
		helium::resource::Sampler::Ptr		m_sampler;

	public:
		void SetRotationData(Matrix4 p_Rot, float p_Yaw, float p_Pitch);
	private:
		void UpdateRotation(float p_yaw, float p_pitch);
		Matrix4 m_Rot;
		float m_Yaw;
		float m_Pitch;

		Vector3 m_right;
		Vector3 m_up;
		Vector3  m_forward;
		
	};
}