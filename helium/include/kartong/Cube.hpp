#pragma once

#include "stdafx.h"
#include <Kartong/GameObject.hpp>
#include <helium/scene/camera.hpp>
#include <helium/scene/transform.hpp>

//#include <kartong/CollisionManager.hpp>
using namespace helium;


namespace kartong
{

	static Lighting sun;
	class Cube// : public GameObject
	{
	public:
		Cube(std::string p_texture, Vector3 p_Vol);
		~Cube();

		typedef std::unique_ptr<Cube> Ptr;
		static Ptr Create(std::string p_texture, Vector3 p_Vol);


		void Initialize();
		void Update(const float& p_Deltatime);
		void Draw(scene::Camera* p_Camera);

		void SetPosition(Vector3 p_Pos);
		void SetVolume(Vector3 p_Vol);
		void SetScale(Vector3 p_Scale);
		void SetRotation(Vector3 p_axis);

		Vector3 GetVolume();

	private:
		Vector3 m_Position;
		Vector3 m_Rotation;
		Vector3 m_Scale;
		Vector3 m_PrevPos;
		Vector3 m_Volume;

		Object m_Cube;

		double m_Time;


		helium::system::RenderSystem* m_render_system;
		helium::resource::Shader::Ptr		m_shader;
		helium::resource::VertexFormat::Ptr m_format;
		helium::resource::Texture::Ptr		m_texture;
		helium::resource::Sampler::Ptr		m_sampler;

		scene::Transform m_Transform;

	};

}