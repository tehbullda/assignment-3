#pragma once

#include <helium/os_event_listener.hpp>
#include <helium/gameplay/state_manager.hpp>
#include <helium/gameplay/abstract_state.hpp>
#include <kartong/MenuButton.hpp>
#include <kartong/Network_system.hpp>

namespace helium
{
	class StateMenu : public gameplay::AbstractState
	{
	public:
		~StateMenu();

		void initialize();
		void shutdown();
		bool update();
		void draw();
		uint32_t next();

	protected:
		Time m_start_time;
		Time m_total_time;
		uint32_t m_id_hash;
		uint32_t m_next;
		system::RenderSystem* m_render_system;
		system::AudioSystem* m_audio_system;
		kartong::NetworkSystem::Ptr m_network_system;
		kartong::input::Input::Ptr m_Input;
		std::vector<kartong::MenuButton*> m_buttons;
		kartong::GUITexture::Ptr m_Logo;

		Mouse::Ptr m_mouse;

	};
}
