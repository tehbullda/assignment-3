#pragma once

#include "stdafx.h"

namespace kartong
{

class SolidCube : public GameObject
{
public:
	SolidCube(const std::string& p_File, const Vector3& p_Volume, const Vector3& p_Pos);
	~SolidCube();

	typedef std::unique_ptr<SolidCube> Ptr;
	static Ptr Create(const std::string& p_File, const Vector3& p_Volume, const Vector3& p_Pos);

	void Initialize();
	void Update(const float& p_Deltatime);
	void Draw(scene::Camera* p_Camera);

private:
	CollisionManager* m_CollisionManager;
	BoundingBox* m_Bounds;
	Cube::Ptr m_Cube;
	float m_LongestRadius;
};
}