#pragma once

#include "stdafx.h"

namespace kartong
{
	class HUDAmmo
	{
	public:
		HUDAmmo();
		~HUDAmmo();

		typedef std::unique_ptr<HUDAmmo> Ptr;
		static Ptr Create();

		void Initialize();
		void Update(int p_Current, int p_Max);
		void Draw();

		void SetPosition(Vector2 p_Pos);
	private:
		void UpdatePositions();
		void DrawRound(Vector2 p_Pos);
		Vector2 m_Size;
		Vector2 m_Position;
		Vector2 m_PrevPosition;
		float m_Padding;
		
		int m_PrevCurrent;
		int m_PrevMax;
		int m_Current;
		int m_Max;

		GUITexture::Ptr m_TexRound;
		//GUITexture::Ptr m_Background;
		std::vector<Vector2> m_Positions;



	};
}