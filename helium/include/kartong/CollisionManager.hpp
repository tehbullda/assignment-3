#pragma once

#include "stdafx.h"
#include <kartong/BoundingBox.hpp>

namespace kartong
{
	class CollisionManager
	{
	private:
		CollisionManager();
	public:
		~CollisionManager();

		typedef std::unique_ptr<CollisionManager> Ptr;
		static Ptr Create();

		BoundingBox* CreateNewBox(GameObject* p_Owner, Vector3 p_Pos, Vector3 p_Volume, float p_Range);

		
		void CheckCollision(BoundingBox* m_Box);
		bool CheckHasCollided(BoundingBox* m_Box, BoundingBox** m_Collided);
		void CheckCollision(Vector3 p_Pos, Vector3 p_Dir);
		

		void MakeAllDrawable();
		void SetDrawAll();
		void DisableDrawAll();
		void DrawAll(Matrix4 p_view, Matrix4 p_projection);
		bool IsDrawingAll();

		int GetSize();


		std::vector<BoundingBox*> m_Boxes;

		
	private:

		bool m_Drawing;





	};
}