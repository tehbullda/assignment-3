#pragma once
namespace kartong {
	enum EDirection {
		UP,
		DOWN,
		LEFT,
		RIGHT,
		RANDOM
	};
	class Particle {
	public:
		//direction is random by default, lifetime 0.0f is random, 4.0f is the max allowed
		Particle(const std::string &texture, const Vector3 &position, const Vector3 &size, const float &speed, const float &lifetime, EDirection direction = EDirection::RANDOM);
		~Particle();

		void Update(const Vector3 &playerposition, const float &deltatime);
		void Draw(scene::Camera* p_Cam);

		bool HasTimedout();
		void Setposition(const Vector3 &pos);
	private:
		void Load(const std::string &filename, const Vector3 &size);
		void LookAtPlayer(const Vector3 &playerpos);
		void SetDirection(EDirection dir);
	private:
		scene::Transform m_Transform;
		Vector3 m_position;
		Vector3 m_direction;
		float m_speed;
		float m_lifetime;
		EDirection m_directiontype;
		bool m_destroy;

		Object m_element;
		helium::system::RenderSystem *m_render_system;
		helium::resource::Shader::Ptr		m_shader;
		helium::resource::VertexFormat::Ptr m_format;
		helium::resource::Texture::Ptr		m_texture;
		helium::resource::Sampler::Ptr		m_sampler;


	};
}