#pragma once

#include "stdafx.h"
#include <kartong/Cube.hpp>


namespace kartong
{
	static Object skybox;


	class Skybox
	{
	public:
		Skybox(const std::string& p_texture);
		~Skybox();

		typedef std::unique_ptr<Skybox> Ptr;
		static Ptr Create(const std::string& p_texture);

		void Draw(helium::Matrix4 p_view, helium::Matrix4 p_projection);


	private:

		helium::system::RenderSystem* m_render_system;
		helium::resource::Shader::Ptr		m_shader;
		helium::resource::VertexFormat::Ptr m_format;
		helium::resource::Texture::Ptr		m_texture;
		helium::resource::Sampler::Ptr		m_sampler;
	};
};