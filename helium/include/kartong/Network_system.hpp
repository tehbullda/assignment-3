#pragma once
#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")
namespace kartong {
	enum MessageType {
		SERVER_ADDCLIENT,
		SERVER_MAKEHOST,
		SERVER_HOSTADDR,
		CONNECTED,
		UPDATEPOS,
		UPDATEACTION,
		DISCONNECTED,
		PLAYERLIST,
		MESSAGE
	};
//#pragma pack(push, 1)
	struct Message {
		MessageType m_type;
		std::string m_identifier;
		Time m_timestamp;
	};
	struct PositionMsg : Message {
		Vector3 m_pos;
		Vector3 m_dir;
	};
	struct ActionMsg : Message {
		Vector3 m_pos;
		Vector3 m_lookdir;
		input::actions::ACTIONS m_action;
	};
	struct MessageMsg : Message {
		std::string m_message;
	};
//#pragma pack(pop)

	class NetworkSystem {
		struct EnemyPlayer {
			EnemyPlayer() {
				m_addr.sin_family = AF_INET;
				m_addr.sin_port = htons(7777);
				memset(m_addr.sin_zero, 0, sizeof(m_addr.sin_zero));
				m_name = "Default";
				m_dirty = false;
				m_pos = Vector3(0, 0, 0);
				m_shooting = false;
				m_dir = Vector3(0, 0, 0);
			}
			EnemyPlayer(const std::string &name, bool dirty, Vector3 pos, bool shooting, Vector3 dir) {
				m_name = name;
				m_dirty = dirty;
				m_pos = pos;
				m_shooting = shooting;
				m_dir = dir;
			}
			struct sockaddr_in m_addr;
			weapons::Weapons m_weapontype;
			std::string m_name;
			bool m_dirty;
			Vector3 m_pos;
			bool m_shooting;
			Vector3 m_dir;
			int m_bulletsleft;
		};
	public:
		typedef std::unique_ptr<NetworkSystem> Ptr;
		static Ptr create(const std::string &playername);
	public:
		NetworkSystem(const std::string playername);
		~NetworkSystem();

		void ConnectToServer(const std::string &addr);
		void ConnectToServer(struct sockaddr_in *addr);
		void ConnectToHost(struct sockaddr_in *addr);

		void SetPlayerName(const std::string &name);

		void CreatePositionMessage(const Vector3 &pos,const Vector3 &dir);
		void CreateActionMessage(input::actions::ACTIONS action, const Vector3 &pos, const Vector3 &lookdir);
		void CreateMsgMessage(const std::string &msg);

		int GetEnemyNumber();
		bool EnemyChanged(const uint32_t &index);
		bool EnemyShooting(const uint32_t &index);
		void EnemyUpdated(const uint32_t &index);
		Vector3 GetEnemyPosition(const uint32_t &index);
		Vector3 GetEnemyDir(const uint32_t &index);
		std::string GetEnemyName(const uint32_t &index);

		void Update(const float &deltatime);
		void UpdatePosition(const Vector3 &pos);
		void UpdateDirection(const Vector3 &dir);
		void UpdateBulletDirection(const Vector3 &dir);
		void UpdateAction(const input::actions::ACTIONS action);
		bool IncomingData();
		std::string DetermineSender(char *buffer, struct sockaddr_in addr);
		void PrintAddress(struct sockaddr_in *addr);
	private:
		void Send(const std::string &buffer, struct sockaddr_in address);
		std::string GeneratePositionMessage(PositionMsg msg);
		std::string GenerateActionMessage(ActionMsg msg);
		std::string GenerateMsgMessage(MessageMsg msg);
		std::string GenerateMessage(Message msg);
		std::string GeneratePlayerList();

		void AddEnemy(const std::string &name, bool dirty, Vector3 pos, bool shooting, Vector3 dir);
		void RemoveEnemy(const std::string &name);
		void SetEnemyData(const std::string &targetname, bool dirty, Vector3 pos, bool shooting, Vector3 dir);
		bool PlayerExists(const std::string &name);

		void HandleMessage(const std::string &buffer, bool knownclient);
		void HandleServerMessage(const std::string &buffer);

	private:
		SOCKET m_sock;
		sockaddr_in m_serveraddress;
		WSAData m_data;
		std::string m_playeridentifier;
		Vector3 m_playerposition;
		Vector3 m_playerdirection;
		Vector3 m_playerbulletdir;
		input::actions::ACTIONS m_playeraction;
		std::vector<std::string> m_messages;
		//std::map<std::string, std::pair<std::pair<bool, Vector3>, std::pair<bool, Vector3>>> m_enemyplayers;
		std::vector<EnemyPlayer*> m_enemyplayers;
		EnemyPlayer* m_host;
		bool m_playermoved;
		int m_numKnownConnections;
		float m_messagetimer;
		bool m_connected;
		bool m_ishost;
	};
}