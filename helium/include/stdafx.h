// stdafx.h 

#pragma once

#include <SDKDDKVer.h>

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>
#include <cstdint>

#include <sstream>
#include <fstream>
#include <memory>
#include <list>
#include <array>
#include <vector>
#include <unordered_map>
#include <queue>

#include <config.hpp>
#include <Helium/debug.hpp>
#include <Helium/string.hpp>
#include <Helium/math.hpp>
#include <Helium/time.hpp>
#include <Helium/visitor.hpp>
#include <helium/mouse.hpp>
#include <helium/keyboard.hpp>
#include <helium/predef.hpp>

#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>

#include <kartong/Global.hpp>

#include <kartong/ResourceManager.hpp>
//#include <kartong/Cube.hpp>
#include <kartong/XboxController.hpp>
#include <kartong/KeyBindings.hpp>
#include <kartong/Skybox.hpp>
#include <kartong/GUITexture.hpp>
#include <kartong/Weapon.hpp>
#include <kartong/GameObject.hpp>
#include <kartong/PlayerObject.hpp>
#include <kartong/EnemyObject.hpp>
#include <kartong/Ground.hpp>
#include <kartong/BoundingBox.hpp>
#include <kartong/CollisionManager.hpp>
#include <kartong/Physics_system.hpp>
#include <kartong/HUD_Ammo.hpp>
#include <kartong/HealthPack.hpp>
#include <kartong/Model.hpp>
#include <kartong/Particle_system.hpp>
#include <kartong/ShootableTestDummy.hpp>






//hej laban
