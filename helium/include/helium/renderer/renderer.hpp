// renderer.hpp

#ifndef RENDERER_HPP_INCLUDED
#define RENDERER_HPP_INCLUDED

namespace helium
{
	namespace system
	{
		class RenderSystem;
	}

	namespace renderer
	{
		class Renderer
		{
		public:
			Renderer(system::RenderSystem* render_system);
			~Renderer();

		private:
			system::RenderSystem* m_render_system;
		};
	}
}

#endif // RENDERER_HPP_INCLUDED
