// render_dispatcher.hpp

#ifndef RENDERDISPATCHER_HPP_INCLUDED
#define RENDERDISPATCHER_HPP_INCLUDED

#include <helium/service_locator.hpp>

namespace helium
{
	namespace system
	{
		class RenderSystem;
	}

	namespace renderer
	{
		class RenderQueue;

		class RenderDispatcher
		{
		private: // non-copyable
			RenderDispatcher(const RenderDispatcher&);
			RenderDispatcher& operator=(const RenderDispatcher&);

		public:
			typedef std::unique_ptr<RenderDispatcher> Ptr;

			static Ptr create(system::RenderSystem* render_system);

		private:
			RenderDispatcher(system::RenderSystem* render_system);

		public:
			~RenderDispatcher();

			void add_queue(RenderQueue* queue);
			void process();

		private:
			system::RenderSystem* m_render_system;
			std::list<RenderQueue*> m_queue_list;
		};
	}
}

#endif // RENDERDISPATCHER_HPP_INCLUDED
