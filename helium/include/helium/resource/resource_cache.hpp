// resource_cache.hpp

#ifndef RESOURCECACHE_HPP_INCLUDED
#define RESOURCECACHE_HPP_INCLUDED

#include <helium/resource/resource_creator.hpp>

namespace helium
{
	namespace system
	{
		class AudioSystem;
		class RenderSystem;
	}

	namespace resource
	{
		class ResourceCache
		{
			class AbstractResource
			{
			public:
				virtual ~AbstractResource() {}
				virtual void release() = 0;
			};

			template <class T>
			class Resource : public AbstractResource
			{
			public:
				Resource(std::unique_ptr<T>&& ptr) : m_resource(std::move(ptr)) { }
				T* get_resource()  {  return m_resource.get(); }
				void release() { T* res = m_resource.release(); res->dispose();  delete res; }

			private:
				std::unique_ptr<T> m_resource;
			};

		private: // non-copyable
			ResourceCache(const ResourceCache&);
			ResourceCache& operator=(const ResourceCache&);

		public:
			typedef std::unique_ptr<ResourceCache> Ptr;

			static Ptr create();

		private:
			ResourceCache();

		public:
			~ResourceCache();

			void set_directory(const std::string& directory);

			bool has_resource(const std::string& filename, const std::string& group = "default");
			void unload_resource(const std::string& filename, const std::string& group = "default");
			void unload_all_resources(const std::string& group = "default");

			template <class T>
			T* load_resource(const std::string& filename, const std::string& group = "default")
			{
				uint32_t group_hash = String::hash32(group.c_str(), (uint32_t)group.length());
				uint32_t name_hash = String::hash32(filename.c_str(), (uint32_t)filename.length());

				auto git = m_groups.find(group_hash);
				if (git == m_groups.end())
				{
					m_groups.insert(std::pair<uint32_t, Group>(group_hash, Group()));
					git = m_groups.find(group_hash);
				}

				ResourceMap& resmap = git->second.m_resources;
				auto rit = resmap.find(name_hash);
				if (rit != resmap.end())
				{
					Debug::write(EDebugLevel::Info, 
						String::format("resourcecache: accessing resource: %s [%s]", 
						filename.c_str(), group.c_str()));
					return static_cast<Resource<T>*>(rit->second)->get_resource();
				}

				resmap.insert(std::pair<uint32_t, AbstractResource*>(name_hash, nullptr));
				rit = resmap.find(name_hash);
				rit->second = new Resource<T>(T::create(m_directory + filename));
				Debug::write(EDebugLevel::Info, 
					String::format("resourcecache: loading resource: %s [%s]", 
						filename.c_str(), group.c_str()));
				return static_cast<Resource<T>*>(rit->second)->get_resource();
				//T* t = static_cast<Resource<T>*>(rit->second);
				//if (t->is_manual()) return t;
				//return ResourceCreator<T>(t->get_resource());
			}

		private:
			std::string m_directory;
			system::AudioSystem* m_audio_system;
			system::RenderSystem* m_render_system;

		private:
			typedef std::unordered_map<uint32_t, AbstractResource*> ResourceMap;
			struct Group
			{
				ResourceMap m_resources;
			};			
			typedef std::unordered_map<uint32_t, Group> GroupMap;

			GroupMap m_groups;			
		};
	}
}

#endif // ResourceCache_HPP_INCLUDED
