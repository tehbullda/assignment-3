// resource_creator.hpp

#ifndef RESOURCECREATOR_HPP_INCLUDED
#define RESOURCECREATOR_HPP_INCLUDED

namespace helium
{
	namespace system
	{
		class AudioSystem;
		class RenderSystem;
	}

	namespace resource
	{
		class Audio;
		class Model;
		class Sampler;
		class Shader;
		class Texture;
		class VertexBuffer;
		class IndexBuffer;

		// note(tommi): todo below
		class Font;

		template <class Res>
		struct ResourceCreator
		{
			static Res* create(Res* res) { return res; }
		};

		// specializations
		template <>
		struct ResourceCreator<Audio>
		{
			static Audio* create(Audio* res);
		};

		template <>
		struct ResourceCreator<Model>
		{
			static Model* create(Model* res);
		};

		template <>
		struct ResourceCreator<Sampler>
		{
			static Sampler* create(Sampler* res);
		};

		template <>
		struct ResourceCreator<Shader>
		{
			static Shader* create(Shader* res);
		};

		template <>
		struct ResourceCreator<Texture>
		{
			static Texture* create(Texture* res);
		};

		template <>
		struct ResourceCreator<VertexBuffer>
		{
			static VertexBuffer* create(VertexBuffer* res);
		};

		template <>
		struct ResourceCreator<IndexBuffer>
		{
			static IndexBuffer* create(IndexBuffer* res);
		};
	}
}

#endif // RESOURCECREATOR_HPP_INCLUDED
