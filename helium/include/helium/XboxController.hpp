#pragma once

#include "stdafx.h"

#include <Xinput.h>

#pragma comment(lib, "Xinput9_1_0.lib")

namespace helium
{
	class XboxController
	{

	public:


		XboxController() : m_DeadZoneX(0.05f), m_DeadZoneY(0.05f) {}
		XboxController(float p_dzX, float p_dzY) : m_DeadZoneX(p_dzX), m_DeadZoneY(p_dzY) {};

		typedef std::unique_ptr<XboxController> Ptr;
		static Ptr Create(float p_dzX, float p_dzY);

		float m_LeftStickX;
		float m_LeftStickY;
		float m_RightStickX;
		float m_RightStickY;
		float m_LeftTrigger;
		float m_RightTrigger;

		int GetPort();
		XINPUT_GAMEPAD *GetState();
		bool CheckConnection();
		bool Refresh();

		bool IsPressed(WORD);
		bool IsPressedOnce(WORD);

	private:


		int m_cId;
		XINPUT_STATE state;
		XINPUT_STATE prev;

		float m_DeadZoneX;
		float m_DeadZoneY;

	};
};
