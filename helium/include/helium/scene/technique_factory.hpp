// tecnique_factory.hpp

#ifndef TECHNIQUEFACTORY_HPP_INCLUDED
#define TECHNIQUEFACTORY_HPP_INCLUDED

namespace helium
{
	namespace resource
	{
		class Material;
	}

	namespace scene
	{
		class Technique;

		class TechniqueFactory
		{
		private: // non-copyable
			TechniqueFactory(const TechniqueFactory&);
			TechniqueFactory& operator=(const TechniqueFactory&);

		public:
			static Technique* create_technique(const resource::Material* material);
		};
	}
}

#endif // TECHNIQUEFACTORY_HPP_INCLUDED
