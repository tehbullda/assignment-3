// transform.hpp

#ifndef TRANSFORM_HPP_INCLUDED
#define TRANSFORM_HPP_INCLUDED

namespace helium
{
	namespace scene
	{
		class Transform
		{
		public:
			Transform();
			Transform(const Transform& rhs);
			Transform& operator=(const Transform& rhs);

			void recalculate();

			void set_position(const Vector3& position);
			void set_rotation(const Vector3& rotation);
			void set_scale(const float scale);
			const Vector3& get_position() const;
			const Vector3& get_rotation() const;
			const Vector3& get_scale() const;
			const Matrix4& get_transform() const;

		private:
			bool m_dirty;
			Matrix4 m_transform;
			Vector3 m_position;
			Vector3 m_scale;
			Vector3 m_rotation;
		};
	}
}

#endif // TRANSFORM_HPP_INCLUDED
