#include "stdafx.h"

#include <map>

namespace input
{
	namespace actions
	{
		enum ACTIONS
		{
			UP,
			DOWN,
			LEFT,
			RIGHT,
			JUMP,
			CROUCH,
			SHOOT,
			SIGHT,
			SIZE
		};
	}
	class XBOXKeyBindings
	{
	public:
		XBOXKeyBindings();
		~XBOXKeyBindings();

		typedef std::unique_ptr<XBOXKeyBindings> Ptr;
		static Ptr Create();

	private:
		void BindAxis(int p_bind, WORD p_button);
		void BindButton(int p_bind, WORD p_button);

	public:
		void Refresh();

		int GetControllerScheme();
		bool GetAction(int p_Key);
		bool GetActionOnce(int p_Key);
		float GetAxis(int p_Axis);


		void SetControllerScheme(int p_Scheme);

	public:
		enum SCHEMES
		{
			NORMAL,
			SPECIAL,
			NORMALINV,
			SPECIALINV
		};
		enum AXES
		{
			LX,
			LY,
			RX,
			RY,
			LT,
			RT
		};
	private:
		std::map<int, WORD> m_Binds;

		input::XboxController::Ptr m_xcontroller;
		bool InvertRY;
		int m_CurrentScheme;
	};

	class KBMKeyBindings
	{
	public:
		KBMKeyBindings();
		~KBMKeyBindings();

		typedef std::unique_ptr<KBMKeyBindings> Ptr;
		static Ptr Create();

		void BindKey(int p_bind, int p_KeyCode);
		void ClearBind(int p_KeyCode);
		void ClearAll();

		bool GetAction(int p_Key);
		bool GetActionOnce(int p_Key);
		bool GetActionReleased(int p_Key);

		void Refresh();
	private:
		std::map<int, WORD> m_Binds;


		bool current[256];
		bool previous[256];
	};

	class Input
	{
	public:
		Input();
		~Input();
		typedef std::unique_ptr<Input> Ptr;
		static Ptr Create();

		void KBM_BindKey(int p_bind, int p_KeyCode);
		void KBM_ClearBind(int p_KeyCode);
		void KBM_ClearAll();
		void KBM_SetDefaultBinds();

		void XB_SetControllerScheme(int p_Scheme);
		int XB_GetControllerScheme();

		float XB_GetAxis(int p_Axis);


		void Refresh();



		bool GetAction(int p_Action);
		bool GetActionOnce(int p_Action);
		bool GetActionReleased(int p_Action);


	private:
		input::KBMKeyBindings::Ptr m_KBM;
		input::XBOXKeyBindings::Ptr m_XB;

	};


};