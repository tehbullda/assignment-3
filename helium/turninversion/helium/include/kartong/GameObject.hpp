#pragma once

#include "stdafx.h"
#include <helium/scene/camera.hpp>

namespace kartong
{
	using namespace helium;

	struct Material
	{
		float ambient;
		float diffuse;
		float specular;
		float shininess;
	};
	struct Object
	{
		int shader;
		int texture;
		int sampler;
		int format;
		int buffer;
		int index;
		int blend;
		int depth;
		int raster;
		int drawcount;
		BoundingSphere BS;
		Material material;
		Matrix4 world;
	};
	struct Lighting
	{
		Vector3 direction; float pad0;
		Vector3 diffuse; float pad1;
		Vector3 ambient; float pad2;
		Vector3 eye; float pad3;
	};
	
	enum ObjectType
	{
		PLAYER,
		INTERACTIVE,
		ENVIRONMENT,
		GROUND,
	};
	enum PhysicsType
	{
		PASS,
		STATIC,
		KINETIC
	};

	class GameObject
	{
	public:
		GameObject();
		virtual ~GameObject();
		void SetTag(const std::string p_Tag);
		void SetType(const int p_Type);
		void SetPhysicsType(const int &type);
		void SetName(const std::string p_Name);

		std::string GetTag();
		int GetType();
		int GetPhysicstype();
		std::string GetName();

		Vector3 m_Position;
		virtual void SetPosition(Vector3 p_Pos);
		Vector3 GetPosition();

		Object obj;
	public:
		virtual void Initialize() = 0;
		virtual void Update(const float& p_Deltatime) = 0;
		virtual void Draw(scene::Camera* p_Camera) = 0;

		BoundingSphere getBoundingSphere();

		virtual void Interact(){};
	//	virtual void RequirementMet(){};

	private:
		std::string m_Tag;
		std::string m_Name;
		uint32_t m_Type;
		int m_Physicstype;

		

	};

};