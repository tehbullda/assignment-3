#pragma once

#include "stdafx.h"
#include <map>

using namespace helium::resource;


namespace kartong
{


	class ResourceManager
	{
	public:
		ResourceManager();
		~ResourceManager();

		uint32_t LoadShader(const std::string& p_Shader);
		uint32_t LoadFormat(const std::string& p_Format, const uint32_t shaderid);
		uint32_t LoadSampler(const std::string& p_Sampler);
		uint32_t LoadTexture(const std::string& p_Texture);

		uint32_t LoadDepthDefault();
		uint32_t LoadDepth(const std::string& p_Depth, bool write, bool read, system::EDepthMode mode);
		uint32_t LoadRasterDefault();
		uint32_t LoadRaster(const std::string& p_Raster, helium::system::EFillMode fillmode, helium::system::ECullMode cullmode);
		uint32_t LoadBlendDefault();
		uint32_t LoadBlend(const std::string& p_Blend, helium::system::EBlendMode src_color, helium::system::EBlendMode dst_color,
			helium::system::EBlendMode src_alpha, helium::system::EBlendMode dest_alpha, helium::system::EBlendOp src_op, helium::system::EBlendOp dest_op, uint32_t mask = 15U, bool atoc = false);

	private:
		void CreateDefaults();

	private:
		helium::system::RenderSystem *m_render_system;
		std::map<std::string, uint32_t> m_Shaders;
		std::map<std::string, uint32_t> m_Formats;
		std::map<std::string, uint32_t> m_Samplers;
		std::map<std::string, uint32_t> m_Textures;
		std::map<std::string, uint32_t> m_Depthstates;
		std::map<std::string, uint32_t> m_Rasterizers;
		std::map<std::string, uint32_t> m_Blendstates;
	};
}