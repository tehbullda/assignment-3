#pragma once

#include "stdafx.h"
#include <kartong/ShootableTestDummy.hpp>


namespace kartong
{

	class TargetManager
	{
	public:
		TargetManager();
		~TargetManager();
		typedef std::unique_ptr<TargetManager> Ptr;
		static Ptr Create();

		void Initialize();
		void Update(const float& p_Deltatime);
		void Draw(scene::Camera* m_Camera);

	private:
		void InitializeWave(const int& p_Wave);

	private:
		system::AudioSystem* m_audio_system;

		int m_CurrentWave;
		float m_Timer;
		std::vector<ShootableTestDummy*> m_Targets;

		GUITexture::Ptr m_Wave1;
		GUITexture::Ptr m_Wave2;
		GUITexture::Ptr m_Wave3;

		
	};

}