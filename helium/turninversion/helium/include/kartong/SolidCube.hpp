#pragma once

#include "stdafx.h"
#include <kartong/ResourceManager.hpp>

namespace kartong
{

class SolidCube : public GameObject
{
public:
	SolidCube(const std::string& p_File, const Vector3& p_Volume, const Vector3& p_Pos);
	~SolidCube();

	typedef std::unique_ptr<SolidCube> Ptr;
	static Ptr Create(const std::string& p_File, const Vector3& p_Volume, const Vector3& p_Pos);

	void Initialize();
	void Update(const float& p_Deltatime);
	void Draw(scene::Camera* p_Camera);

private:
	CollisionManager* m_CollisionManager;
	ResourceManager *m_resource_manager;
	BoundingBox* m_Bounds;
	Cube::Ptr m_Cube;
	float m_LongestRadius;
};
}