#pragma once
#include <random>

namespace Randomizer {
	int GetRandomInt(int p_min = 0, int p_max = INT_MAX);
	int GetRandomIntFromWeights(std::initializer_list<double> p_weights);
	double GetRandomDouble(double p_min = 0.0, double p_max = 0.0);
	double GetNormalDistribution(double p_mean, double p_deviation);
};