#pragma once

#include "stdafx.h"
#include <kartong/BoundingBox.hpp>
#include <kartong/CollisionManager.hpp>

namespace kartong
{
	namespace geometry
	{

		class Ground : GameObject
		{
		public:
			Ground(const std::string& p_texture);
			~Ground(){ };

			//typedef std::unique_ptr<Ground> Ptr;
			//static Ptr Create(const std::string& p_texture);

			void Initialize(){};
			void  Update(const float& p_Deltatime) { p_Deltatime; };
			void Draw(scene::Camera* p_Camera);
		private:
			Vector3 m_Volume;
			CollisionManager* m_CollisionManager;
			BoundingBox* m_Bounds;
			helium::system::RenderSystem* m_render_system;
			helium::resource::Shader::Ptr		m_shader;
			helium::resource::VertexFormat::Ptr m_format;
			helium::resource::Texture::Ptr		m_texture;
			helium::resource::Sampler::Ptr		m_sampler;

			Cube::Ptr m_Box;
		};

	}
}