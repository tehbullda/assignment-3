#pragma once

#include "stdafx.h"
#include <kartong/Model.hpp>
#include <kartong/Cube.hpp>

namespace kartong
{
	class SolidPart : GameObject
	{
	public:
		SolidPart(const std::string& p_Model, const Vector3 p_Pos);
		~SolidPart();

		typedef std::unique_ptr<SolidPart> Ptr;
		static Ptr Create(const std::string& p_Model, Vector3 p_Pos);

		void Initialize();
		void Update(const float& p_Deltatime);
		void Draw(scene::Camera* p_Cam);


	private:
		Model::Ptr m_Model;
		BoundingBox* m_Bounds;
		CollisionManager* m_CollisionManager;

	};

	class Arc : public GameObject
	{
	public:
		//*Only 0, 90, 180 works for angle
		Arc(const Vector3& p_Pos, const bool& p_Rotated90);
		~Arc();



		typedef std::unique_ptr<Arc> Ptr;
		static Ptr Create(const Vector3& p_Pos, const bool& p_Rotated90);

			void Initialize();
		void Update(const float& p_Deltatime);
		void Draw(scene::Camera* p_Camera);


	private:
		Model::Ptr m_Model;
		BoundingBox* m_LPillar;
		BoundingBox* m_RPillar;
		CollisionManager* m_CollisionManager;
	};
	class Stairs : GameObject
	{
	public:
		//*Only 0, 90, 180 works for angle
		Stairs(const Vector3& p_Pos);
		~Stairs();



		typedef std::unique_ptr<Stairs> Ptr;
		static Ptr Create(const Vector3& p_Pos);

		void Initialize();
		void Update(const float& p_Deltatime);
		void Draw(scene::Camera* p_Camera);


	private:
		Cube::Ptr m_Step1;
		Cube::Ptr m_Step2;
		Cube::Ptr m_Step3;
		Cube::Ptr m_Step4;
		Cube::Ptr m_Step5;

		BoundingBox* m_Step1b;
		BoundingBox* m_Step2b;
		BoundingBox* m_Step3b;
		BoundingBox* m_Step4b;
		BoundingBox* m_Step5b;

		CollisionManager* m_CollisionManager;
	};

}