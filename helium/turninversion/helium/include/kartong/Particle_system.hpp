#pragma once
#include <kartong/Particle.hpp>
namespace kartong {
	class ParticleSystem {
	public:
		typedef std::unique_ptr<ParticleSystem> Ptr;
		static Ptr create(const std::string &particledirectory);
	public:
		ParticleSystem(const std::string &particledirectory);
		~ParticleSystem();

		void Update(const Vector3 &playerposition, const float &deltatime);
		void Draw(scene::Camera* p_Cam);

		void CreateParticle(const std::string &texture, const Vector3 &position, const Vector3 &size, const float &speed, const float &lifetime, EDirection direction = EDirection::RANDOM);

	private:
		std::string m_directory;
		std::vector<Particle*> m_particles;
	};
}