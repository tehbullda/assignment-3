#pragma once

#include <helium/os_event_listener.hpp>
#include <helium/gameplay/state_manager.hpp>
#include <helium/gameplay/abstract_state.hpp>
#include <helium/resource/Audio.hpp>
#include <kartong/Level.hpp>
#include <kartong/ShootableTestDummy.hpp>
#include <kartong/TextObject.hpp>

namespace helium
{
	class StateGame : public gameplay::AbstractState
	{
	public:
		~StateGame();

		void initialize();
		void shutdown();
		bool update();
		void draw();
		uint32_t next();

	protected:
		Time m_start_time;
		Time m_total_time;
		uint32_t m_id_hash;
		gameplay::StateManager::Ptr m_state_manager;
		system::CollisionSystem* m_collision_system;
		system::RenderSystem* m_render_system;
		system::AudioSystem* m_audio_system;
		//kartong::NetworkSystem::Ptr m_network_system;
		kartong::ParticleSystem::Ptr m_particle_system;
		kartong::input::Input* m_Input;
		uint32_t m_next;
		bool m_paused, m_muted;

		kartong::TextObject *m_testtext;

		//kartong::Skybox::Ptr m_Skybox;
		

		kartong::PlayerObject* m_Player;
		scene::Camera* camera;
		//std::vector<kartong::EnemyObject*> m_enemies;
		//kartong::geometry::Ground* m_Ground;
		kartong::CollisionManager* m_CollisionManager;

		kartong::HUDAmmo::Ptr m_HudAmmo;
		/*kartong::HealthPack::Ptr m_TestHPPack;
		
		kartong::Model::Ptr m_TestModel;
		kartong::SolidPart::Ptr m_LevelPart;*/

		kartong::Level::Ptr m_Level;

		std::vector<kartong::MenuButton*> m_buttons;
		helium::Mouse::Ptr m_mouse;

		//remove later
		float rotarestuff;
	};
}
