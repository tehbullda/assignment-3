#pragma once

#include "stdafx.h"

namespace kartong
{
	class HealthPack : public GameObject
	{
	public:
		HealthPack();
		~HealthPack();

		typedef std::unique_ptr<HealthPack>Ptr;
		static Ptr Create();

		
		void Initialize();
		void Update(const float& p_Deltatime);
		void Draw(scene::Camera* m_Cam);
		void SetActive(const bool& p_Active);
		bool IsActive();

		void Interact();
		//bool RequirementMet();

		float GetHealth();


	private:
		CollisionManager* m_CollisionManager;
		BoundingBox* m_Bounds;
		Cube::Ptr m_Cube;
		Cube::Ptr m_Bot;

		double m_RotateTime;

		float m_ResetWhen;
		float m_CountTimer;
		bool m_Active;
		bool m_Scaling;
		float m_Scale;


	};
}