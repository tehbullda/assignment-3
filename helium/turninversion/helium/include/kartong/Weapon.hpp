#pragma once

#include "stdafx.h"

#include "kartong/Model.hpp"
#include <kartong/Cube.hpp>
using namespace helium;

namespace kartong
{
	namespace weapons
	{
		enum Weapons
		{
			SNIPER,
			RAILGUN,
			PISTOL//,
			//SIZE
		};

	}
	namespace weaponsoundtypes
	{
		enum WeaponSoundTypes
		{
			SHOT,
			AFTER,
			RELOAD//,
			//SIZE
		};
	}
	class Weapon
	{
	public:
		Weapon();
		~Weapon();

		void Initialize();
		void Update(float p_Deltatime);
		bool Shoot();
		void Reload();
		void Draw(scene::Camera* p_Camera, bool p_Zoomed);

		void SetDamage(const float p_Damage);
		void SetRange(const float p_Range);
		void SetMaxAmmo(const int p_Ammo);
		void SetReloadTime(const float p_Time);
		void SetFirerate(const float p_Firerate);

		float GetDamage();
		float GetRange();
		int GetMaxAmmo();
		static int GetMaxAmmo(weapons::Weapons type);
		int GetCurrentammo();
		float GetReloadTime();
		float GetFirerate();

		void SetPosition(helium::Vector3 p_Position);
		void SetRotation(helium::Vector3 p_Rotation);
		void SetScale(helium::Vector3 p_Scale);

		void SetWeapon(const int& p_Weapon);

		bool ShotFired();
	private:
		int m_CurrentWeapon;
		struct  stats
		{
			float Damage;
			float Range;
			float ReloadTime;
			float FireRate;
			int MaxAmmo;
		} m_Stats;
		int m_Ammo;
		bool m_CanShoot;
		bool m_Reloading;
		bool m_CanPlayAfterSound;
		bool m_ShotFired;
		float m_Timer;
		float m_ReloadTimer;
		float m_AfterEffectTimer;
		float m_AfterEffectCounter;

		void Playsound(int p_Type);
		void DrawScope();
		void DrawIcon();
		

	private:
		system::AudioSystem* m_audio_system;

		Model::Ptr m_Weapon_Sniper;

		GUITexture::Ptr m_Weapon;
		GUITexture::Ptr m_SniperScope;
		GUITexture::Ptr m_Crosshair;

		GUITexture::Ptr m_SniperIcon;




		


		//Will need model;
	};
}