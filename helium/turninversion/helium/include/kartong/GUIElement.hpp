#pragma once

#include "stdafx.h"



namespace kartong
{

	struct Material
	{
		float ambient;
		float diffuse;
		float specular;
		float shininess;
	};

	struct Object
	{
		int shader;
		int texture;
		int sampler;
		int format;
		int buffer;
		int index;
		int blend;
		int raster;
		int drawcount;
		int depth;
		Material material;
		helium::Matrix4 world;
	};
	static Object element;


	class GUITexture
	{
	public:
		GUITexture(const std::string& p_texture, const helium::Vector2 p_Pos);
		~GUITexture();

		typedef std::unique_ptr<GUITexture> Ptr;
		static Ptr Create(const std::string& p_texture);

		void Draw(helium::Matrix4 p_view, helium::Matrix4 p_projection);
		
		void SetPos(const helium::Vector2 p_Pos);
		void SetNewTexture(const std::string& p_Texture);
		


		helium::Vector2 GetPos();


	private:
		helium::system::RenderSystem* m_render_system;
		helium::resource::Shader::Ptr		m_shader;
		helium::resource::VertexFormat::Ptr m_format;
		helium::resource::Texture::Ptr		m_texture;
		helium::resource::Sampler::Ptr		m_sampler;

		int p_PosX;
		int p_PosY;


	};
};