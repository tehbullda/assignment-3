// shader_parameter.hpp

#ifndef SHADERPARAMETER_HPP_INCLUDED
#define SHADERPARAMETER_HPP_INCLUDED

namespace helium
{
	namespace scene
	{
		class ShaderParameter
		{
			friend class ShaderUnit;
		private: // non-copyable
			ShaderParameter(const ShaderParameter&);
			ShaderParameter& operator=(const ShaderParameter&);

		public:
			typedef std::unique_ptr<ShaderParameter> Ptr;

			static Ptr create(const std::string& name);

		private:
			ShaderParameter(const std::string& name);

		public:
			~ShaderParameter();

			bool is_valid() const;
			uint32_t get_name_hash() const;
			uint32_t get_size() const;
			const void* get_buffer() const;

#if !defined(NDEBUG)
			const std::string& get_name() const;
#endif

		private:
			void set_size(uint32_t size);
			void set_buffer(const void* buffer);

		private:
			uint32_t m_name_hash;
			uint32_t m_size;
			void* m_buffer;
#if !defined(NDEBUG)
			std::string m_name;
#endif
		};
	}
}

#endif // SHADERPARAMETER_HPP_INCLUDED
