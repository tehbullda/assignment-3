// scene.hpp

#ifndef SCENE_HPP_INCLUDED
#define SCENE_HPP_INCLUDED

#include <helium/scene/camera.hpp>
#include <helium/scene/scene_node.hpp>
#include <helium/renderer/render_queue.hpp>
#include <helium/renderer/render_dispatcher.hpp>

namespace helium
{
	namespace scene
	{
		/*
			note(tommi): this class is used to render all 3d objects in a scene
		*/
		class Scene
		{
		private: // non-copyable
			Scene(const Scene&);
			Scene& operator=(const Scene&);

		public:
			typedef std::unique_ptr<Scene> Ptr;

			static Ptr create();

		private:
			Scene();

		public:
			~Scene();

			void pre_render();
			void post_render();
			void render();

			SceneNode* create_scene_node();
			void destroy_scene_node(SceneNode* node);

			void set_camera(Camera* camera);
			Camera* get_camera();

		private:
			Camera* m_camera;
			std::vector<SceneNode::Ptr> m_nodes;
			std::vector<SceneNode*> m_visibles;

			renderer::RenderQueue m_render_queue;
			renderer::RenderDispatcher::Ptr m_render_dispatcher;
		};
	}
}

#endif // SCENE_HPP_INCLUDED
