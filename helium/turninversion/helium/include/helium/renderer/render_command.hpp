// render_command.hpp

#ifndef RENDERCOMMAND_HPP_INCLUDED
#define RENDERCOMMAND_HPP_INCLUDED

namespace helium
{
	namespace renderer
	{
		enum class ERenderCommandType : uint8_t
		{
			Clear,
			Present,
			Shader,
			Texture,
			VertexFormat,
			VertexBuffer,
			IndexBuffer,
			Sampler,
			Blend,
			Depth,
			Rasterizer,
			ShaderConstant,
			Draw,
			DrawIndexed,
			Invalid,
		};

#pragma pack(push, 1)
		struct RenderCommand
		{
			struct SelectCommand {
				int32_t  m_id;
			};
			struct TextureCommand {
				int32_t  m_id;
				uint32_t m_slot;
			};
			struct SamplerCommand {
				int32_t  m_id;
				uint32_t m_slot;
			};
			struct ConstantCommand {
				uint32_t m_hash;
				uint32_t m_size;
				void*    m_data;
			};
			struct DrawCommand {
				uint32_t m_mode;
				uint32_t m_start;
				uint32_t m_count;
			};

			ERenderCommandType m_type;			
			union {
				int32_t					m_id;
				TextureCommand	m_texture;
				SamplerCommand	m_sampler;
				ConstantCommand m_constant;
				DrawCommand		  m_draw;
			} m_cmd;
		};
#pragma pack(pop)
	}
}

#endif // RENDERCOMMAND_HPP_INCLUDED
