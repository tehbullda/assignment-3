// render_queue.hpp

#ifndef RENDERQUEUE_HPP_INCLUDED
#define RENDERQUEUE_HPP_INCLUDED

#include <helium/renderer/render_command.hpp>

namespace helium
{
	namespace renderer
	{
		class RenderQueue
		{
		public:
			enum { MAX_QUEUE_SIZE = 4096 };
			RenderQueue();

			void reset();
			void push(RenderCommand& cmd);
			bool pop(RenderCommand& cmd);

		private:
			uint32_t m_index;
			uint32_t m_count;
			std::array<RenderCommand, MAX_QUEUE_SIZE> m_queue;
		};
	}
}

#endif // RENDERQUEUE_HPP_INCLUDED
