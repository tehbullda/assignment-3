// example_game.hpp

#ifndef EXAMPLEGAME_HPP_INCLUDED
#define EXAMPLEGAME_HPP_INCLUDED

#include <helium/gameplay/abstract_game.hpp>

namespace helium
{
	namespace example
	{
		class Game : public gameplay::AbstractGame
		{
		private: // non-copyable
			Game(const Game&);
			Game& operator=(const Game&);

		public:
			typedef std::unique_ptr<Game> Ptr;

			static gameplay::AbstractGame::Ptr create();

		private:
			Game();

		public:
			~Game();

			void process();
			virtual void initialize();
			virtual void shutdown();

		private:
			kartong::input::Input* m_Input;
			kartong::Skybox::Ptr m_Skybox;
			kartong::GUITexture::Ptr m_guitex;

			//kartong::PlayerObject::Ptr m_testplayer;
		};
	}
}

#endif // EXAMPLEGAME_HPP_INCLUDED
