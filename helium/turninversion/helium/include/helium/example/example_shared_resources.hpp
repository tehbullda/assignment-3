// example_shared_resources.hpp

#ifndef EXAMPLESHAREDRESOURCES_HPP_INCLUDED
#define EXAMPLESHAREDRESOURCES_HPP_INCLUDED

#include <helium/resource/font.hpp>
#include <helium/resource/model.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>

namespace helium
{
	namespace example
	{
		struct SharedResources
		{
			resource::Shader* m_shader;
			resource::Texture* m_texture;
			resource::VertexFormat* m_format;
			resource::Font* m_font;
			resource::VertexBuffer* m_vertex_buffer;
			resource::IndexBuffer* m_index_buffer;
		};
	}
}

#endif // EXAMPLESHAREDRESOURCES_HPP_INCLUDED
