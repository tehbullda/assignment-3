// texture.hpp

#ifndef TEXTURE_HPP_INCLUDED
#define TEXTURE_HPP_INCLUDED

#include <helium/resource/resource.hpp>

namespace helium
{
	namespace system
	{
		enum class ETextureType : uint32_t;
		enum class ETextureFormat : uint32_t;
	}
	namespace resource
	{
		class Texture : public Resource
		{
		private: // non-copyable
			Texture(const Texture&);
			Texture& operator=(const Texture&);

		public:
			typedef std::unique_ptr<Texture> Ptr;

			static Ptr create(const std::string& filename);
			static Ptr create(const std::vector<std::string>& filenames);

		private:
			Texture();

		public:
			~Texture();

			bool has_mipmaps() const;
			system::ETextureType get_type() const;
			system::ETextureFormat get_format() const;
			uint16_t get_width() const;
			uint16_t get_height() const;
			uint16_t get_depth() const;
			const uint8_t* get_pixel_data(uint32_t index = 0) const;

			void dispose();

		private:
			system::ETextureFormat m_format;
			system::ETextureType m_type;
			uint16_t m_width;
			uint16_t m_height;
			uint16_t m_depth;
			uint16_t m_mipmaps;
			uint8_t** m_pixels;
			bool m_stbi;
		};
	}
}

#endif // TEXTURE_HPP_INCLUDED
