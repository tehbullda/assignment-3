// resource.hpp

#ifndef RESOURCE_HPP_INCLUDED
#define RESOURCE_HPP_INCLUDED

namespace helium
{
	namespace resource
	{
		class Resource
		{
			template <class T> 
			friend struct ResourceCreator;

		public:
			Resource();
			virtual ~Resource();

			void set_id(uint32_t id);
			uint32_t get_id() const;

			virtual void dispose() = 0;

		protected:
			uint32_t m_id;
		};
	}
}

#endif // ABSTRACTRESOURCECREATOR_HPP_INCLUDED
