// render_resource_factory.hpp

#ifndef RENDERRESOURCEFACTORY_HPP_INCLUDED
#define RENDERRESOURCEFACTORY_HPP_INCLUDED

#include <helium/scene/shader_unit.hpp>
#include <helium/scene/texture_unit.hpp>

namespace helium
{
	namespace system
	{
		class RenderSystem;
	}

	namespace resource
	{
		// note(tommi): this class right here does to much
		//   it should be divided into smaller 'managers'

		class RenderResourceFactory
		{
		private:
			RenderResourceFactory(const RenderResourceFactory&);
			RenderResourceFactory& operator=(const RenderResourceFactory&);

		public:
			typedef std::unique_ptr<RenderResourceFactory> Ptr;

			static Ptr create();

		private:
			RenderResourceFactory();

		public:
			~RenderResourceFactory();

		private:
			system::RenderSystem* m_render_system;
		};
	}
}

#endif // RENDERRESOURCEFACTORY_HPP_INCLUDED
