// sampler.hpp

#ifndef SAMPLER_HPP_INCLUDED
#define SAMPLER_HPP_INCLUDED

#include <helium/resource/resource.hpp>

namespace helium
{
	namespace system
	{
		enum class ESamplerFilterMode : uint32_t;
		enum class ESamplerAddressMode : uint32_t;
	}

	namespace resource
	{
		class Sampler : public Resource
		{
		private: // non-copyable
			Sampler(const Sampler&);
			Sampler& operator=(const Sampler&);

		public:
			typedef std::unique_ptr<Sampler> Ptr;

			static Ptr create(const std::string& filename);

		private:
			Sampler();

		public:
			~Sampler();

			bool has_addr_u() const;
			bool has_addr_v() const;
			bool has_addr_w() const;

			system::ESamplerFilterMode get_filter_mode() const;
			system::ESamplerAddressMode get_address_mode(uint32_t index) const;
			system::ESamplerAddressMode get_addr_u() const;
			system::ESamplerAddressMode get_addr_v() const;
			system::ESamplerAddressMode get_addr_w() const;

			void dispose();

		private:
			system::ESamplerFilterMode m_filter_mode;
			system::ESamplerAddressMode m_address_mode[3];
		};
	}
}

#endif // SAMPLER_HPP_INCLUDED
