// resource_manager.hpp

#ifndef RESOURCEMANAGER_HPP_INCLUDED
#define RESOURCEMANAGER_HPP_INCLUDED

#include <helium/resource/audio.hpp>
#include <helium/resource/config.hpp>
#include <helium/resource/font.hpp>
#include <helium/resource/material.hpp>
#include <helium/resource/model.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/vertex_format.hpp>

namespace helium
{
	namespace system
	{
		enum class ETextureFormat : uint32_t;
		enum class ETextureType : uint32_t;
	}

	namespace resource
	{
		enum class EResourceType : uint32_t
		{
			Audio,
			Config,
			Font,
			Material,
			Model,
			Sampler,
			Shader,
			Texture,
			VertexFormat,
			Invalid,
		};

		class ResourceManager
		{
			class AbstractResource
			{
			public:
				virtual ~AbstractResource() {}
				virtual void release() = 0;
			};

			template <class T>
			class Resource : public AbstractResource
			{
			public:
				Resource(std::unique_ptr<T> ptr) : m_resource(std::move(ptr)) { }
				T* get()  {  return m_resource.get(); }
				void release() { T* res = m_resource.release(); delete res; }

			private:
				std::unique_ptr<T> m_resource;
			};

		private: // non-copyable
			ResourceManager(const ResourceManager&);
			ResourceManager& operator=(const ResourceManager&);

		public:
			typedef std::unique_ptr<ResourceManager> Ptr;

			static Ptr create();

		private:
			ResourceManager();

		public:
			~ResourceManager();

			void set_directory(const std::string& directory);

			bool has_resource(const std::string& filename, const std::string& group = "default");
			void unload_resource(const std::string& filename, const std::string& group = "default");
			void unload_all_resources(const std::string& group = "default");

			template <class T>
			T* load_resource(const std::string& filename, const std::string& group = "default")
			{
				auto git = m_groups.find(group);
				if (git == m_groups.end())
				{
					m_groups.insert(std::pair<std::string, Group>(group, Group()));
					git = m_groups.find(group);
				}

				ResourceMap& resmap = git->second.m_resources;
				auto rit = resmap.find(filename);
				if (rit != resmap.end())
				{
					Debug::write(EDebugLevel::Info, 
						String::format("resourcemanager: accessing resource: %s [%s]", 
						filename.c_str(), group.c_str()));
					return static_cast<Resource<T>*>(rit->second)->get();
				}

				resmap.insert(std::pair<std::string, AbstractResource*>(filename, nullptr));
				rit = resmap.find(filename);
				rit->second = new Resource<T>(T::create(m_directory + filename));
				Debug::write(EDebugLevel::Info, String::format("resourcemanager: creating resource: %s [%s]", filename.c_str(), group.c_str()));
				return static_cast<Resource<T>*>(rit->second)->get();
			}

		private:
			std::string m_directory;

		private:
			typedef std::unordered_map<std::string, AbstractResource*> ResourceMap;
			struct Group
			{
				ResourceMap m_resources;
			};			
			typedef std::unordered_map<std::string, Group> GroupMap;

			GroupMap m_groups;			
		};
	}
}

#endif // RESOURCEMANAGER_HPP_INCLUDED
