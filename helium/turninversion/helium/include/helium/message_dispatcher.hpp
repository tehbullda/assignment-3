// message_dispatcher.hpp

#ifndef MESSAGEDISPATCHER_HPP_INCLUDED
#define MESSAGEDISPATCHER_HPP_INCLUDED

namespace helium
{
	class Message;

	class MessageDispatcher
	{
	public:
		typedef std::unique_ptr<MessageDispatcher> Ptr;

		static Ptr create();

	private:
		MessageDispatcher();

	public:
		~MessageDispatcher();

		void notify(Message* message);
	};
}

#endif // MESSAGEDISPATCHER_HPP_INCLUDED
