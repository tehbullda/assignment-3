// math.hpp

#ifndef MATH_HPP_INCLUDED
#define MATH_HPP_INCLUDED

namespace helium
{
	class Math
	{
	public:
		static const float PI;

		static float sqrt(float value);
		static float sin(float radian);
		static float cos(float radian);
		static float tan(float radian);
		static float to_rad(float degrees);
		static float to_deg(float radians);

		template <typename T>
		static T max(T a, T b)
		{
			return a > b ? a : b;
		}

		template <typename T>
		static T min(T a, T b)
		{
			return a < b ? a : b;
		}

		template <typename T>
		static T abs(T a)
		{
			return a < 0 ? -a : a;
		}

		template <typename T>
		static T clamp(T v, T a, T b)
		{
			return v < a ? a : v > b ? b : v;
		}

		template <typename T>
		static bool are_equal(const T& a, const T& b) { return a == b; }
		template <>
		static bool are_equal<float>(const float& a, const float& b);
		template <>
		static bool are_equal<double>(const double& a, const double& b);

	};

	class Vector2
	{
	public:
		Vector2();
		Vector2(float x, float y);
		Vector2(const Vector2& rhs);

		Vector2& operator=(const Vector2& rhs);
		Vector2& operator+=(const Vector2& rhs);
		Vector2& operator-=(const Vector2& rhs);
		Vector2& operator*=(const float real);
		Vector2& operator/=(const float real);
		Vector2 operator+(const Vector2&rhs);
		Vector2 operator-(const Vector2&rhs);
		Vector2 operator*(const float real);
		Vector2 operator/(const float real);

		float length() const;
		float length_squared() const;
		float dot(const Vector2& rhs) const;
		void normalize();

		float m_x;
		float m_y;
	};

	class Vector3
	{
	public:
		Vector3();
		Vector3(float x, float y, float z);
		Vector3(const Vector3& rhs);

		Vector3& operator=(const Vector3& rhs);
		Vector3& operator+=(const Vector3& rhs);
		Vector3& operator-=(const Vector3& rhs);
		Vector3& operator*=(const float real);
		Vector3& operator/=(const float real);
		bool Vector3::operator!=(const Vector3 &rhs);
		Vector3 operator+(const Vector3& rhs);
		const Vector3 operator+(const Vector3& rhs) const;
		Vector3 operator-(const Vector3& rhs);
		Vector3 operator*(const float real);
		Vector3 operator/(const float real);

		Vector3 cross(const Vector3& rhs);
		bool compare(const Vector3 &rhs);

		float length() const;
		float length_squared() const;
		float dot(const Vector3& rhs) const;
		void normalize();

		float m_x;
		float m_y;
		float m_z;
	};

	class Vector4
	{
	public:
		Vector4();
		Vector4(float x, float y, float z, float w);
		Vector4(const Vector4& rhs);
		Vector4& operator=(const Vector4& rhs);

		float m_x;
		float m_y;
		float m_z;
		float m_w;
	};

	class Matrix4
	{
	public:
		Matrix4();
		Matrix4(float _11, float _12, float _13, float _14,
			float _21, float _22, float _23, float _24,
			float _31, float _32, float _33, float _34,
			float _41, float _42, float _43, float _44);
		Matrix4(const Matrix4& rhs);

		Matrix4& operator=(const Matrix4& rhs);
		Matrix4 operator*(const Matrix4 &rhs);
		const Matrix4 operator*(const Matrix4 &rhs) const;

		Vector3 operator*(const Vector3& rhs);

		void identity();
		Matrix4 inverse();

		static Matrix4 scale(float x, float y, float z);
		static Matrix4 scale(const Vector3 &v);
		static Matrix4 rotation(const Vector3 &axis, float radians);
		static Matrix4 rotation(float x, float y, float z, float radians);
		static Matrix4 translation(float x, float y, float z);
		static Matrix4 translation(const Vector3 &v);
		static Matrix4 perspective(float fov, float aspect, float znear, float zfar);
		static Matrix4 orthogonal(float width, float height, float znear, float zfar);

		union
		{
			float _m[16];
			struct
			{
				float _11, _12, _13, _14;
				float _21, _22, _23, _24;
				float _31, _32, _33, _34;
				float _41, _42, _43, _44;
			} m;
		};
	};
	class Hitbox2D {
	public:
		Hitbox2D() {
			m_x = 0;
			m_y = 0;
			m_w = 0;
			m_h = 0;
		}
		Hitbox2D(float x, float y, float  w, float h) {
			m_x = x;
			m_y = y;
			m_w = w;
			m_h = h;
		}
		bool Contains(Vector2 pos) {
			if (pos.m_x > m_x && pos.m_x < m_x + m_w) {
				if (pos.m_y > m_y && pos.m_y < m_y + m_h) {
					return true;
				}
			}
			return false;
		}
	private:
		float m_x, m_y, m_w, m_h;
	};
	class Ray
	{
	public:
		Ray();
		Ray(const Ray& rhs);
		Ray(const Vector3& origin, const Vector3& direction);

		Ray& operator=(const Ray& rhs);

		Vector3 m_origin;
		Vector3 m_direction;
	};

	class Plane
	{
	public:
		Plane();
		Plane(const Plane& rhs);
		Plane(const Vector3& normal, const float D);
		Plane& operator=(const Plane& rhs);

		Vector3 m_normal;
		float m_D;
	};

	class BoundingSphere
	{
	public:
		BoundingSphere();
		BoundingSphere(const Vector3& center, float radius);
		BoundingSphere(const BoundingSphere& rhs);
		BoundingSphere& operator=(const BoundingSphere& rhs);

		Vector3 m_center;
		float m_radius;
	};

	class AxisAlignedBoundingBox
	{
	public:
		AxisAlignedBoundingBox();
		AxisAlignedBoundingBox(const Vector3& center, const Vector3& halfextent);
		AxisAlignedBoundingBox(const AxisAlignedBoundingBox& rhs);
		AxisAlignedBoundingBox& operator=(const AxisAlignedBoundingBox& rhs);

		Vector3 m_center, m_extent;
	};

	class Frustum
	{
	public:
		Frustum();
		Frustum(const Frustum& rhs);
		Frustum& operator=(const Frustum& rhs);

		void construct(Matrix4& viewproj);
		const bool is_inside(const Vector3& point) const;
		const bool is_inside(const BoundingSphere& sphere) const;
		const bool is_inside(const AxisAlignedBoundingBox& aabb) const;

		Plane m_planes[6];
	};
}

#endif // MATH_HPP_INCLUDED
