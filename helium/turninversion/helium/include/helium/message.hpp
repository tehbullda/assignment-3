// message.hpp

#ifndef MESSAGE_HPP_INCLUDED
#define MESSAGE_HPP_INCLUDED

namespace helium
{
	enum class EMsgType : uint32_t
	{
		Close,
		MouseMove,
		MouseButton,
		MouseWheel,
		Keyboard,
		Text,
		Resize,
		Focus,
		Invalid,
	};

	class Message
	{
	public:
		Message();
		explicit Message(EMsgType type);

		struct MouseMove
		{
			short x, y;
		};
		struct MouseButton
		{
			unsigned index;
			bool state;
		};
		struct MouseWheel
		{
			float delta;
		};
		struct Keyboard
		{
			unsigned keycode;
			bool state;
		};
		struct Text
		{
			unsigned unicode;
		};
		struct Resize
		{
			int width;
			int height;
		};
		struct Focus
		{
			bool state;
		};

		EMsgType m_type;
		union
		{
			union
			{
				MouseMove move;
				MouseButton button;
				MouseWheel wheel;
			} mouse;
			Keyboard keyboard;
			Text text;
			Resize resize;
			Focus focus;
		} m_data;
	};
}

#endif // EVENT_HPP_INCLUDED
