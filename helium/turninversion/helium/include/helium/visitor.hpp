// visitor.hpp

#ifndef VISITOR_HPP_INCLUDED
#define VISITOR_HPP_INCLUDED

namespace helium
{
	class Visitor
	{
		Visitor(const Visitor&);
		Visitor& operator=(const Visitor&);

	public:
		virtual ~Visitor();

		virtual void visit(float& value) = 0;
		virtual void visit(double& value) = 0;
		virtual void visit(int8_t& value) = 0;
		virtual void visit(uint8_t& value) = 0;
		virtual void visit(int16_t& value) = 0;
		virtual void visit(uint16_t& value) = 0;
		virtual void visit(int32_t& value) = 0;
		virtual void visit(uint32_t& value) = 0;
		virtual void visit(int64_t& value) = 0;
		virtual void visit(uint64_t& value) = 0;
		virtual void visit(const char* string) = 0;
	};
}

#endif // VISITOR_HPP_INCLUDED
