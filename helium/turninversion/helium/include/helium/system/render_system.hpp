// render_system.hpp

#ifndef RENDERSYSTEM_HPP_INCLUDED
#define RENDERSYSTEM_HPP_INCLUDED

namespace helium
{
	namespace system
	{
		enum class ETextureFormat : uint32_t
		{
			R8,
			R8G8,
			R8G8B8,
			R8G8B8A8,
			D24S8,
			D32,
			Invalid,
		};

		enum class ETextureType : uint32_t
		{
			Texture1D,
			Texture2D,
			Texture3D,
			Invalid,
		};

		enum class EBufferMode : uint32_t
		{
			Static,
			Dynamic,
			Invalid,
		};

		enum class EIndexBufferType : uint32_t
		{
			Int16,
			Int32,
			Invalid,
		};

		// Sampler state
		enum class ESamplerFilterMode : uint32_t
		{
			Nearest,
			Linear,
			Invalid,
		};

		enum class ESamplerAddressMode : uint32_t
		{
			Wrap,
			Clamp,
			Mirror,
			Invalid,
		};

		// Blend state
		enum class EBlendOp : uint32_t
		{
			Add,
			Subtract,
			ReverseSubtract,
			Invalid,
		};

		enum class EBlendMode : uint32_t
		{
			Zero,
			One,
			SrcColor,
			OneMinusSrcColor,
			DestColor,
			SrcAlpha,
			OneMinusSrcAlpha,
			DestAlpha,
			OneMinusDestAlpha,
			SrcAlphaSaturate,
			Invalid,
		};

		// Depth state
		enum class EDepthMode : uint32_t
		{
			Never,
			Less,
			Equal,
			LessEqual,
			Greater,
			NotEqual,
			GraterEqual,
			Always,
			Invalid,
		};

		// Rasterizer state
		enum class ECullMode : uint32_t
		{
			None,
			Back,
			Front,
			Invalid,
		};

		enum class EFillMode : uint32_t
		{
			Solid,
			Wireframe,
			Invalid,
		};

		// Draw modes
		enum class EDrawMode : uint32_t
		{
			Points,
			LineList,
			LineStrip,
			TriangleList,
			TriangleStrip,
			Invalid,
		};

		// Vertex format
		enum class EVertexAttributeFormat : uint32_t
		{
			Generic,
			Position,
			TexCoord,
			Normal,
			Tangent,
			Invalid,
		};

		enum class EVertexAttributeType : uint32_t
		{
			Float,
			Short,
			Byte, 
			Invalid,
		};

		struct VertexFormatDesc
		{
			uint32_t index;
			EVertexAttributeFormat format;
			EVertexAttributeType type;
			uint32_t size;
		};

		// Unnamed enums
		enum ERenderSystemSettings
		{
			INVALID_RESOURCE = -1,
			MAX_TEXTURE_UNITS = 8,
			MAX_FORMAT_DESC = 32,
		};

		class RenderSystem
		{
		private:
			RenderSystem(const RenderSystem&);
			RenderSystem& operator=(const RenderSystem&);

		public:
			typedef std::unique_ptr<RenderSystem> Ptr;

			static Ptr create(HWND window_handle, int width, int height);

		private:
			RenderSystem(HWND window_handle, int width, int height);

		public:
			~RenderSystem();

			void clear();
			void present();

			HWND get_window();
			bool get_fullscreen();
			void update_vertex_buffer(const uint32_t &p_vertex_buffer_id, const void* data, const uint32_t &bytesize);
			void update_index_buffer(const uint32_t &p_index_buffer_id, const void* data, const uint32_t &data_size);

			void select_shader(uint32_t shader_id);
			void select_texture(uint32_t texture_id, uint32_t unit = 0);
			void select_vertex_format(uint32_t vertex_format_id);
			void select_vertex_buffer(uint32_t vertex_buffer_id);
			void select_index_buffer(uint32_t index_buffer_id);
			void select_sampler_state(uint32_t sampler_state_id, uint32_t unit = 0);
			void select_blend_state(uint32_t blend_state_id);
			void select_depth_state(uint32_t depth_state_id);
			void select_rasterizer_state(uint32_t rasterizer_state_id);

			void set_shader_constant_float(const char* name, const float value);
			void set_shader_constant_vector2(const char* name, const Vector2& value);
			void set_shader_constant_vector3(const char* name, const Vector3& value);
			void set_shader_constant_vector4(const char* name, const Vector4& value);
			void set_shader_constant_matrix4(const char* name, const Matrix4& value);
			void set_shader_constant_raw(const char* name, const void* value, uint32_t size);
			void set_shader_constant_raw(const uint32_t hash, const void* value, uint32_t size);

			void apply();

			void draw(EDrawMode mode, uint32_t start, uint32_t count);
			void drawi(EDrawMode mode, uint32_t start, uint32_t count);

			uint32_t  create_shader(const char* vertex_src, const char* pixel_src);
			void destroy_shader(uint32_t shader_id);
			uint32_t  create_texture(ETextureFormat format, ETextureType type,
				uint32_t width, uint32_t height, uint32_t depth, const void* data);
			void destroy_texture(uint32_t texture_id);
			uint32_t  create_vertex_format(VertexFormatDesc* vertex_format_desc, uint32_t size, uint32_t shader_id);
			void destroy_vertex_format(uint32_t vertex_format_id);
			uint32_t  create_vertex_buffer(EBufferMode mode, const void* data, uint32_t size, uint32_t count);
			void destroy_vertex_buffer(uint32_t vertex_buffer_id);
			uint32_t  create_index_buffer(const EIndexBufferType type, uint32_t count, const void* data, const EBufferMode mode = EBufferMode::Static);
			void destroy_index_buffer(uint32_t index_buffer_id);
			uint32_t  create_sampler_state(ESamplerFilterMode filter, 
				ESamplerAddressMode addr_u, ESamplerAddressMode addr_v, ESamplerAddressMode addr_w);
			void destroy_sampler_state(uint32_t sampler_state_id);
			uint32_t  create_blend_state(EBlendMode src_color, EBlendMode dest_color,
				EBlendMode src_alpha, EBlendMode dest_alpha,
				EBlendOp src_op, EBlendOp dest_op, uint32_t mask = 0xF, bool atoc = false); 
			void destroy_blend_state(uint32_t blend_state_id);
			uint32_t  create_depth_state(bool write, bool read, EDepthMode mode);
			void destroy_depth_state(uint32_t depth_state_id);
			uint32_t  create_rasterizer_state(EFillMode fill_mode, ECullMode cull_mode);
			void destroy_rasterizer_state(uint32_t rasterizer_state_id);

		private:
			void apply_shader();
			void apply_texture_and_sampler();
			void apply_vertex_format();
			void apply_vertex_buffer();
			void apply_index_buffer();
			void apply_blend_state();
			void apply_depth_state();
			void apply_rasterizer_state();
			void reset_state();

		private:
			HWND m_window;
		private:
			uint32_t m_selected_shader, m_current_shader;
			uint32_t m_selected_vertex_format, m_current_vertex_format;
			uint32_t m_selected_vertex_buffer, m_current_vertex_buffer;
			uint32_t m_selected_index_buffer, m_current_index_buffer;
			uint32_t m_selected_blend_state, m_current_blend_state;
			uint32_t m_selected_depth_state, m_current_depth_state;
			uint32_t m_selected_rasterizer_state, m_current_rasterizer_state;
			uint32_t m_selected_texture[MAX_TEXTURE_UNITS], m_current_texture[MAX_TEXTURE_UNITS];
			uint32_t m_selected_sampler_state[MAX_TEXTURE_UNITS], m_current_sampler_state[MAX_TEXTURE_UNITS];
			EDrawMode m_current_topology;

		private:
			struct IDXGISwapChain* m_swap_chain;
			struct ID3D11Device* m_device;
			struct ID3D11DeviceContext* m_context;
			struct ID3D11RenderTargetView* m_rtv;
			struct ID3D11Texture2D* m_depth_buffer;
			struct ID3D11DepthStencilView* m_dsv;

		private:
			struct Shader;
			struct Texture;
			struct VertexFormat;
			struct VertexBuffer;
			struct IndexBuffer;
			struct SamplerState;
			struct BlendState;
			struct DepthState;
			struct RasterizerState;

			std::vector<Shader> m_shaders;
			std::vector<Texture> m_textures;
			std::vector<VertexFormat> m_vertex_formats;
			std::vector<VertexBuffer> m_vertex_buffers;
			std::vector<IndexBuffer> m_index_buffers;
			std::vector<SamplerState> m_sampler_states;
			std::vector<BlendState> m_blend_states;
			std::vector<DepthState> m_depth_states;
			std::vector<RasterizerState> m_rasterizer_state;
		};
	}
}

#endif // RENDERSYSTEM_HPP_INCLUDED
