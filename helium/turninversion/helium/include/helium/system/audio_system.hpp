// audio_system.hpp

#ifndef AUDIOSYSTEM_HPP_INCLUDED
#define AUDIOSYSTEM_HPP_INCLUDED

namespace FMOD
{
	class System;
	class ChannelGroup;
}

namespace helium
{
	namespace system
	{
		enum class EAudioType : uint32_t
		{
			Buffer,
			Stream,
			Invalid,
		};

		enum ChannelType {
			MUSIC,
			SOUND,
			MASTER
		};

		class AudioEmitter
		{
		private:
			friend class AudioSystem;
			AudioEmitter(uint32_t id);

		private:
			uint32_t m_id;
			Vector3 m_position;
			Vector3 m_velocity;
		};

		class AudioListener
		{
		private:
			friend class AudioSystem;
			AudioListener(uint32_t id);

		private:
			uint32_t m_id;
			Vector3 m_position;
			Vector3 m_velocity;
		};

		class AudioSystem
		{
			struct Sound;

		private: // non-copyable
			AudioSystem(const AudioSystem&);
			AudioSystem& operator=(const AudioSystem&);

		public:
			typedef std::unique_ptr<AudioSystem> Ptr;

			static Ptr create();

		private:
			AudioSystem();

		public:
			~AudioSystem();

			void process();

			void add_sound(const std::string &name, ChannelType type, EAudioType audiotype);
			void play(const std::string &name, bool looping = false);
			void setpause(bool paused);
			void setmute(bool mute);
			void stop();

		private:
			int  add_sound(const std::string identifier, Sound& sound);

		private:
			FMOD::System* m_system;
			FMOD::ChannelGroup* m_master;
			FMOD::ChannelGroup* m_music;
			FMOD::ChannelGroup* m_effect;

		private:
			std::map<std::string, Sound*> m_sounds;
			bool m_muted;
		};
	}
}

#endif // AUDIOSYSTEM_HPP_INCLUDED
