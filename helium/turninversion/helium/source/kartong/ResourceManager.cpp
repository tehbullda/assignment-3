#include <stdafx.h>
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>

using namespace kartong;

ResourceManager::ResourceManager()
{
	m_render_system = ServiceLocator<helium::system::RenderSystem>::get_service();
	CreateDefaults();
};

ResourceManager::~ResourceManager()
{

};

uint32_t ResourceManager::LoadShader(const std::string& p_Shader)
{
	auto it = m_Shaders.begin();
	while (it != m_Shaders.end())
	{
		if (p_Shader == it->first)
			return it->second;
	}
	Shader::Ptr tmpshader = Shader::create(p_Shader);
	uint32_t shader = m_render_system->create_shader(tmpshader->get_vertex_source().c_str(), tmpshader->get_pixel_source().c_str());
	m_Shaders.insert(std::pair<std::string, uint32_t>(p_Shader, shader));
	return shader;
};
uint32_t ResourceManager::LoadFormat(const std::string& p_Format, const uint32_t shaderid) {
	auto it = m_Formats.begin();
	while (it != m_Formats.end())
	{
		if (p_Format == it->first)
			return it->second;
	}
	VertexFormat::Ptr tmpformat = VertexFormat::create(p_Format);
	uint32_t format = m_render_system->create_vertex_format(const_cast<helium::system::VertexFormatDesc*>(tmpformat->get_desc()), tmpformat->get_count(), shaderid);
	m_Formats.insert(std::make_pair(p_Format, format));
	return format;
}

uint32_t ResourceManager::LoadSampler(const std::string& p_Sampler) {
	auto it = m_Samplers.begin();
	while (it != m_Samplers.end())
	{
		if (p_Sampler == it->first)
			return it->second;
	}
	Sampler::Ptr tmpsampler = Sampler::create(p_Sampler);
	uint32_t sampler = m_render_system->create_sampler_state(tmpsampler->get_filter_mode(), tmpsampler->get_addr_u(), tmpsampler->get_addr_v(), tmpsampler->get_addr_w());
	m_Samplers.insert(std::make_pair(p_Sampler, sampler));
	return sampler;
}

uint32_t ResourceManager::LoadTexture(const std::string& p_Texture) {
	auto it = m_Textures.begin();
	while (it != m_Textures.end())
	{
		if (p_Texture == it->first)
			return it->second;
	}
	Texture::Ptr tmptexture = helium::resource::Texture::create(p_Texture);
	uint32_t texture = m_render_system->create_texture(
		tmptexture->get_format(),
		tmptexture->get_type(),
		tmptexture->get_width(),
		tmptexture->get_height(),
		tmptexture->get_depth(),
		tmptexture->get_pixel_data());
	m_Textures.insert(std::make_pair(p_Texture, texture));
	return texture;
}

uint32_t ResourceManager::LoadDepthDefault() {
	return m_Depthstates.at("Default");
}

uint32_t ResourceManager::LoadDepth(const std::string& p_Depth, bool write, bool read, system::EDepthMode mode) {
	auto it = m_Depthstates.begin();
	while (it != m_Depthstates.end())
	{
		if (p_Depth == it->first)
			return it->second;
	}
	uint32_t depth = m_render_system->create_depth_state(write, read, mode);
	m_Textures.insert(std::make_pair(p_Depth, depth));
	return depth;
}

uint32_t ResourceManager::LoadRasterDefault() {
	return m_Rasterizers.at("Default");
}

uint32_t ResourceManager::LoadRaster(const std::string& p_Raster, system::EFillMode fillmode, system::ECullMode cullmode) {
	auto it = m_Rasterizers.begin();
	while (it != m_Rasterizers.end())
	{
		if (p_Raster == it->first)
			return it->second;
	}
	uint32_t raster = m_render_system->create_rasterizer_state(fillmode, cullmode);
	m_Textures.insert(std::make_pair(p_Raster, raster));
	return raster;
}

uint32_t ResourceManager::LoadBlendDefault() {
	return m_Blendstates.at("Default");
}

uint32_t ResourceManager::LoadBlend(const std::string& p_Blend, system::EBlendMode src_color, system::EBlendMode dst_color,
	system::EBlendMode src_alpha, system::EBlendMode dest_alpha, system::EBlendOp src_op, system::EBlendOp dest_op, uint32_t mask, bool atoc) {
	auto it = m_Blendstates.begin();
	while (it != m_Blendstates.end())
	{
		if (p_Blend == it->first)
			return it->second;
	}
	uint32_t blend = m_render_system->create_blend_state(src_color, dst_color, src_alpha, dest_alpha, src_op, dest_op, mask, atoc);
	m_Textures.insert(std::make_pair(p_Blend, blend));
	return blend;
}

void ResourceManager::CreateDefaults() {
	m_Depthstates.insert(std::make_pair("Default", m_render_system->create_depth_state(true, true, helium::system::EDepthMode::Less)));
	m_Rasterizers.insert(std::make_pair("Default", m_render_system->create_rasterizer_state(system::EFillMode::Solid, system::ECullMode::Back)));
	m_Blendstates.insert(std::make_pair("Default", m_render_system->create_blend_state(system::EBlendMode::One, system::EBlendMode::One, 
										system::EBlendMode::One, system::EBlendMode::One, system::EBlendOp::Add, system::EBlendOp::Add)));
}