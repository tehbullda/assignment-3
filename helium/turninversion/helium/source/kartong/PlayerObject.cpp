#include "stdafx.h"

#include <helium/scene/camera.hpp>
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <kartong/CollisionManager.hpp>
#include <kartong/Weapon.hpp>
#include <iostream>
#include <kartong/Randomizer.hpp>

#include <versionhelpers.h>

#define ProjectionZFar 150.0f

namespace kartong
{
	using namespace helium;
	PlayerObject::PlayerObject(input::Input* p_Input, const int& p_ID)
		//: m_Collided(this)
	{
		m_Rendersystem = helium::ServiceLocator<helium::system::RenderSystem>::get_service();
		m_CollisionManager = helium::ServiceLocator<kartong::CollisionManager>::get_service();
		m_Input = p_Input;
		m_Mouse = Mouse::create();
		ResetStats();

		//m_Position = Vector3(0.0f, 50.0f, 0.0f);
		m_Grounded = false;
		m_Zoomed = false;

		SetTag(("Player" + std::to_string(p_ID)));
		SetName("Laboon");
		SetType(ObjectType::PLAYER);
		SetPhysicsType(PhysicsType::KINETIC);
		m_PlayerCamera = new scene::Camera();
		m_PlayerCamera->set_position('-', m_Position);
		m_PlayerCamera->set_perspective(Math::to_rad(45.0f), kartong::global::ResW / kartong::global::ResH, 0.5f, ProjectionZFar);
		
		
		m_PlayerCamera->update();



		m_standing = true;

		m_Dirty = true;
		m_pitch = 0.0f;
		m_yaw = 0.0f;
		m_roll = 0.0f;
		m_view.identity();
		m_projection.identity();

		m_Sense = 10.0f;
		m_Mouseyaw = 0.022f;
		m_Mousepitch = 0.022f;

		m_Bounds = m_CollisionManager->CreateNewBox(this, m_Position, Vector3(2.5f, 2.0f, 2.5f), 100.0f);
		m_Bounds->SetPosition(m_Position);
		m_Collided = m_CollisionManager->CreateNewBox(this, Vector3(0, 0, 0), Vector3(0, 0, 0), 0.0f);


		m_TestWeapon = new Weapon();
		m_TestWeapon->Initialize();
		m_TestWeapon->SetWeapon(weapons::SNIPER);
		m_TestWeapon->SetRotation(Vector3(helium::Math::to_rad(90), 0.0f, 0.0f));

		m_AmmoHUD = HUDAmmo::Create();
		m_AmmoHUD->SetPosition(Vector2(global::ResW - 60.0f, 60.0f));

		::ShowCursor(false);

	};
	PlayerObject::~PlayerObject()
	{
		//if (m_PlayerCamera) {
		//	delete m_PlayerCamera;
		//	m_PlayerCamera = nullptr;
		//}
		if (m_ZoomCamera != nullptr) {
			delete m_ZoomCamera;
			m_ZoomCamera = nullptr;
		}

		if (m_TestWeapon != nullptr)
		{
			delete m_TestWeapon;
			m_TestWeapon = nullptr;
		}
		//if (m_Collided != nullptr)
		//{
		//	delete m_Collided;
		//	m_Collided = nullptr;
		//}

	};
	void PlayerObject::Initialize()
	{
		m_AmmoHUD->Initialize();
	};
	void PlayerObject::Update(const float& p_Deltatime)
	{
		
		//m_Bounds->SetPosition(Vector3(m_Position.m_x, m_Position.m_y, m_Position.m_z));


		/*	m_Bounds->SetPosition(m_Position)*/;
		m_TestWeapon->Update(p_Deltatime);
		m_AmmoHUD->Update(m_TestWeapon->GetCurrentammo(), m_TestWeapon->GetMaxAmmo());




		if (m_Input->GetActionOnce(input::actions::SIGHT))
		{
			m_Zoomed = !m_Zoomed;
		}

		if (!m_Zoomed)
		{
			m_PlayerCamera->set_perspective(Math::to_rad(45.0f), kartong::global::ResW / kartong::global::ResH, 0.5f, ProjectionZFar);
			m_Dirty = true;
		}
		else
		{
			
			m_PlayerCamera->set_perspective(Math::to_rad(15.0f), kartong::global::ResW / kartong::global::ResH, 0.5f, ProjectionZFar);
			m_Dirty = true;
		}

		UpdatePhysics();
		UpdateCollision();
		UpdateDirection(p_Deltatime);
		UpdateMovement(p_Deltatime);
		UpdateWeapon();
		m_Position = Vector3(m_Bounds->m_Position.m_x, m_Bounds->m_Position.m_y + 1.0f, m_Bounds->m_Position.m_z);
		m_Bounds->SetRotationData(m_PlayerCamera->get_view(), m_yaw, m_pitch);
		obj.BS.m_center = m_Position;

		obj.BS.m_radius = m_Bounds->GetHalfVolume().m_x;
	};

	void PlayerObject::PostUpdate() {
		m_prevPos = m_Position;
	};

	void PlayerObject::Draw(scene::Camera* p_Camera)
	{
		p_Camera;
		m_TestWeapon->Draw(m_PlayerCamera, m_Zoomed);
		m_AmmoHUD->Draw();

	};

	void PlayerObject::UpdateMovement(const float& p_Deltatime)
	{

		if (m_Input->GetAction(input::actions::UP))
			MoveForward(1.0f * (m_Current.movespeed * p_Deltatime));
		if (m_Input->GetAction(input::actions::DOWN))
			MoveForward(-1.0f * (m_Current.movespeed * p_Deltatime));
		if (m_Input->GetAction(input::actions::LEFT))
			MoveSidestep(-1.0f * (m_Current.movespeed * p_Deltatime));
		if (m_Input->GetAction(input::actions::RIGHT))
			MoveSidestep(1.0f * (m_Current.movespeed * p_Deltatime));


		if (GetAsyncKeyState(VK_UP))
			MoveForward(0.1f * (m_Current.movespeed * p_Deltatime));
		if (GetAsyncKeyState(VK_DOWN))
			MoveForward(-0.1f * (m_Current.movespeed * p_Deltatime));
		if (GetAsyncKeyState(VK_LEFT))
			MoveSidestep(-0.1f * (m_Current.movespeed * p_Deltatime));
		if (GetAsyncKeyState(VK_RIGHT))
			MoveSidestep(0.1f * (m_Current.movespeed * p_Deltatime));

		//Testing
		if (GetAsyncKeyState('O'))
		{
			MoveElevate(1.0f * (m_Current.movespeed * p_Deltatime));
			m_Grounded = false;
		}
		if (GetAsyncKeyState('P'))
			MoveElevate(-1.0f * (m_Current.movespeed * p_Deltatime));



		if (m_Input->GetActionOnce(input::actions::CROUCH)) {
			ToggleCrouch();
		}

		if (m_Input->GetAction(input::actions::UP) || m_Input->GetAction(input::actions::DOWN)
			|| m_Input->GetAction(input::actions::LEFT) || m_Input->GetAction(input::actions::RIGHT))
			return;

		MoveForward(m_Input->GetAxis(input::axes::LY) * (m_Current.movespeed * p_Deltatime));
		MoveSidestep(m_Input->GetAxis(input::axes::LX) * (m_Current.movespeed * p_Deltatime));


	};
	void PlayerObject::UpdateDirection(const float& p_Deltatime)
	{
		m_DeltaTime = p_Deltatime;
		//Debug::write(EDebugLevel::Info, std::to_string(m_DeltaTime / 60.0f));

		GetWindowRect(m_Rendersystem->get_window(), &m_WindowRect);
		m_MousePos.m_x = m_Mouse->get_x();
		m_MousePos.m_y = m_Mouse->get_y();
		//if (m_Mouse->is_button_down(EMouseButton::Left))
		//{
		int x, y;

		//if (!m_Rendersystem->get_fullscreen()) {
		//	if (IsWindows8OrGreater()) {
		//		x = m_MousePos.m_x - kartong::global::ResW / 2 + 8;
		//		y = m_MousePos.m_y - kartong::global::ResH / 2 + 31;
		//	}
		//	else {
		//		x = m_MousePos.m_x - kartong::global::ResW / 2 + 8;
		//		y = m_MousePos.m_y - kartong::global::ResH / 2 + 30;
		//	}
		//}
		//else {
			x = m_MousePos.m_x - kartong::global::ResW / 2;
			y = m_MousePos.m_y - kartong::global::ResH / 2;
		//}
		//	std::cout << "X: " << x << " Y: " << y << std::endl;
		if (x != 0 || y != 0)
		{
			//RotateY(-1.0f * (5.0f * p_Deltatime));
			RotateY((float)x * m_DeltaTime * m_Sense * m_Mouseyaw);
			RotateX((float)y * m_DeltaTime * m_Sense * m_Mousepitch);

		}
		//}

		m_PrevMousePos = m_MousePos;
		SetCursorPos((kartong::global::ResW / 2) + m_WindowRect.left, (kartong::global::ResH / 2) + m_WindowRect.top);
		if (x != 0 || y != 0)
			return;


		RotateX(-m_Input->GetAxis(input::axes::RY) * (5.0f * m_DeltaTime));
		RotateY(m_Input->GetAxis(input::axes::RX) * (5.0f * m_DeltaTime));

	};

	void PlayerObject::UpdatePhysics()
	{
		//if (!m_Grounded)
		//{
		//m_Bounds->m_Position = Physics_system::Apply_gravity(m_Position, m_DeltaTime);
		m_Dirty = true;
		//}
	};
	void PlayerObject::UpdateWeapon()
	{
		m_TestWeapon->SetPosition(Vector3(m_Position.m_x, m_Position.m_y - 2.0f, m_Position.m_z));
		m_TestWeapon->SetScale(Vector3(0.3f, 0.0f, 0.0f));
		m_TestWeapon->SetRotation((Vector3(helium::Math::to_rad(90.0f), -m_yaw, -m_pitch)));

		if (m_Input->GetActionOnce(input::actions::RELOAD))
			m_TestWeapon->Reload();
		if (m_Input->GetAction(input::actions::SHOOT))
			m_TestWeapon->Shoot();

		if (m_TestWeapon->ShotFired())
		{
			m_CollisionManager->CheckCollision(m_Position, m_forward);
			m_pitch += (float)Randomizer::GetRandomDouble(-0.02f, -0.06f);
			m_yaw += (float)Randomizer::GetRandomDouble(-0.02f, 0.04f);
		}
	};
	void PlayerObject::UpdateCollision()
	{
		m_CollisionManager->CheckCollision(m_Bounds);

		while (m_CollisionManager->CheckHasCollided(m_Bounds, &m_Collided))
		{
			if (m_Collided->m_Owner->GetType() == kartong::ObjectType::ENVIRONMENT)
			{
				if (m_Collided->m_Owner->GetPhysicstype() == kartong::PhysicsType::STATIC)
				{
					if (m_Bounds->m_Position.m_x > m_Collided->GetPosition().m_x + m_Collided->GetHalfVolume().m_x)
						m_Bounds->m_Position.m_x = m_Collided->GetPosition().m_x + m_Collided->GetHalfVolume().m_x + m_Bounds->GetHalfVolume().m_x;
					if (m_Bounds->m_Position.m_x < m_Collided->GetPosition().m_x - m_Collided->GetHalfVolume().m_x)
						m_Bounds->m_Position.m_x = m_Collided->GetPosition().m_x - m_Collided->GetHalfVolume().m_x - m_Bounds->GetHalfVolume().m_x;

					if (m_Bounds->m_Position.m_y > m_Collided->GetPosition().m_y + m_Collided->GetHalfVolume().m_y - 0.5f)
						m_Bounds->m_Position.m_y = m_Collided->GetPosition().m_y + m_Collided->GetHalfVolume().m_y + m_Bounds->GetHalfVolume().m_y;
					if (m_Bounds->m_Position.m_y < m_Collided->GetPosition().m_y - m_Collided->GetHalfVolume().m_y)
						m_Bounds->m_Position.m_y = m_Collided->GetPosition().m_y - m_Collided->GetHalfVolume().m_y - m_Bounds->GetHalfVolume().m_y;

					if (m_Bounds->m_Position.m_z > m_Collided->GetPosition().m_z + m_Collided->GetHalfVolume().m_z)
						m_Bounds->m_Position.m_z = m_Collided->GetPosition().m_z + m_Collided->GetHalfVolume().m_z + m_Bounds->GetHalfVolume().m_z;
					if (m_Bounds->m_Position.m_z < m_Collided->GetPosition().m_z - m_Collided->GetHalfVolume().m_z)
						m_Bounds->m_Position.m_z = m_Collided->GetPosition().m_z - m_Collided->GetHalfVolume().m_z - m_Bounds->GetHalfVolume().m_z;

					m_Grounded = true;
					m_Dirty = true;

				}

				//m_Position.m_y = m_Collided->GetPosition().m_y + m_Collided->GetHalfVolume().m_y + m_Bounds->GetVolume().m_y + 1;
			}
			if (m_Collided->m_Owner->GetType() == kartong::ObjectType::INTERACTIVE)
			{
				if (m_Collided->m_Owner->GetPhysicstype() == kartong::PhysicsType::PASS)
				{
					if (m_Collided->m_Owner->GetTag() == "Pickup")
					{
						if (m_Collided->m_Owner->GetName() == "HealthPack")
						{
							//if (m_Collided->m_Owner->RequirementMet())
							{
								m_Collided->m_Owner->Interact();
								Debug::write(EDebugLevel::Info, "Healed!");
							}
						}
					}
				}
			}
		}
	};


	void PlayerObject::MovePlayerX(const float& p_Val)
	{
		m_Bounds->m_Position.m_x += p_Val;
	};
	void PlayerObject::MovePlayerY(const float& p_Val)
	{
		m_Bounds->m_Position.m_y += p_Val;
	};
	void PlayerObject::MovePlayerZ(const float& p_Val)
	{
		m_Bounds->m_Position.m_z += p_Val;
	};


	void PlayerObject::ToggleCrouch()
	{
		m_standing = !m_standing;
		m_standing ? m_PlayerCamera->move_elevate(5) : m_PlayerCamera->move_elevate(-5);
	};

	scene::Camera* PlayerObject::GetPlayerCamera()
	{
		return m_PlayerCamera;
	};

	bool PlayerObject::PlayerMoved() {
		return !m_Position.compare(m_prevPos);
	};

	bool PlayerObject::PlayerShot() {
		return m_Input->GetActionOnce(input::actions::SHOOT);
	}
	Vector3 PlayerObject::GetDirection() {
		return m_Position - m_prevPos;
	}

	void PlayerObject::ResetStats()
	{
		//Need to read from file;
		m_Max.hp = 100.;
		m_Max.movespeed = 15.0f;
		m_Max.jumpheight = 100.0f;
		m_Max.jumpforce = 100.0f;

		m_Current.hp = 100;
		m_Current.movespeed = 15.0f;
		m_Current.jumpheight = 100.0f;
		m_Current.jumpforce = 100.0f;
	};
	void PlayerObject::UpdateView()
	{
		if (!m_Dirty)
			return;
		m_Dirty = false;

		if (m_pitch < -1.3f)
			m_pitch = -1.3f;
		if (m_pitch > 1.3f)
			m_pitch = 1.3f;

		Vector3 right(1.0f, 0.0f, 0.0f);
		Vector3 up(0.0f, 1.0f, 0.0f);
		Vector3 forward(0.0f, 0.0f, 1.0f);

		Matrix4 ry = Matrix4::rotation(up, m_yaw);
		right = ry * right;
		forward = ry * forward;

		//Use theese for movement to make it "2d"
		m_RightMove = right;
		m_ForwardMove = forward;
		m_UpMove = up;

		Matrix4 rx = Matrix4::rotation(right, m_pitch);
		up = rx * up;
		forward = rx * forward;

		right.normalize();
		forward.normalize();
		up.normalize();


		m_ForwardMove.normalize();
		m_RightMove.normalize();

		m_right = right;
		m_up = up;
		m_forward = forward;

		m_view.m._11 = m_right.m_x; m_view.m._12 = m_up.m_x; m_view.m._13 = m_forward.m_x; m_view.m._14 = 0.0f;
		m_view.m._21 = m_right.m_y; m_view.m._22 = m_up.m_y; m_view.m._23 = m_forward.m_y; m_view.m._24 = 0.0f;
		m_view.m._31 = m_right.m_z; m_view.m._32 = m_up.m_z; m_view.m._33 = m_forward.m_z; m_view.m._34 = 0.0f;
		m_view.m._41 = -m_Position.dot(m_right);
		m_view.m._42 = -m_Position.dot(m_up);
		m_view.m._43 = -m_Position.dot(m_forward);
		m_view.m._44 = 1.0f;

		m_PlayerCamera->set_position('-', m_Position);
		m_PlayerCamera->set_view(m_view, m_projection);
	};
	float PlayerObject::GetYaw()
	{
		return m_yaw;
	};
	float PlayerObject::GetPitch()
	{
		return m_pitch;
	};


	void PlayerObject::MoveForward(float amount)
	{
		m_Dirty = true;
		//m_forward = Vector3(0.0f, 0.0f, 1.0f);
		m_Bounds->m_Position += m_ForwardMove * amount;
	};
	void PlayerObject::MoveSidestep(float amount)
	{
		m_Dirty = true;
		//m_right = Vector3(1.0f, 0.0f, 0.0f);
		m_Bounds->m_Position += m_RightMove * amount;
	};
	void PlayerObject::MoveElevate(float amount)
	{
		m_Dirty = true;
		m_Bounds->m_Position += m_UpMove * amount;
	};
	void PlayerObject::RotateX(float amount)
	{
		m_Dirty = true;
		m_pitch += amount;
	};
	void PlayerObject::RotateY(float amount)
	{
		m_Dirty = true;
		m_yaw += amount;
	};
	void PlayerObject::RotateZ(float amount)
	{
		m_Dirty = true;
		m_roll += amount;
	};
	void PlayerObject::SetPosition(Vector3 p_Pos)
	{
		m_Bounds->SetPosition(p_Pos);
	};
};

