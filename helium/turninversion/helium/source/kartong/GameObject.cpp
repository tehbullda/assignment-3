#include "stdafx.h"

using namespace kartong;


GameObject::GameObject()
{
	m_Name = "NONE";
	m_Tag = "NONE";
	m_Type = ENVIRONMENT;
	m_Physicstype = PASS;
};
GameObject::~GameObject()
{

};
void GameObject::SetTag(const std::string p_Tag)
{
	m_Tag = p_Tag;
};
void GameObject::SetType(const int p_Type)
{
	m_Type = p_Type;
};

void GameObject::SetPhysicsType(const int &type) {
	m_Physicstype = type;
}
void GameObject::SetName(const std::string p_Name)
{
	m_Name = p_Name;
};


std::string GameObject::GetTag()
{
	return m_Tag;
};
int GameObject::GetType()
{
	return m_Type;
};

int GameObject::GetPhysicstype() {
	return m_Physicstype;
}
std::string GameObject::GetName()
{
	return m_Name;
};

void GameObject::SetPosition(Vector3 p_Pos)
{
	m_Position = p_Pos;
};
Vector3 GameObject::GetPosition()
{
	return m_Position;
};
BoundingSphere GameObject::getBoundingSphere()
{
	return obj.BS;
};

//bool GameObject::RequirementMet()
//{
//	return true;
//
//};