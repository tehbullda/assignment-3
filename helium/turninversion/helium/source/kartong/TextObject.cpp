#include "stdafx.h"
#include <kartong/TextObject.hpp>
#include <helium/resource/font.hpp>
#include <helium/service_locator.hpp>

using namespace kartong;
using namespace helium;

#define SpaceSizeInPixels 5


TextObject::TextObject(const std::string &desc, const std::string &texture, Vector3 pos)
				: GUITexture(texture, Vector2(pos.m_x, pos.m_y), true) {
	m_render_system = ServiceLocator<helium::system::RenderSystem>::get_service();
	m_text.m_font = resource::Font::create(desc);
	m_dirty = false;
}

TextObject::~TextObject() {
}

void TextObject::Update() {
	if (m_dirty) {
		UpdateBuffers();
	}
}
//Credit to @Enfisk for this method minus the space-handling
void TextObject::UpdateBuffers() {
	if (m_text.m_font) {
		float curX = 0;

		std::vector<Vertex> vertices;
		std::vector<short> indices;

		for (unsigned i = 0; i < m_text.m_string.size(); ++i)
		{
			helium::resource::Glyph glyph = m_text.m_font->get_glyph(m_text.m_string[i]);
			if (m_text.m_string[i] == ' ') {
				curX += SpaceSizeInPixels;
				continue;
			}
			vertices.push_back({ Vector3(curX, 0, 0), Vector2(glyph.texcoord.m_x, glyph.texcoord.m_y) });
			vertices.push_back({ Vector3(curX + glyph.width, 0, 0), Vector2(glyph.texcoord.m_x + glyph.texcoord.m_z, glyph.texcoord.m_y) });
			vertices.push_back({ Vector3(curX, glyph.height, 0), Vector2(glyph.texcoord.m_x, glyph.texcoord.m_y + glyph.texcoord.m_w) });
			vertices.push_back({ Vector3(curX + glyph.width, glyph.height, 0), Vector2(glyph.texcoord.m_x + glyph.texcoord.m_z, glyph.texcoord.m_y + glyph.texcoord.m_w) });

			curX += glyph.width;
		}


		for (unsigned int i = 0; i < m_text.m_string.size() * 4; i += 4)
		{
			//Top Right half
			indices.push_back(i + 1);
			indices.push_back(i + 3);
			indices.push_back(i);

			//Bottom-Left half
			indices.push_back(i + 3);
			indices.push_back(i + 2);
			indices.push_back(i);
		}

		m_render_system->update_vertex_buffer(element.buffer, &vertices[0], sizeof(Vertex)* static_cast<unsigned int>(vertices.size()));
		m_render_system->update_index_buffer(element.index, &indices[0], sizeof(short)* static_cast<unsigned int>(indices.size()));

		element.drawcount = static_cast<uint32_t>(indices.size());
		m_dirty = false;
	}
}

void TextObject::Draw() {
	GUITexture::Draw();
}

void TextObject::SetText(const std::string &text) {
	m_text.m_string = text;
	m_dirty = true;
}

void TextObject::SetPosition(Vector3 pos) {
	GUITexture::SetPos(Vector2(pos.m_x, pos.m_y));
	m_dirty = true;
}

