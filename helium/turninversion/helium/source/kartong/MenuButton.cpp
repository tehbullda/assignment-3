//MenuButton.cpp

#include <stdafx.h>
#include <kartong/MenuButton.hpp>
#include <kartong/GUITexture.hpp>
#include <helium/Mouse.hpp>
namespace kartong {
	MenuButton::MenuButton(const std::string identifier, const std::string &texture, const std::string &hovertexture, const helium::Vector2 &pos) {
		m_texture = GUITexture::Create(texture, pos);
		m_hovertexture = GUITexture::Create(hovertexture, pos);
		m_texture->SetPos(pos, 0.1f); // Otherwise the two textures are in the same place and only one of them is rendered.
		m_hitbox = m_texture->GetHitbox();
		m_mouse = Mouse::create();
		m_state = Normal;
		m_identifier = identifier;
		m_buttondown = false;
	}
	MenuButton::~MenuButton() {
	}

	void MenuButton::Update(helium::Vector2 mousePos) {
		if (m_mouse->is_button_down(EMouseButton::Left)) { // If the button is down
			if (m_hitbox.Contains(mousePos)) { // If the button is down and is within a buttons borders
				if (m_mouse->is_button_down_once(EMouseButton::Left)) { // It the mouse was clicked while within a buttons borders
					m_buttondown = true;
				}
			}
		}
		else if (m_hitbox.Contains(mousePos) && m_buttondown) { // If the button is not down but was clicked within the buttons borders and is still inside the buttons borders
			SetState(Pressed);
		}
		else if (m_hitbox.Contains(mousePos)) {
			SetState(Hover);
			m_buttondown = false; // The button is not pressed
		}
		else {
			SetState(Normal);
			m_buttondown = false; // Same as above
		}

		m_mouse->post_frame();
	}

	bool MenuButton::ButtonPressed() {
		return m_state == State::Pressed ? true : false;
	}

	std::string MenuButton::GetIdentifier() {
		return m_identifier;
	}

	void MenuButton::Draw() {
		m_texture->Draw();
		if (m_state == Hover) {
			m_hovertexture->Draw();
		}
	}
	void MenuButton::SetState(State state) {
		m_state = state;
	}
}