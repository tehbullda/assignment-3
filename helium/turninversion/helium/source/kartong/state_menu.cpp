// abstract_state.cpp

#include "stdafx.h"
#include <helium/gameplay/state_factory.hpp>
#include <helium/example/example_loading_state.hpp>
#include <helium/example/example_scene_state.hpp>
#include <helium/service_locator.hpp>
#include <helium/system/audio_system.hpp>
#include <helium/system/render_system.hpp>
#include <kartong/Network_system.hpp>
#include <kartong/state_menu.hpp>
#include <helium/os_event.hpp>
#include <helium/os_event_dispatcher.hpp>
#include <helium/scene/camera.hpp>


namespace helium {
	using namespace gameplay;
	using namespace kartong;

	static scene::Camera::Ptr camera;

	StateMenu::~StateMenu() {

	}

	void StateMenu::initialize() {
		m_render_system = ServiceLocator<system::RenderSystem>::get_service();
		m_audio_system = ServiceLocator<system::AudioSystem>::get_service();
		m_network_system = NetworkSystem::create("name");

		//m_network_system->Connect("217.208.15.170");

		m_Input = input::Input::Create();
		m_Input->XB_SetControllerScheme(input::XBOXKeyBindings::SCHEMES::NORMAL);
		m_Input->KBM_SetDefaultBinds();

		camera = scene::Camera::create();
		camera->set_perspective(Math::to_rad(45.0f), kartong::global::ResW / kartong::global::ResH, 0.5f, 100.0f);
		camera->move_elevate(10.0f);
		camera->update();

		int window_h = kartong::global::ResH;
		int height = 97;
		int padding = 5;
		
		m_buttons.push_back(new MenuButton("game", "menu/Button_Start.jpg", "menu/Selected_Indicator.png", Vector2(padding, window_h - (height * 4) - (padding * 4))));
		m_buttons.push_back(new MenuButton("game", "menu/Button_Join.png", "menu/Selected_Indicator.png", Vector2(padding, window_h - (height * 3) - (padding * 3))));
		m_buttons.push_back(new MenuButton("settings", "menu/Button_Settings.png", "menu/Selected_Indicator.png", Vector2(padding, window_h - (height * 2) - (padding * 2))));
		m_buttons.push_back(new MenuButton("quit", "menu/Button_Quit.png", "menu/Selected_Indicator.png", Vector2(padding, window_h - height - padding)));

		m_Logo = kartong::GUITexture::Create("menu/Logo.png", Vector2(padding, padding));

		m_mouse = Mouse::create();
		::ShowCursor(true);
	}

	void StateMenu::shutdown() {
		for (uint32_t i = 0; i < m_buttons.size(); i++) {
			delete m_buttons[i];
		}
	}

	bool StateMenu::update() {
		//m_network_system->Update();
		for (unsigned i = 0; i < m_buttons.size(); i++) {
			m_buttons[i]->Update(Vector2(m_mouse->get_x(), m_mouse->get_y()));
			if (m_buttons[i]->ButtonPressed()) {
				if (m_buttons[i]->GetIdentifier() == "game") {
					m_next = String::hash32("game", 4);
					return false;
				}
				else if (m_buttons[i]->GetIdentifier() == "quit"){
					::PostQuitMessage(WM_QUIT);
					//DestroyWindow(m_render_system->get_window());
					//m_next = 0;
					return false;
					//helium::
				}
			}
		}
		return true;
	}

	void StateMenu::draw() {
		m_render_system->clear();
		for (unsigned i = 0; i < m_buttons.size(); i++) {
			m_buttons[i]->Draw();
		}
		m_Logo->Draw();
		m_render_system->present();
	}

	uint32_t StateMenu::next() {
		return m_next;
	}
}