#include "stdafx.h"

//#include <Helium/XboxController.hpp>
//Controller360::Controller360()
//{
//	
//};
namespace kartong
{
	namespace input
	{
		XboxController::Ptr XboxController::Create(float p_dzX, float p_dzY)
		{
			return XboxController::Ptr(new XboxController(p_dzX, p_dzY));

		}
		int XboxController::GetPort()
		{
			return m_cId + 1;
		};
		XINPUT_GAMEPAD* XboxController::GetState()
		{
			return &state.Gamepad;
		};
		bool XboxController::CheckConnection()
		{
			int controllerId = -1;

			for (DWORD i = 0; i < XUSER_MAX_COUNT && controllerId == -1; i++)
			{
				XINPUT_STATE state;
				ZeroMemory(&state, sizeof(XINPUT_STATE));

				if (XInputGetState(i, &state) == ERROR_SUCCESS)
					controllerId = i;
			}
			m_cId = controllerId;
			return controllerId != -1;
		};
		bool XboxController::Refresh()
		{
			prev = state;
			if (m_cId == -1)
				CheckConnection();
			if (m_cId != -1)
			{
				ZeroMemory(&state, sizeof(XINPUT_STATE));
				if (XInputGetState(m_cId, &state) != ERROR_SUCCESS)
				{
					m_cId = -1;
					return false;
				}

				float normLX = helium::Math::max<float>(-1.0f, (float)state.Gamepad.sThumbLX / 32767);
				float normLY = helium::Math::max<float>(-1.0f, (float)state.Gamepad.sThumbLY / 32767);

				m_LeftStickX = (abs(normLX) < m_DeadZoneX ? 0 : (abs(normLX) - m_DeadZoneX) * (normLX / abs(normLX)));
				m_LeftStickY = (abs(normLY) < m_DeadZoneY ? 0 : (abs(normLY) - m_DeadZoneY) * (normLY / abs(normLY)));

				if (m_DeadZoneX > 0) m_LeftStickX *= 1 / (1 - m_DeadZoneX);
				if (m_DeadZoneY > 0) m_LeftStickY *= 1 / (1 - m_DeadZoneY);
				//Right
				float normRX = helium::Math::max<float>(-1.0f, (float)state.Gamepad.sThumbRX / 32767);
				float normRY = helium::Math::max<float>(-1.0f, (float)state.Gamepad.sThumbRY / 32767);

				m_RightStickX = (abs(normRX) < m_DeadZoneX ? 0 : (abs(normRX) - m_DeadZoneX) * (normRX / abs(normRX)));
				m_RightStickY = (abs(normRY) < m_DeadZoneY ? 0 : (abs(normRY) - m_DeadZoneY) * (normRY / abs(normRY)));

				if (m_DeadZoneX > 0) m_RightStickX *= 1 / (1 - m_DeadZoneX);
				if (m_DeadZoneY > 0) m_RightStickY *= 1 / (1 - m_DeadZoneY);
				//Triggers
				m_LeftTrigger = (float)state.Gamepad.bLeftTrigger / 255;
				m_RightTrigger = (float)state.Gamepad.bRightTrigger / 255;


				return true;
			}
			return false;

		};
		bool XboxController::IsPressed(WORD p_Button)
		{
			return (state.Gamepad.wButtons & p_Button) != 0;
		};
		bool XboxController::IsPressedOnce(WORD p_Button)
		{
			if (state.Gamepad.wButtons == p_Button && prev.Gamepad.wButtons != p_Button)
				return (state.Gamepad.wButtons & p_Button) != 0;
			return false;
		};
	};
};
//----XBOX-----PS3----
//0 - A     /    Cross
//1 - B     /   Circle
//2 - X     /   Square
//3 - Y     / Triangle
//4 - LB    /       L1
//5 - RB    /       R1
//6 - Back  /   Select
//7 - Start /    Start
//8 - LS    /       L3
//9 - RS    /       R3

//X - Left X axis
//Y - Left Y axis
//V - Unknown
//Z - Well, both L2 and R2 // need to use XInput to split the axises
//R - Right Y axis
//U - Right X axis
//PovX - D-Pad X axis
//PovY - D-Pad Y axis