

#include "stdafx.h"
#include <helium/gameplay/state_factory.hpp>
#include <helium/example/example_loading_state.hpp>
#include <helium/example/example_scene_state.hpp>
#include <helium/example/example_game.hpp>
#include <kartong/state_menu.hpp>
#include <kartong/state_game.hpp>
#include <helium/system/render_system.hpp>

#include <helium/service_locator.hpp>
#include <helium/system/audio_system.hpp>
#include <helium/system/collision_system.hpp>
#include <helium/system/render_system.hpp>
#include <kartong/network_system.hpp>

#include <helium/os_event.hpp>
#include <helium/os_event_dispatcher.hpp>
#include <helium/scene/camera.hpp>
#include <kartong/TextObject.hpp>



namespace helium
{
	using namespace gameplay;
	using namespace kartong;

	//static scene::Camera::Ptr camera;

	// public
	StateGame::~StateGame()
	{
		if (m_Input != nullptr)
		{
			delete m_Input;
			m_Input = nullptr;
		}
		if (camera != nullptr)
		{
			delete camera;
			camera = nullptr;
		}

		if (m_Player) {
			delete m_Player;
			m_Player = nullptr;
		}
		for (unsigned i = 0; i < m_buttons.size(); i++) {
			if (m_buttons[i]) {
				delete m_buttons[i];
				m_buttons[i] = nullptr;
			}
		}
		if (m_testtext) {
			delete m_testtext;
			m_testtext = nullptr;
		}
	}

	void StateGame::initialize()
	{
		//AbstractState::initialize();
		m_render_system = ServiceLocator<system::RenderSystem>::get_service();
		m_audio_system = ServiceLocator<system::AudioSystem>::get_service();
		m_CollisionManager = ServiceLocator<kartong::CollisionManager>::get_service();
		//m_network_system = kartong::NetworkSystem::create("Default");
		m_particle_system = kartong::ParticleSystem::create("../data/particles/");


		m_particle_system->CreateParticle("test.jpg", Vector3(0.0f, 10.0f, 0.0f), Vector3(5, 5, 5), 10.0f, 0.0f);

		m_mouse = helium::Mouse::create();

		m_buttons.push_back(new MenuButton("resume", "menu/Button_Resume.jpg", "menu/Selected_Indicator.png", Vector2(global::ResW / 2 - 230, 300)));
		m_buttons.push_back(new MenuButton("quit", "menu/Button_Quit.png", "menu/Selected_Indicator.png", Vector2(global::ResW / 2 - 230, 600)));


		m_audio_system->add_sound("../data/sound/music/backgroundmusicnew.wav", system::ChannelType::MUSIC, system::EAudioType::Stream);
		m_audio_system->play("../data/sound/music/backgroundmusicnew.wav", true);
		m_Input = new input::Input();
		m_Input->XB_SetControllerScheme(input::XBOXKeyBindings::SCHEMES::NORMAL);
		m_Input->KBM_SetDefaultBinds();

		//camera = scene::Camera::create();
		/*camera = new scene::Camera();
		camera->set_perspective(Math::to_rad(45.0f), kartong::global::ResW / kartong::global::ResH, 0.5f, 100.0f);*/
		/*camera->move_elevate(10.0f);
		camera->update();
		*/
		//m_Skybox = Skybox::Create("skyboox.png");
		m_Player = new PlayerObject(m_Input, 0);//PlayerObject::Create(m_Input, 0);
		m_Player->Initialize();
		m_Player->SetPosition(Vector3(0.0f, 5.0f, -70.0f));

		//m_Ground = new geometry::Ground("grass_stolen.png");
		//m_network_system->SetPlayerName(m_Player->GetName());
		m_next = String::hash32("menu", 4);
		//m_network_system->ConnectToServer("217.208.15.170");

		camera = m_Player->GetPlayerCamera();
		camera->update();


		::ShowCursor(false);


		//m_HudAmmo->Initialize();
		//m_HudAmmo->SetPosition(Vector2(300.0f, 300.0f));

		/*	m_font = resource::Font::create("../data/fonts/font.fontdesc.txt");
			resource::Texture::Ptr texture = resource::Texture::create("../data/fonts/font.png");
			m_font_stuff.texture = m_render_system->create_texture();
			m_font->set_texture(texture.get());*/

		m_testtext = new TextObject("../data/fonts/font.fontdesc.txt", "../fonts/font.png", Vector3(300, 200, 0));
		m_testtext->SetText("12 : 30");


		m_Level = Level::Create(camera);
		m_Level->Initialize();

		//m_CollisionManager->MakeAllDrawable();
		//m_CollisionManager->SetDrawAll();
		m_paused = m_muted = false;
	}
	bool StateGame::update()
	{

		if (GetFocus() != m_render_system->get_window()) {
			return true;
		}
		m_Input->Refresh();
		m_audio_system->process();
		if (m_Input->GetActionOnce(input::actions::PAUSE)) {
			m_paused = !m_paused;
			m_muted = !m_muted;
			m_audio_system->setmute(m_muted);
			::ShowCursor(m_paused);
			//m_audio_system->setpause(m_paused);
			//return false;
		}
		if (m_paused) {
			for (uint32_t i = 0; i < m_buttons.size(); i++) {
				m_buttons[i]->Update(Vector2(m_mouse->get_x(), m_mouse->get_y()));
				if (m_buttons[i]->ButtonPressed()) {
					if (m_buttons[i]->GetIdentifier() == "resume") {
						m_paused = false;
						::ShowCursor(m_paused);
					}
					else if (m_buttons[i]->GetIdentifier() == "quit") {
						return false;
					}
				}
			}
			return true;
		}

		static Time prev(Time::now());
		Time now = Time::now();
		Time delta = now - prev;
		prev = now;

		float deltatime = delta.as_seconds();
		////////////////////7

		m_Level->Update(deltatime);

		//GetWindowRect(helium::system::
		//ShowCursor(false);
		m_particle_system->Update(m_Player->GetPosition(), deltatime);
		//m_network_system->Update(deltatime);
		m_Player->Update(deltatime);
		/*if (m_Player->PlayerMoved()) {
			m_network_system->CreatePositionMessage(m_Player->GetPosition(), m_Player->GetDirection());
			}


			if (m_Player->PlayerMoved()) {
			m_network_system->UpdatePosition(m_Player->GetPosition());
			m_network_system->UpdateDirection(m_Player->GetDirection());
			}
			if (m_Player->PlayerShot()) {
			m_network_system->UpdateBulletDirection(m_Player->GetPlayerCamera()->get_forward());
			m_network_system->UpdateAction(input::actions::SHOOT);
			}
			for (uint32_t i = 0; i < (uint32_t)m_network_system->GetEnemyNumber(); i++) {
			if (m_network_system->EnemyShooting(i)) {
			m_audio_system->play_sound("../data/sound/weapons/SteamBurst PS02_84_2.wav");
			}
			}*/
		m_testtext->Update();

		m_Player->PostUpdate();

		//m_HudAmmo->Update(5, 5);

		/*if (m_enemies.size() != m_network_system->GetEnemyNumber()) {
			m_enemies.push_back(new EnemyObject(m_network_system->GetEnemyNumber(), m_network_system->GetEnemyName((uint32_t)m_enemies.size())));
			}*/
		//float sensetivity = 10.0f;
		//float mouseyaw = 0.022f;
		//float mousepitch = 0.022f;

		// update camera
		//if (mb)
		//{
		//	int x = mx - dx;
		//	int y = my - dy;
		//	if (x != 0 || y != 0)
		//	{
		//		camera->rotatey((float)x * deltatime * sensetivity * mouseyaw);
		//		camera->rotatex((float)y * deltatime * sensetivity * mousepitch);
		//	}
		//}
		//dx = mx; dy = my;

		//		float cameraspeed = 15.0f;
		//float rotatespeed = 5.0f;
		//float amnt1 = deltatime * cameraspeed;
		//float amnt2 = deltatime * rotatespeed;
		//if (m_Input->GetAction(input::actions::UP))
		//	camera->move_forward(amnt1);
		//if (m_Input->GetAction(input::actions::DOWN))
		//	camera->move_forward(-amnt1);
		//if (m_Input->GetAction(input::actions::LEFT))
		//	camera->move_sidestep(-amnt1);
		//if (m_Input->GetAction(input::actions::RIGHT))
		//	camera->move_sidestep(amnt1);
		//
		/*if (m_enemies.size() < m_network_system->GetEnemyNumber()) {
		}



		if (m_enemies.size() < m_network_system->GetEnemyNumber()) {
		m_enemies.push_back(new EnemyObject((int)m_enemies.size(), m_network_system->GetEnemyName((uint32_t)m_enemies.size())));

		Debug::write(EDebugLevel::Info, "Adding Enemy");
		}*/

		/*for (uint32_t i = 0; i < m_enemies.size(); i++) {
			m_enemies[i]->Update(deltatime);
			m_enemies[i]->SetPosition(m_network_system->GetEnemyPosition(i));
			m_enemies[i]->SetDirection(m_network_system->GetEnemyDir(i));
			}*/

		//if (m_Input->GetActionOnce(input::actions::PAUSE)) {
		//	m_paused = !m_paused;
		//	//m_audio_system->setpause(m_paused);
		//	//return false;
		//}

		rotarestuff += deltatime;
		//m_TestModel->SetRotation(Vector3(rotarestuff, rotarestuff, rotarestuff));

		m_Player->UpdateView();
		
		return true;
	};

	void StateGame::draw() {
		m_render_system->clear();


		//m_Ground->draw(camera->get_view(), camera->get_projection());


		//Last draws = skybox then UI
		if (m_paused) {
			for (uint32_t i = 0; i < m_buttons.size(); i++) {
				m_buttons[i]->Draw();
			}
		}
		else {
			m_Level->Draw();

			m_testtext->Draw();
			//m_particle_system->Draw(camera);
			m_CollisionManager->DrawAll(m_Player->m_PlayerCamera->get_view(), camera->get_projection());



			/*for (uint32_t i = 0; i < m_enemies.size(); i++) {
				m_enemies[i]->Draw(camera);
				}*/

			m_Player->Draw(camera);

			//m_HudAmmo->Draw();
		}




		camera->update();
		m_render_system->present();
	}

	void StateGame::shutdown()
	{
		if (m_Input != nullptr)
		{
			delete m_Input;
			m_Input = nullptr;
		}
		if (camera != nullptr)
		{
			delete camera;
			camera = nullptr;
		}

		if (m_Player) {
			delete m_Player;
			m_Player = nullptr;
		}
		for (unsigned i = 0; i < m_buttons.size(); i++) {
			if (m_buttons[i])
				delete m_buttons[i];
		}
		m_buttons.clear();
		if (m_testtext) {
			delete m_testtext;
			m_testtext = nullptr;
		}
		::ShowCursor(true);
		m_audio_system->stop();
		//AbstractState::shutdown();
	}

	uint32_t StateGame::next() {
		return m_next;
	}
}
