#include "stdafx.h"
#include "kartong/SolidPart.hpp"
#include <helium/service_locator.hpp>

using namespace kartong;
using namespace helium;

SolidPart::SolidPart(const std::string& p_Model, const Vector3 p_Pos)
{
	m_CollisionManager = ServiceLocator<kartong::CollisionManager>::get_service();
	m_Model = Model::Create(p_Model);
	m_Position = p_Pos;

	m_Bounds = m_CollisionManager->CreateNewBox(this, m_Position, Vector3(
		-m_Model->m_LowestPoints.m_x + m_Model->m_HighestPoints.m_x,
		-m_Model->m_LowestPoints.m_y + m_Model->m_HighestPoints.m_y,
		-m_Model->m_LowestPoints.m_z + m_Model->m_HighestPoints.m_z), 100.0f);


	m_Model->SetPosition(m_Position);
	m_Bounds->SetPosition(m_Position);
	
	

};
SolidPart::~SolidPart()
{

};

SolidPart::Ptr SolidPart::Create(const std::string& p_Model, Vector3 p_Pos)
{
	return SolidPart::Ptr(new SolidPart(p_Model, p_Pos));
};


void SolidPart::Initialize()
{

};

void SolidPart::Update(const float& p_Deltatime)
{
	p_Deltatime;
	obj.BS.m_center = m_Position;
};

void SolidPart::Draw(scene::Camera* p_Camera)
{
	m_Model->Draw(p_Camera);
};

//////////////////////

Arc::Arc(const Vector3& p_Pos, const bool& p_Rotated90)
{
	SetTag(("LevelPart"));
	SetName(("Arc"));
	SetType(ObjectType::ENVIRONMENT);
	SetPhysicsType(PhysicsType::STATIC);


	m_CollisionManager = ServiceLocator<kartong::CollisionManager>::get_service();
	m_Model = Model::Create("../data/models/arc.dae");
	m_Position = p_Pos;

	if (!p_Rotated90)
		m_Model->SetRotation(Vector3(Math::to_rad(90.0f), Math::to_rad(0.0f), 0.0f));
	else
		m_Model->SetRotation(Vector3(Math::to_rad(90.0f), Math::to_rad(90.0f), 0.0f));

	m_LPillar = m_CollisionManager->CreateNewBox(this, m_Position, Vector3(2.3f, 10.0f, 2.3f), 100.0f);
	m_RPillar = m_CollisionManager->CreateNewBox(this, m_Position, Vector3(2.3f, 10.0f, 2.3f), 100.0f);

	m_Model->SetPosition(m_Position);

	if (!p_Rotated90)
	{
		m_LPillar->SetPosition(Vector3(m_Position.m_x - 4.2f, m_Position.m_y - 1.5f, m_Position.m_z));
		m_RPillar->SetPosition(Vector3(m_Position.m_x + 4.2f, m_Position.m_y - 1.5f, m_Position.m_z));
	}
	else
	{
		m_LPillar->SetPosition(Vector3(m_Position.m_x , m_Position.m_y - 1.5f, m_Position.m_z - 4.2f));
		m_RPillar->SetPosition(Vector3(m_Position.m_x , m_Position.m_y - 1.5f, m_Position.m_z + 4.2f));
	}
	obj.BS.m_radius = 20.0f;
};
Arc::~Arc()
{

};

Arc::Ptr Arc::Create(const Vector3& p_Pos, const bool& p_Rotated90)
{
	return Arc::Ptr(new Arc(p_Pos, p_Rotated90));
};


void Arc::Initialize()
{

};

void Arc::Update(const float& p_Deltatime)
{
	obj.BS.m_center = m_Position;
	p_Deltatime;
};

void Arc::Draw(scene::Camera* p_Cam)
{
	m_Model->Draw(p_Cam);
};

////////////////////////

Stairs::Stairs(const Vector3& p_Pos)
{
	SetTag(("LevelPart"));
	SetName(("Stairs"));
	SetType(ObjectType::ENVIRONMENT);
	SetPhysicsType(PhysicsType::STATIC);

	m_Position = p_Pos;

	m_CollisionManager = ServiceLocator<kartong::CollisionManager>::get_service();

	

	m_Step1 = Cube::Create("whitetex.png", Vector3(6.0f, 1.0f, 1.2f));
	
	m_Step2 = Cube::Create("whitetex.png", Vector3(6.0f, 2.0f, 1.2f));
	
	m_Step3 = Cube::Create("whitetex.png", Vector3(6.0f, 3.0f, 1.2f));
	
	m_Step4 = Cube::Create("whitetex.png", Vector3(6.0f, 4.0f, 1.2f));
	
	m_Step5 = Cube::Create("whitetex.png", Vector3(6.0f, 5.0f, 1.2f));
	

	Vector3 pos1 = Vector3(m_Position.m_x + 0.0f, m_Position.m_y + (m_Step1->GetVolume().m_y / 2), m_Position.m_z + -1.2f);
	Vector3 pos2 = Vector3(m_Position.m_x + 0.0f, m_Position.m_y + (m_Step2->GetVolume().m_y / 2), m_Position.m_z + -2.4f);
	Vector3 pos3 = Vector3(m_Position.m_x + 0.0f, m_Position.m_y + (m_Step3->GetVolume().m_y / 2), m_Position.m_z + -3.6f);
	Vector3 pos4 = Vector3(m_Position.m_x + 0.0f, m_Position.m_y + (m_Step4->GetVolume().m_y / 2), m_Position.m_z + -4.8f);
	Vector3 pos5 = Vector3(m_Position.m_x + 0.0f, m_Position.m_y + (m_Step5->GetVolume().m_y / 2), m_Position.m_z + -6.0f);

	m_Step1->SetPosition(Vector3(pos1));
	m_Step2->SetPosition(Vector3(pos2));
	m_Step3->SetPosition(Vector3(pos3));
	m_Step4->SetPosition(Vector3(pos4));
	m_Step5->SetPosition(Vector3(pos5));

	m_Step1b = m_CollisionManager->CreateNewBox(this, pos1, Vector3(6.0f, 1.0f, 1.2f), 25.0f);
	m_Step2b = m_CollisionManager->CreateNewBox(this, pos2, Vector3(6.0f, 2.0f, 1.2f), 25.0f);
	m_Step3b = m_CollisionManager->CreateNewBox(this, pos3, Vector3(6.0f, 3.0f, 1.2f), 25.0f);
	m_Step4b = m_CollisionManager->CreateNewBox(this, pos4, Vector3(6.0f, 4.0f, 1.2f), 25.0f);
	m_Step5b = m_CollisionManager->CreateNewBox(this, pos5, Vector3(6.0f, 5.0f, 1.2f), 25.0f);



};
Stairs::~Stairs()
{

};

Stairs::Ptr Stairs::Create(const Vector3& p_Pos)
{
	return Stairs::Ptr(new Stairs(p_Pos));
};


void Stairs::Initialize()
{

};

void Stairs::Update(const float& p_Deltatime)
{
	p_Deltatime;
};

void Stairs::Draw(scene::Camera* p_Cam)
{
	m_Step1->Draw(p_Cam);
	m_Step2->Draw(p_Cam);
	m_Step3->Draw(p_Cam);
	m_Step4->Draw(p_Cam);
	m_Step5->Draw(p_Cam);
};