#pragma once

#include "stdafx.h"

using namespace kartong;

CollisionManager::CollisionManager()
{
	m_Drawing = false;
};
CollisionManager::~CollisionManager()
{

	auto it = m_Boxes.begin();
	while (it != m_Boxes.end())
	{


		if (*it != nullptr)
		{
			delete *it;
			*it = nullptr;
		}
		it++;
	}
	m_Boxes.clear();


};
CollisionManager::Ptr CollisionManager::Create()
{
	return CollisionManager::Ptr(new CollisionManager());
};

BoundingBox* CollisionManager::CreateNewBox(GameObject* p_Owner, Vector3 p_Pos, Vector3 p_Volume, float p_Range)
{
	BoundingBox* box = new BoundingBox(p_Owner);
	box->SetPosition(p_Pos);
	box->SetVolume(p_Volume);
	box->SetCheckRadius(p_Range);
	if (m_Drawing)
	{
		box->MakeDrawable();
		box->SetDraw();
	}
	m_Boxes.push_back(box);

	return m_Boxes.back();
};

void CollisionManager::CheckCollision(BoundingBox* m_Box)
{
	for (int i = 0; i < m_Boxes.size(); i++)
	{
		if (m_Box != m_Boxes.at(i))
		{
			m_Boxes.at(i)->SetCollided(m_Boxes.at(i)->CheckIntersection(*m_Box));
		}
	}
	m_Box->SetCollided(false);
};
bool CollisionManager::CheckHasCollided(BoundingBox* m_Box, BoundingBox** m_Collided)
{
	for (int i = 0; i < m_Boxes.size(); i++)
	{
		if (m_Box != m_Boxes.at(i))
		{
			if (m_Boxes.at(i)->HasCollided())
			{
				m_Boxes.at(i)->SetCollided(false);
				*m_Collided = m_Boxes.at(i);
				return true;
			}
		}
	}
	return false;
};
void CollisionManager::CheckCollision(Vector3 p_Pos, Vector3 p_Dir)
{
	for (int i = 0; i < m_Boxes.size(); i++)
	{
		if (m_Boxes.at(i)->m_Owner->GetType() != ObjectType::PLAYER)
		{
			if (m_Boxes.at(i)->CheckRayIntersection(p_Pos, p_Dir))
			{
				m_Boxes.at(i)->SetCollided(true);
			}
			else
				m_Boxes.at(i)->SetCollided(false);
		}
		else
			m_Boxes.at(i)->SetCollided(false);
	}
	float shortest = 100000.0f; //full�sning f�r att orkar inte
	float length = 0.0f;
	Vector3 boxpos;
	kartong::BoundingBox* closestBox = nullptr;
	for (int i = 0; i < m_Boxes.size(); i++)
	{
		if (m_Boxes.at(i)->HasCollided())
		{
			boxpos = m_Boxes.at(i)->GetPosition();
			/*Vector3 boxdiff = boxpos - p_Pos;
			length = boxdiff.length();*/
			length = Math::sqrt(
				((boxpos.m_x - p_Pos.m_x) * (boxpos.m_x - p_Pos.m_x)) +
				((boxpos.m_y - p_Pos.m_y) * (boxpos.m_y - p_Pos.m_y)) +
				((boxpos.m_z - p_Pos.m_z) * (boxpos.m_z - p_Pos.m_z)));

			//if (m_Boxes.at(i)->m_Owner->GetTag() == "Dummy")
				//Debug::write(EDebugLevel::Info, m_Boxes.at(i)->m_Owner->GetName() + ": " + std::to_string(length));

			if (length < shortest)
			{
				shortest = length;
				closestBox = m_Boxes.at(i);
			}
		}
	}
	if (closestBox)
	{
		closestBox->SetShot(true);
	}
	
	//for (int i = 0; i < m_Boxes.size(); i++)
	//{
	//	if (m_Boxes.at(i)->HasCollided())
	//	{
	//		
	//		length = Math::sqrt(
	//			((boxpos.m_x - p_Pos.m_x) * (boxpos.m_x - p_Pos.m_x)) +
	//			((boxpos.m_y - p_Pos.m_y) * (boxpos.m_y - p_Pos.m_y)) +
	//			((boxpos.m_z - p_Pos.m_z) * (boxpos.m_z - p_Pos.m_z)));


	//		//Debug::write(EDebugLevel::Info, std::to_string(length));
	//		if (length == shortest)
	//		{
	//			m_Boxes.at(i)->SetShot(true);
	//			return;
	//		}
	//	}
	//}
};

int CollisionManager::GetSize()
{
	return (int)m_Boxes.size();
};

void CollisionManager::MakeAllDrawable()
{
	for (int i = 0; i < m_Boxes.size(); i++)
	{
		m_Boxes.at(i)->MakeDrawable();
	}
};
void CollisionManager::SetDrawAll()
{
	for (int i = 0; i < m_Boxes.size(); i++)
	{
		m_Drawing = true;
		if (!m_Boxes.at(i)->IsDrawable())
			m_Boxes.at(i)->MakeDrawable();
		m_Boxes.at(i)->SetDraw();
	}
};
void CollisionManager::DisableDrawAll()
{
	for (int i = 0; i < m_Boxes.size(); i++)
	{
		m_Drawing = true;
		m_Boxes.at(i)->DisableDraw();
	}
};
void CollisionManager::DrawAll(Matrix4 p_view, Matrix4 p_projection)
{
	for (int i = 0; i < m_Boxes.size(); i++)
	{
		m_Boxes.at(i)->Draw(p_view, p_projection);
	}
};

bool CollisionManager::IsDrawingAll()
{
	return m_Drawing;
};

