#include "stdafx.h"
#include <kartong/EnemyObject.hpp>
#include <kartong/GameObject.hpp>

using namespace kartong;

EnemyObject::EnemyObject(const int &id)
{
	m_box = new Cube("grass_stolen.png", Vector3(5, 5, 5));
	SetTag("Enemy " + id);
	SetName("Enemy " + id);
	SetPhysicsType(KINETIC);
	SetType(ObjectType::PLAYER);
	m_Position = Vector3(10.0f, 50.0f, 10.0f);
	m_box->SetPosition(m_Position);
	m_Bounds = new BoundingBox(this);
	m_Bounds->SetVolume(Vector3(5, 5, 5));
	m_Bounds->SetPosition(Vector3(m_Position.m_x, m_Position.m_y / 2, m_Position.m_z));
	m_direction = Vector3(0, 0, 0);
};
EnemyObject::EnemyObject(const int &id, const std::string &name)
{
	m_box = new Cube("grass_stolen.png", Vector3(5, 5, 5));
	SetTag("Enemy " + id);
	SetName(name);
	SetPhysicsType(KINETIC);
	SetType(ObjectType::PLAYER);
	m_Position = Vector3(10.0f, 50.0f, 10.0f);
	m_box->SetPosition(m_Position);
	m_Bounds = new BoundingBox(this);
	m_Bounds->SetVolume(Vector3(5, 5, 5));
	m_Bounds->SetPosition(Vector3(m_Position.m_x, m_Position.m_y / 2, m_Position.m_z));
	m_direction = Vector3(0, 0, 0);
};
EnemyObject::~EnemyObject()
{
	if (m_box != nullptr) {
		delete m_box;
		m_box = nullptr;
	}
	if (m_Bounds != nullptr) {
		delete m_Bounds;
		m_Bounds = nullptr;
	}
};

void EnemyObject::Initialize()
{
};
void EnemyObject::Update(const float &p_DeltaTime)
{
	p_DeltaTime;
	m_Position += m_direction;
	m_box->SetPosition(m_Position);
	m_Bounds->SetPosition(m_Position);

	Debug::write(EDebugLevel::Info,
		std::to_string(m_Position.m_x) + " | " +
		std::to_string(m_Position.m_y) + " | " +
		std::to_string(m_Position.m_z));
};
void EnemyObject::Draw(helium::scene::Camera *camera)
{
	m_box->Draw(camera);
};

void EnemyObject::SetDirection(const Vector3 &dir) {
	m_direction = dir;
}
