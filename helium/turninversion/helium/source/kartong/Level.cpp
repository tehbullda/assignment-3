#include "stdafx.h"
#include "kartong/Level.hpp"
#include <kartong/GameObject.hpp>

using namespace kartong;

Level::Level(scene::Camera* p_Camera)
{
	m_Camera = p_Camera;
};
Level::~Level()
{

};
Level::Ptr Level::Create(scene::Camera* p_Camera)
{
	return Level::Ptr(new Level(p_Camera));
};

void Level::Initialize()
{
	//Skybox
	m_Skybox = Skybox::Create("skyboox.png");

	InitializeBasicGeometry();
	InitializeInteractives();
	InitializeExtras();

	//m_REMOVETHISLATER = Model::Create("Weapon.dae");
	//m_REMOVETHISLATER->SetPosition(Vector3(40.0f, 10.0f, 40.0f));
};

void Level::Update(const float& p_Deltatime)
{
	m_DeltaTime = p_Deltatime;
	UpdateBasicGeometry();
	UpdateInteractives();
	UpdateExtras();
};
void Level::Draw()
{
	m_Skybox->Draw(m_Camera->get_view(), m_Camera->get_projection());
	//m_Camera->update();

	//FRUSTSTRUI
	m_visibles.clear();

	// determine visibility 
	const Frustum& frustum = m_Camera->get_frustum();
	for (uint32_t i = 0; i < m_Objects.size(); i++)
	{
		

		//m_Objects[i]->force_transform_recalc();
		if (!frustum.is_inside(m_Objects[i]->getBoundingSphere()))
			continue;

		m_visibles.push_back(m_Objects[i]);
	}
	for (uint32_t i = 0; i < m_visibles.size(); i++)
	{
		//Debug::write(EDebugLevel::Info, m_visibles[i]->GetName());
		m_visibles[i]->Draw(m_Camera);
	}
	//for (uint32_t i = 0; i < m_Objects.size(); i++)
	//{
	//	m_Objects[i]->Draw(m_Camera);
	//}
	///////////////////////////

	
	//DrawBasicGeometry();
	DrawInteractives();
	//DrawExtras();

	//m_REMOVETHISLATER->Draw(m_Camera);
};


void Level::InitializeBasicGeometry()
{
	m_Ground = SolidCube::Create("red.png", Vector3(40.0f, 10.0f, 150.0f), Vector3(0.0f, 0.0f, 0.0f));
	m_Ground->SetName("Ground");
	m_Wall1 = SolidCube::Create("rajnbov.png", Vector3(2.0f, 20.0f, 150.0f), Vector3(21.0f, 10.0f, 0.0f));
	m_Wall1->SetName("Wall1");
	m_Wall2 = SolidCube::Create("rajnbov.png", Vector3(2.0f, 20.0f, 150.0f), Vector3(-21.0f, 10.0f, 0.0f));
	m_Wall2->SetName("Wall2");
	m_Wall3 = SolidCube::Create("rajnbov.png", Vector3(43.0f, 20.0f, 2.0f), Vector3(0.0f, 10.0f, 75.0f));
	m_Wall3->SetName("Wall3");
	m_Wall4 = SolidCube::Create("rajnbov.png", Vector3(43.0f, 20.0f, 2.0f), Vector3(0.0f, 10.0f, -75.0f));
	m_Wall4->SetName("Wall4");
	m_Deskthing = SolidCube::Create("whitetex.png", Vector3(40.0f, 3.0f, 3.0f), Vector3(0.0f, 5.0f, -60.0f));
	m_Pillar1 = SolidCube::Create("whitetex.png", Vector3(2.0f, 5.0f, 1.0f), Vector3(-15.0f, 7.5f, -30.0f));
	m_Pillar2 = SolidCube::Create("whitetex.png", Vector3(2.0f, 5.0f, 1.0f), Vector3(-7.0f, 7.5f, -30.0f));
	m_Pillar3 = SolidCube::Create("whitetex.png", Vector3(2.0f, 5.0f, 1.0f), Vector3(7.0f, 7.5f, -30.0f));
	m_Pillar4 = SolidCube::Create("whitetex.png", Vector3(2.0f, 5.0f, 1.0f), Vector3(15.0f, 7.5f, -30.0f));
	m_BackPillar = SolidCube::Create("whitetex.png", Vector3(5.0f, 5.0f, 1.0f), Vector3(0.0f, 11.5f, 30.0f));
	m_Arc_Top = Arc::Create(Vector3(-10.0f, 13.1f, -60.0f), false);
	m_Arc_Bot = Arc::Create(Vector3(10.0f, 13.1f, -60.0f), false);
	m_Arc_Left = Arc::Create(Vector3(-15.0f, 11.8f, 0.0f), false);
	m_Arc_Right = Arc::Create(Vector3(15.0f, 11.8f, 0.0f), false);
	m_Stairs = Stairs::Create(Vector3(-40.0f, 4.5f, -40.0f));

	m_Objects.push_back(m_Ground.get());
	m_Objects.push_back(m_Wall1.get());
	m_Objects.push_back(m_Wall2.get());
	m_Objects.push_back(m_Wall3.get());
	m_Objects.push_back(m_Wall4.get());
	m_Objects.push_back(m_Deskthing.get());
	m_Objects.push_back(m_Pillar1.get());
	m_Objects.push_back(m_Pillar2.get());
	m_Objects.push_back(m_Pillar3.get());
	m_Objects.push_back(m_Pillar4.get());
	m_Objects.push_back(m_BackPillar.get());
	m_Objects.push_back(m_Arc_Top.get());
	m_Objects.push_back(m_Arc_Bot.get());
	m_Objects.push_back(m_Arc_Left.get());
	m_Objects.push_back(m_Arc_Right.get());
	//m_Objects.push_back(m_Stairs.get());








	
};
void Level::InitializeInteractives()
{
	m_TestHPPack = HealthPack::Create();
	m_TestHPPack->SetPosition(Vector3(-15.0f, 7.0f, -65.0f));
	m_TestHPPack->Update(0.01f);
	m_Objects.push_back(m_TestHPPack.get());
	m_Targets = TargetManager::Create();
	m_Targets->Initialize();
};
void Level::InitializeExtras()
{

};
void Level::UpdateBasicGeometry()
{
	for (uint32_t i = 0; i < m_Objects.size(); i++)
	{
		m_Objects[i]->Update(m_DeltaTime);
	}
};
void Level::UpdateInteractives()
{
	m_TestHPPack->Update(m_DeltaTime);
	m_Targets->Update(m_DeltaTime);
};
void Level::UpdateExtras()
{

};
void Level::DrawBasicGeometry()
{
	m_Ground->Draw(m_Camera);
	m_Wall1->Draw(m_Camera);
	m_Wall2->Draw(m_Camera);
	m_Wall3->Draw(m_Camera);
	m_Wall4->Draw(m_Camera);
	m_Deskthing->Draw(m_Camera);
	m_Pillar1->Draw(m_Camera);
	m_Pillar2->Draw(m_Camera);
	m_Pillar3->Draw(m_Camera);
	m_Pillar4->Draw(m_Camera);
	m_BackPillar->Draw(m_Camera);
	m_Arc_Top->Draw(m_Camera);
	m_Arc_Bot->Draw(m_Camera);
	m_Arc_Left->Draw(m_Camera);
	m_Arc_Right->Draw(m_Camera);
	m_Stairs->Draw(m_Camera);
	
};
void Level::DrawInteractives()
{
	m_TestHPPack->Draw(m_Camera);
	m_Targets->Draw(m_Camera);

};
void Level::DrawExtras()
{

};

