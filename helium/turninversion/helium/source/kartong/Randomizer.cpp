#include "stdafx.h"
#include <kartong/Randomizer.hpp>
/*
	Kudos to Jonas Lundgren for this randomizer
*/
namespace Randomizer {
	std::random_device rd;
	std::mt19937 engine(rd());

	int Randomizer::GetRandomInt(int p_min, int p_max) {
		int min = p_min < p_max ? p_min : p_max;
		int max = p_min < p_max ? p_max : p_min;

		std::uniform_int_distribution<int> distr(min, max);
		return distr(Randomizer::engine);
	}

	int Randomizer::GetRandomIntFromWeights(std::initializer_list<double> p_weights) {
		std::discrete_distribution<int> distr(p_weights);
		return distr(Randomizer::engine);
	}

	double Randomizer::GetRandomDouble(double p_min, double p_max) {
		double min = p_min < p_max ? p_min : p_max;
		double max = p_min < p_max ? p_max : p_min;

		std::uniform_real_distribution<double> distr(min, max);
		return distr(Randomizer::engine);
	}

	double Randomizer::GetNormalDistribution(double p_mean, double p_deviation) {
		std::normal_distribution<double> distr(p_mean, p_deviation);
		return distr(Randomizer::engine);
	}
}