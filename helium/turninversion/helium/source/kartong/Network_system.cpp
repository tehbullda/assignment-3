#include "stdafx.h"
#include <kartong/Network_system.hpp>
#include <iostream>

#ifndef timebetweenpackages
#define timebetweenpackages 0.1f
#endif

#ifndef buffersize
#define buffersize 4096
#endif
namespace kartong {
	NetworkSystem::Ptr NetworkSystem::create(const std::string &playername) {
		return NetworkSystem::Ptr(new NetworkSystem(playername));
	}
	NetworkSystem::NetworkSystem(const std::string playername) {
		WSAStartup(MAKEWORD(2, 1), &m_data);
		m_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		m_serveraddress.sin_family = AF_INET;
		m_serveraddress.sin_port = htons(7777);
		memset(m_serveraddress.sin_zero, 0, sizeof(m_serveraddress.sin_zero));
		m_host = new EnemyPlayer;
		u_long i = 1;
		ioctlsocket(m_sock, FIONBIO, &i); //Set to nonblocking
		m_playeridentifier = playername;
		m_messagetimer = timebetweenpackages;
	}
	NetworkSystem::~NetworkSystem() {
		WSACleanup();
		for (unsigned i = 0; i < m_enemyplayers.size(); i++) {
			delete m_enemyplayers[i];
		}
		m_enemyplayers.clear();
		if (m_host) {
			delete m_host;
		}
	}

	void NetworkSystem::ConnectToServer(const std::string &addr) {
		m_serveraddress.sin_addr.S_un.S_addr = inet_addr(addr.c_str());
		Message msg;
		msg.m_type = MessageType::CONNECTED;
		msg.m_identifier = m_playeridentifier;
		msg.m_timestamp = 0;
		Send(GenerateMessage(msg), m_serveraddress);
	}

	void NetworkSystem::ConnectToServer(struct sockaddr_in *addr) {
		m_serveraddress = *addr;
		Message msg;
		msg.m_type = MessageType::CONNECTED;
		msg.m_identifier = m_playeridentifier;
		msg.m_timestamp = 0;
		Send(GenerateMessage(msg), m_serveraddress);
	}

	void NetworkSystem::ConnectToHost(struct sockaddr_in *addr) {
		m_host->m_addr = *addr;
		Message msg;
		msg.m_type = MessageType::CONNECTED;
		msg.m_identifier = m_playeridentifier;
		msg.m_timestamp = 0;
		Send(GenerateMessage(msg), m_host->m_addr);
	}

	void NetworkSystem::SetPlayerName(const std::string &name) {
		m_playeridentifier = name;
	}

	void NetworkSystem::CreatePositionMessage(const Vector3 &pos, const Vector3 &dir) {
		PositionMsg msg;
		msg.m_pos = pos;
		msg.m_dir = dir;
		msg.m_identifier = m_playeridentifier;
		msg.m_timestamp = Time::now();
		msg.m_type = MessageType::UPDATEPOS;
		m_messages.push_back(GeneratePositionMessage(msg));
		m_playermoved = false;
	}

	void NetworkSystem::CreateActionMessage(input::actions::ACTIONS action, const Vector3 &pos, const Vector3 &lookdir) {
		ActionMsg msg;
		msg.m_pos = pos;
		msg.m_lookdir = lookdir;
		msg.m_action = action;
		msg.m_identifier = m_playeridentifier;
		msg.m_timestamp = Time::now();
		msg.m_type = MessageType::UPDATEACTION;
		m_messages.push_back(GenerateActionMessage(msg));
		m_playeraction = input::actions::SIZE;
	}

	void NetworkSystem::CreateMsgMessage(const std::string &message) {
		MessageMsg msg;
		msg.m_message = message;
		msg.m_identifier = m_playeridentifier;
		msg.m_timestamp = Time::now();
		msg.m_type = MessageType::MESSAGE;
		m_messages.push_back(GenerateMessage(msg));
	}

	int NetworkSystem::GetEnemyNumber() {
		return m_numKnownConnections;
	}

	bool NetworkSystem::EnemyChanged(const uint32_t &index) {
		if (m_enemyplayers.size() > index) {
			return false;
		}
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			if (i == index) {
				return m_enemyplayers[i]->m_dirty;
			}
		}
		return false;
	}
	bool NetworkSystem::EnemyShooting(const uint32_t &index) {
		if (m_enemyplayers.size() > index) {
			return false;
		}
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			if (i == index) {
				return m_enemyplayers[i]->m_shooting;
			}
		}
		return false;
	}

	void NetworkSystem::EnemyUpdated(const uint32_t &index) {
		if (m_enemyplayers.size() > index) {
			return;
		}
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			if (i == index) {
				m_enemyplayers[i]->m_dirty = false;
			}
		}
	}

	Vector3 NetworkSystem::GetEnemyPosition(const uint32_t &index) {
		if (m_enemyplayers.size() > index) {
			return Vector3(0, 0, 0);
		}
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			if (i == index) {
				return m_enemyplayers[i]->m_pos;
			}
		}
		return Vector3(0, 0, 0);
	}

	Vector3 NetworkSystem::GetEnemyDir(const uint32_t &index) {
		if (m_enemyplayers.size() > index) {
			return Vector3(0, 0, 0);
		}
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			if (i == index) {
				return m_enemyplayers[i]->m_dir;
			}
		}
		return Vector3(0, 0, 0);
	}

	std::string NetworkSystem::GetEnemyName(const uint32_t &index) {
		if (m_enemyplayers.size() > index) {
			return "";
		}
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			if (i == index) {
				return m_enemyplayers[i]->m_name;
			}
		}
		return "";
	}

	void NetworkSystem::Update(const float &deltatime) {
		if (!m_connected) {
			return;
		}
		m_messagetimer -= deltatime;
		if (m_messagetimer <= 0.0f) {
			if (m_playermoved) {
				CreatePositionMessage(m_playerposition, m_playerdirection);
			}
			if (m_playeraction != input::actions::SIZE) {
				CreateActionMessage(m_playeraction, m_playerposition, m_playerbulletdir);
			}
			m_messagetimer = timebetweenpackages;
		}
		if (m_messages.size() > 0) {
			if (!m_host) {
				Send(m_messages.at(m_messages.size() - 1), m_host->m_addr);
			}
			else {
				for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
					Send(m_messages.at(m_messages.size() - 1), m_enemyplayers[i]->m_addr);
				}
			}
			m_messages.pop_back();
		}
		if (IncomingData()) {
			char buffer[buffersize] = { 0 };
			struct sockaddr_in tmpaddr;
			int size = sizeof(tmpaddr);
			int bytes = recvfrom(m_sock, buffer, sizeof(buffer)-1, MSG_PEEK, (struct sockaddr*)&tmpaddr, &size);
			if (bytes < 0) {
				//Debug::write(EDebugLevel::Error, "Socket error (receive)");
			}
			else if (bytes == 0) {
			}
			else {
				std::string sender = DetermineSender(buffer, tmpaddr);
				if (sender == "Server") {
					bytes = recvfrom(m_sock, buffer, sizeof(buffer)-1, MSG_PEEK, (struct sockaddr*)&tmpaddr, &size);
					HandleServerMessage(buffer);
				}
				else if (sender == "KnownClient") {
					HandleMessage(buffer, true);
				}
				else {
					HandleMessage(buffer, false);
				}
			}
		}
	}

	void NetworkSystem::UpdatePosition(const Vector3 &pos) {
		m_playerposition = pos;
		m_playermoved = true;
	}

	void NetworkSystem::UpdateDirection(const Vector3 &dir) {
		m_playerdirection = dir;
	}

	void NetworkSystem::UpdateBulletDirection(const Vector3 &dir) {
		m_playerbulletdir = dir;
	}

	void NetworkSystem::UpdateAction(const input::actions::ACTIONS action) {
		m_playeraction = action;
	}

	bool NetworkSystem::IncomingData() {
		unsigned long ret = 0;
		ioctlsocket(m_sock, FIONREAD, &ret);
		return ret > 0;
	}

	std::string NetworkSystem::DetermineSender(char *buffer, struct sockaddr_in addr) {
		std::stringstream ss(buffer);
		int type;
		std::string signature;
		ss >> type >> signature;

		if (m_serveraddress.sin_addr.S_un.S_addr == addr.sin_addr.S_un.S_addr) { // If the message comes from the servers address
			if (type == SERVER_ADDCLIENT || type == SERVER_MAKEHOST) { // And if the message type is server-related
				if (signature == "BulldawgServer") { // And the hardcoded name for the server is correct
					return "Server"; // We can relatively safely assume that the message is from the server
				}
			}
		}
		else if (m_host->m_addr.sin_addr.S_un.S_addr == addr.sin_addr.S_un.S_addr) {
			if (signature == m_host->m_name) {
				return "Host";
			}
		}
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			if (m_enemyplayers[i]->m_addr.sin_addr.S_un.S_addr == addr.sin_addr.S_un.S_addr) {
				if (m_enemyplayers[i]->m_name == signature) {
					return "KnownClient";
				}
			}
		}
		return "UnknownClient";
	}

	void NetworkSystem::PrintAddress(struct sockaddr_in *addr) {
		std::string address = (int)addr->sin_addr.S_un.S_un_b.s_b1 + ".";
		address += (int)addr->sin_addr.S_un.S_un_b.s_b2 + ".";
		address += (int)addr->sin_addr.S_un.S_un_b.s_b3 + "." + (int)addr->sin_addr.S_un.S_un_b.s_b4;
		Debug::write(EDebugLevel::Info, address);
	}

	void NetworkSystem::Send(const std::string &buffer, struct sockaddr_in address) {
		int bytes = sendto(m_sock, buffer.c_str(), (int)buffer.length(), 0, (struct sockaddr*)&address, sizeof(struct sockaddr));
		if (bytes == (int)buffer.length()) {
			m_connected = true;
		}
	}

	std::string NetworkSystem::GeneratePositionMessage(PositionMsg msg) {
		std::stringstream message;
		message << msg.m_type << " " << msg.m_identifier << " " << msg.m_timestamp.as_seconds()
			<< " " << msg.m_pos.m_x << " " << msg.m_pos.m_y << " " << msg.m_pos.m_z << " " << msg.m_dir.m_x << " " << msg.m_dir.m_y << " " << msg.m_dir.m_z;
		return message.str();
	}

	std::string NetworkSystem::GenerateActionMessage(ActionMsg msg) {
		std::stringstream message;
		message << msg.m_type << " " << msg.m_identifier << " " << msg.m_timestamp.as_seconds()
			<< " " << msg.m_pos.m_x << " " << msg.m_pos.m_y << " " << msg.m_pos.m_z << " " << msg.m_lookdir.m_x << " " << msg.m_lookdir.m_y << " " << msg.m_lookdir.m_z;
		return message.str();
	}

	std::string NetworkSystem::GenerateMsgMessage(MessageMsg msg) {
		std::stringstream message;
		message << msg.m_type << " " << msg.m_identifier << " " << msg.m_timestamp.as_seconds() << " " << msg.m_message;
		return message.str();
	}

	std::string NetworkSystem::GenerateMessage(Message msg) {
		std::stringstream message;
		message << msg.m_type << " " << msg.m_identifier << " " << msg.m_timestamp.as_seconds();
		return message.str();
	}
	//This is for the host telling the clients who are connected
	std::string NetworkSystem::GeneratePlayerList() {
		std::stringstream message;
		message << PLAYERLIST << " " << m_enemyplayers.size() << " ";
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			message << m_enemyplayers[i]->m_name << " ";
		}
		return message.str();
	}

	void NetworkSystem::AddEnemy(const std::string &name, bool dirty, Vector3 pos, bool shooting, Vector3 dir) {
		m_enemyplayers.push_back(new EnemyPlayer(name, dirty, pos, shooting, dir));
		m_numKnownConnections++;
	}

	void NetworkSystem::RemoveEnemy(const std::string &name) {
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			if (m_enemyplayers[i]->m_name == name) {
				m_enemyplayers.erase(m_enemyplayers.begin() + i);
				m_numKnownConnections--;
			}
		}
	}

	void NetworkSystem::SetEnemyData(const std::string &targetname, bool dirty, Vector3 pos, bool shooting, Vector3 dir) {
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			if (m_enemyplayers[i]->m_name == targetname) {
				m_enemyplayers[i]->m_dirty = dirty;
				m_enemyplayers[i]->m_pos = pos;
				m_enemyplayers[i]->m_shooting = shooting;
				m_enemyplayers[i]->m_dir = dir;
			}
		}
	}

	bool NetworkSystem::PlayerExists(const std::string &name) {
		if (m_playeridentifier == name) {
			return true;
		}
		for (uint32_t i = 0; i < m_enemyplayers.size(); i++) {
			if (m_enemyplayers[i]->m_name == name) {
				return true;
			}
		}
		return false;
	}

	void NetworkSystem::HandleMessage(const std::string &buffer, bool knownclient) {
		std::stringstream stream(buffer);
		uint32_t type, time;
		std::string name, message;
		Vector3 pos, dir;
		stream >> type >> name >> time;
		if (!knownclient) {
			switch (type) {
			case MessageType::CONNECTED:
				AddEnemy(name, true, Vector3(0, 0, 0), false, Vector3(0, 0, 0));
				break;
			case MessageType::DISCONNECTED:
				RemoveEnemy(name);
				break;
			case MessageType::MESSAGE:
				stream >> message;
				Debug::write(EDebugLevel::Info, name + " says: " + message);
				break;
			case MessageType::UPDATEACTION:
				stream >> pos.m_x >> pos.m_y >> pos.m_z;
				stream >> dir.m_x >> dir.m_y >> dir.m_z;
				if (!PlayerExists(name)) {
					AddEnemy(name, true, pos, true, dir); //If the connected message was dropped and the first message recieved was an action update
				}
				SetEnemyData(name, true, pos, true, dir);
				break;
			case MessageType::UPDATEPOS:
				stream >> pos.m_x >> pos.m_y >> pos.m_z;
				stream >> dir.m_x >> dir.m_y >> dir.m_z;
				if (!PlayerExists(name)) {
					AddEnemy(name, true, pos, false, dir); //Same as above
				}
				SetEnemyData(name, true, pos, false, dir);
				break;
			default:
				Debug::write(EDebugLevel::Info, "Bad message");
				break;
			}
		}
	}
	void NetworkSystem::HandleServerMessage(const std::string &buffer) {
		std::stringstream ss(buffer);
		int type;
		ss >> type;
		switch (type) {
		case SERVER_ADDCLIENT:
			break;
		case SERVER_MAKEHOST:
			break;
		}
	}
}