#include "stdafx.h"

#include <helium/service_locator.hpp>
//#include <helium/system/audio_system.hpp>
//#include <helium/system/collision_system.hpp>
#include <helium/system/render_system.hpp>
//#include <helium/gameplay/abstract_game.hpp>
//
//#include <helium/os_event.hpp>
//#include <helium/os_event_dispatcher.hpp>
//#include <helium/scene/camera.hpp>
//
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>


using namespace kartong::geometry;
using namespace kartong;

Ground::Ground(const std::string& p_texture)
{
	p_texture;
	SetTag(("Geometry"));
	SetName(("GroundPlane"));
	SetType(ObjectType::ENVIRONMENT);
	SetPhysicsType(PhysicsType::STATIC);

	m_render_system = helium::ServiceLocator<helium::system::RenderSystem>::get_service();
	m_CollisionManager = helium::ServiceLocator<kartong::CollisionManager>::get_service();
	
	m_Volume = Vector3(100.0f, 10.0f, 100.0f);
//	m_Bounds->SetPosition(Vector3(m_Position.m_x, m_Position.m_y - -5.0f, m_Position.m_z));

	m_Box = Cube::Create(p_texture, Vector3(m_Volume));
	//m_Box->SetVolume(m_Volume);
	m_Box->SetPosition(m_Position);


	m_Bounds = m_CollisionManager->CreateNewBox(this, m_Position, m_Volume, 200.0f);
	m_Bounds->SetPosition(Vector3(m_Position.m_x, m_Position.m_y/2, m_Position.m_z));
};

//Ground::~Ground()
//{
//	if (m_Bounds != nullptr)
//	{
//		delete m_Bounds;
//		m_Bounds = nullptr;
//	}
//};

void Ground::Draw(scene::Camera* p_Cam)
{
	m_Box->Draw(p_Cam);

};