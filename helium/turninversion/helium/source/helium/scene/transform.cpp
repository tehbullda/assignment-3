// transform.cpp

#include "stdafx.h"
#include <helium/scene/transform.hpp>

namespace helium
{
	namespace scene
	{
		Transform::Transform()
		{
			m_dirty = false;
			m_scale = Vector3(1.0f, 1.0f, 1.0f);
			m_transform.identity();
			recalculate();
		}

		Transform::Transform(const Transform& rhs)
		{
			m_transform = rhs.m_transform;
			m_position = rhs.m_position;
			m_scale = rhs.m_scale;
			m_rotation = rhs.m_rotation;
		}

		Transform& Transform::operator=(const Transform& rhs)
		{
			m_transform = rhs.m_transform;
			m_position = rhs.m_position;
			m_scale = rhs.m_scale;
			m_rotation = rhs.m_rotation;
			return *this;
		}

		void Transform::recalculate()
		{
			if (!m_dirty)
				return;
			m_dirty = false;

			Matrix4 t = Matrix4::translation(m_position);
			Matrix4 s = Matrix4::scale(m_scale);
			Matrix4 rx = Matrix4::rotation(Vector3(1.0f, 0.0f, 0.0f), m_rotation.m_x);
			Matrix4 ry = Matrix4::rotation(Vector3(0.0f, 1.0f, 0.0f), m_rotation.m_y);
			Matrix4 rz = Matrix4::rotation(Vector3(0.0f, 0.0f, 1.0f), m_rotation.m_z);

			// note(tommi): the order is important and might need fixin'
			m_transform = (rx * ry * rz) * s * t;
		}

		void Transform::set_position(const Vector3& position)
		{
			m_dirty = true;
			m_position = position;
		}

		void Transform::set_rotation(const Vector3& rotation)
		{
			m_dirty = true;
			m_rotation = rotation;
		}

		void Transform::set_scale(const float scale)
		{
			m_dirty = true;
			m_scale = Vector3(scale, scale, scale);
		}

		const Vector3& Transform::get_position() const
		{
			return m_position;
		}

		const Vector3& Transform::get_rotation() const
		{
			return m_rotation;
		}

		const Vector3& Transform::get_scale() const
		{
			return m_scale;
		}
		const Matrix4& Transform::get_transform() const
		{
			return m_transform;
		}
	}
}
