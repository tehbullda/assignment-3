// scene.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/scene/scene.hpp>

namespace helium
{
	namespace scene
	{
		// static 
		Scene::Ptr Scene::create()
		{
			return Scene::Ptr(new Scene);
		}

		// private
		Scene::Scene()
		{
			m_camera = nullptr;
			m_render_dispatcher = renderer::RenderDispatcher::create(
				ServiceLocator<system::RenderSystem>::get_service());
		}

		// public
		Scene::~Scene()
		{
		}

		void Scene::pre_render()
		{
			m_visibles.clear();

			// determine visibility 
			const Frustum& frustum = m_camera->get_frustum();
			for (uint32_t i = 0; i < m_nodes.size(); i++)
			{


				m_nodes[i]->force_transform_recalc();
				if (!frustum.is_inside( m_nodes[i]->get_bounding_volume() ))
					continue;

				m_visibles.push_back(m_nodes[i].get());
			}

			// sort nodes by shader and texture

		}

		void Scene::post_render()
		{
			// render the scene
			m_render_dispatcher->process();
		}

		void Scene::render()
		{
			// fill queue

			// add queue to dispatcher
			m_render_dispatcher->add_queue(&m_render_queue);
		}

		SceneNode* Scene::create_scene_node()
		{
			return nullptr;
		}

		void Scene::set_camera(Camera* camera)
		{
			m_camera = camera;
		}

		Camera* Scene::get_camera()
		{
			return m_camera;
		}
	}
}
