// lighting_property.cpp

#include "stdafx.h"
#include <helium/scene/lighting_property.hpp>

namespace helium
{
	namespace scene
	{
		// static
		LightingProperty::Ptr LightingProperty::create()
		{
			return LightingProperty::Ptr(new LightingProperty);
		}

		// private
		LightingProperty::LightingProperty()
			: m_ambient(0.2f, 0.2f, 0.2f)
			, m_diffuse(0.8f, 0.8f, 0.8f)
			, m_specular(0.5f, 0.5f, 0.5f)
		{
			m_power = 32.0f;
		}

		/*LightingProperty::LightingProperty(const LightingProperty& rhs)
		{
			m_ambient = rhs.m_ambient;
			m_diffuse = rhs.m_diffuse;
			m_specular = rhs.m_specular;
			m_power = rhs.m_power;
		}

		LightingProperty& LightingProperty::operator=(const LightingProperty& rhs)
		{
			m_ambient = rhs.m_ambient;
			m_diffuse = rhs.m_diffuse;
			m_specular = rhs.m_specular;
			m_power = rhs.m_power;
			return *this;
		}*/

		// public
		LightingProperty::~LightingProperty()
		{
		}

		const Vector3& LightingProperty::get_ambient() const
		{
			return m_ambient;
		}

		const Vector3& LightingProperty::get_diffuse() const
		{
			return m_diffuse;
		}

		const Vector3& LightingProperty::get_specular() const
		{
			return m_specular;
		}

		const float LightingProperty::get_specular_power() const
		{
			return m_power;
		}

		void LightingProperty::set_ambient(const Vector3& ambient)
		{
			m_ambient = ambient;
		}

		void LightingProperty::set_diffuse(const Vector3& diffuse)
		{
			m_diffuse = diffuse;
		}

		void LightingProperty::set_specular(const Vector3& specular)
		{
			m_specular = specular;
		}

		void LightingProperty::set_specular_power(float power)
		{
			m_power = power;
		}
	}
}
