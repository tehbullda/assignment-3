// abstract_state.cpp

#include "stdafx.h"
#include <helium/gameplay/abstract_state.hpp>

namespace helium
{
	namespace gameplay
	{
		AbstractState::~AbstractState()
		{
			m_start_time = Time::now();
			m_id_hash = 0;
		}
	}
}
