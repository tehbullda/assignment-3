// audio.cpp

#include "stdafx.h"
#include "stb_vorbis.h"
#include <helium/resource/audio.hpp>

namespace helium
{
	namespace resource
	{
		// static 
		Audio::Ptr Audio::create(const std::string& filename)
		{
			FILE* file = nullptr;
			file = fopen(filename.c_str(), "rb");
			if (!file)
			{
				Debug::write(EDebugLevel::Error, String::format("audio: could not find file %s", filename.c_str()));
				return Audio::Ptr(nullptr);
			}

			stb_vorbis* vorbis = stb_vorbis_open_file(file, 0, 0, 0);
			stb_vorbis_info info = stb_vorbis_get_info(vorbis);
			uint32_t size = info.channels * stb_vorbis_stream_length_in_samples(vorbis);
			int16_t* stream = new int16_t[size];
			uint32_t length = 0;
			while (length < size)
			{
				int bytes = stb_vorbis_get_samples_short_interleaved(vorbis, info.channels, stream + length, size - length);
				if (bytes == 0)
					break;
				length += bytes * info.channels;
			}
			stb_vorbis_close(vorbis);
			vorbis = nullptr;
			fclose(file);

			Debug::write(EDebugLevel::Info, String::format("audio: %s", filename.c_str()));

			Audio* audio = new Audio;
			audio->m_type = (info.channels == 1) ? EAudioType::Mono : EAudioType::Stereo;
			audio->m_size = size;
			audio->m_stream = stream;
			return Audio::Ptr(audio);
		}

		// private
		Audio::Audio()
		{
			m_type = EAudioType::Invalid;
			m_size = 0;
			m_stream = nullptr;
		}

		// public
		Audio::~Audio()
		{
			m_type = EAudioType::Invalid;
			m_size = 0;
			if (m_stream)
				delete[] m_stream;
			m_stream = nullptr;
		}

		bool Audio::is_valid() const
		{
			return m_stream != nullptr;
		}

		EAudioType Audio::get_type() const
		{
			return m_type;
		}

		uint32_t Audio::get_size() const
		{
			return m_size;
		}

		const void* Audio::get_stream() const
		{
			return m_stream;
		}
	}
}
