// resource_creator.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/audio_system.hpp>
#include <helium/system/render_system.hpp>
#include <helium/resource/resource_creator.hpp>
// resources
#include <helium/resource/audio.hpp>
#include <helium/resource/model.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>

// note(tommi): todo below
#include <helium/resource/font.hpp>

namespace helium
{
	namespace resource
	{
		Audio* ResourceCreator<Audio>::create(Audio* res)
		{
			// todo(tommi): todo
			(void)res;
			return res;
		}

		Model* ResourceCreator<Model>::create(Model* res)
		{
			// todo(tommi): todo
			(void)res;
			return res;
		}

		Sampler* ResourceCreator<Sampler>::create(Sampler* res)
		{
			system::RenderSystem* rensys = ServiceLocator<system::RenderSystem>::get_service();
			res->m_id = rensys->create_sampler_state(
				res->get_filter_mode(),
				res->get_addr_u(),
				res->get_addr_v(),
				res->get_addr_w());
			return res;
		}

		Shader* ResourceCreator<Shader>::create(Shader* res)
		{
			system::RenderSystem* rensys = ServiceLocator<system::RenderSystem>::get_service();
			res->m_id = rensys->create_shader(
				res->get_vertex_source().c_str(), 
				res->get_pixel_source().c_str());
			return res;
		}

		Texture* ResourceCreator<Texture>::create(Texture* res)
		{
			system::RenderSystem* rensys = ServiceLocator<system::RenderSystem>::get_service();
			res->m_id = rensys->create_texture(
				res->get_format(),
				res->get_type(),
				res->get_width(),
				res->get_height(),
				res->get_depth(),
				res->get_pixel_data());
			return res;
		}

		VertexBuffer* ResourceCreator<VertexBuffer>::create(VertexBuffer* res)
		{
			// todo(tommi): todo
			(void)res;
			return res;
		}

		IndexBuffer* ResourceCreator<IndexBuffer>::create(IndexBuffer* res)
		{
			// todo(tommi): todo
			(void)res;
			return res;
		}
	}
}
