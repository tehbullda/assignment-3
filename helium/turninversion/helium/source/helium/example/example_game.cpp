// example_game.cpp

#include "stdafx.h"
#include <helium/gameplay/state_factory.hpp>
#include <helium/example/example_loading_state.hpp>
#include <helium/example/example_scene_state.hpp>
#include <helium/example/example_game.hpp>
#include <kartong/state_menu.hpp>
#include <kartong/state_game.hpp>
#include <helium/system/render_system.hpp>

#include <helium/service_locator.hpp>
#include <helium/system/audio_system.hpp>
#include <helium/system/collision_system.hpp>
#include <helium/system/render_system.hpp>

#include <helium/os_event.hpp>
#include <helium/os_event_dispatcher.hpp>
#include <helium/scene/camera.hpp>

#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>

namespace helium
{
	using namespace gameplay;
	using namespace kartong;

	namespace example
	{

		static scene::Camera::Ptr camera;

		//static 
		AbstractGame::Ptr Game::create()
		{
			return AbstractGame::Ptr(new Game);
		}


		// private
		Game::Game()
		{
		}

		// public
		Game::~Game()
		{
			delete m_Input;
			m_Input = nullptr;
		}

		void Game::initialize()
		{
			AbstractGame::initialize();

			uint32_t string_hash[] =
			{
				String::hash32("menu", 4),
				String::hash32("game", 4),
			};

			gameplay::StateFactory* factory = m_state_manager->get_factory();
			factory->attach<StateMenu>(string_hash[0]);
			factory->attach<StateGame>(string_hash[1]);
			m_state_manager->set_state(string_hash[0]);

			m_collision_system = ServiceLocator<system::CollisionSystem>::get_service();
			m_render_system = ServiceLocator<system::RenderSystem>::get_service();
			m_audio_system = ServiceLocator<system::AudioSystem>::get_service();




			//m_Input = input::Input::Create();
			//m_Input->XB_SetControllerScheme(input::XBOXKeyBindings::SCHEMES::NORMAL);
			//m_Input->KBM_SetDefaultBinds();


			/*camera = scene::Camera::create();
			camera->set_perspective(Math::to_rad(45.0f), 1280.0f / 720.0f, 0.5f, 100.0f);
			camera->move_elevate(10.0f);
			camera->update();

			m_Skybox = Skybox::Create("bob.png");

			m_guitex = GUITexture::Create("gui.png", Vector2(200.0f, 200.0f));


			m_guitex = GUITexture::Create("gui.png", Vector2(200.0f, 200.0f));*/
		}
		void Game::process()
		{
			m_state_manager->update();
			m_state_manager->draw();
			//if (GetFocus() != m_render_system->get_window())
			//	return;

			//m_Input->Refresh();

			//static Time prev(Time::now());
			//Time now = Time::now();
			//Time delta = now - prev;
			//prev = now;

			//float deltatime = delta.as_seconds();
			////float sensetivity = 10.0f;
			////float mouseyaw = 0.022f;
			////float mousepitch = 0.022f;

			//// update camera
			////if (mb)
			////{
			////	int x = mx - dx;
			////	int y = my - dy;
			////	if (x != 0 || y != 0)
			////	{
			////		camera->rotatey((float)x * deltatime * sensetivity * mouseyaw);
			////		camera->rotatex((float)y * deltatime * sensetivity * mousepitch);
			////	}
			////}
			////dx = mx; dy = my;

			//float cameraspeed = 10.0f;
			//float amnt = deltatime * cameraspeed;
			//if (m_Input->GetAction(input::actions::UP))
			//	camera->move_forward(amnt);
			//if (m_Input->GetAction(input::actions::DOWN))
			//	camera->move_forward(-amnt);
			//if (m_Input->GetAction(input::actions::LEFT))
			//	camera->move_sidestep(-amnt);
			//if (m_Input->GetAction(input::actions::RIGHT))
			//	camera->move_sidestep(amnt);

			//if (m_Input->GetAction(input::actions::UP))
			//	camera->rotatex(0.01f);
			//if (m_Input->GetAction(input::actions::DOWN))
			//	camera->rotatex(-0.01f);
			//if (m_Input->GetAction(input::actions::LEFT))
			//	camera->rotatey(-0.01f);
			//if (m_Input->GetAction(input::actions::RIGHT))
			//	camera->rotatey(0.01f);


			///*helium::Debug::write(EDebugLevel::Info, std::to_string(camera->get_position().m_x));
			//camera->update();*/



			//if (m_Input->GetAction(input::actions::JUMP))
			//{
			//	m_guitex->SetPos(Vector2(300, 300));
			//	m_guitex->SetNewTexture("PstiKqw.png");
			//}
			//if (m_Input->GetActionOnce(input::actions::SHOOT))
			//	Debug::write(EDebugLevel::Info, "Shooting!");
			//if (m_Input->GetActionOnce(input::actions::CROUCH))
			//	Debug::write(EDebugLevel::Info, "Crouching!");
			//if (m_Input->GetActionOnce(input::actions::SIGHT))
			//	Debug::write(EDebugLevel::Info, "Aiming!");

			//camera->update();

			//m_render_system->clear();
			//m_Skybox->Draw(camera->get_view(), camera->get_projection());

			//m_guitex->Draw(camera->get_view(), camera->get_projection());
			//m_render_system->present();
		};

		void Game::shutdown()
		{
			AbstractGame::shutdown();
		}
	}
}
