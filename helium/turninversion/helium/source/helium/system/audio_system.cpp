// audio_system.cpp

#include "stdafx.h"
#include <fmod.hpp>
#include <fmod_errors.h>
#pragma comment(lib, "fmod_vc.lib")
#pragma comment(lib, "fmodL_vc.lib")
#include <helium/system/audio_system.hpp>

namespace helium
{
	namespace system
	{
		// struct
		struct AudioSystem::Sound
		{
			FMOD::Sound *m_sound;
			FMOD::Channel* m_channel;
			EAudioType m_type;
			bool m_looping;
		};

		// emitter
		AudioEmitter::AudioEmitter(uint32_t id)
		{
			m_id = id;
		}

		// listener
		AudioListener::AudioListener(uint32_t id)
		{
			m_id = id;
		}

		// static 
		AudioSystem::Ptr AudioSystem::create()
		{
			return AudioSystem::Ptr(new AudioSystem);
		}

		// private
		AudioSystem::AudioSystem()
		{
			FMOD_RESULT result = FMOD_OK;

			result = FMOD::System_Create(&m_system);
			if (result != FMOD_OK)
				Debug::fatal("audiosystem: could not create system");

			result = m_system->init(32, FMOD_INIT_NORMAL, NULL);
			if (result != FMOD_OK)
				Debug::fatal("audiosystem: could not initialize system");
			Debug::write(EDebugLevel::Info, "audiosystem: initialize");

			result = m_system->createChannelGroup("music", &m_music);
			if (result != FMOD_OK)
				Debug::fatal("audiosystem: could not create channel group music");
			Debug::write(EDebugLevel::Info, "  created channel group: music");
			result = m_music->setVolume(0.5f);

			result = m_system->createChannelGroup("effect", &m_effect);
			if (result != FMOD_OK)
				Debug::fatal("audiosystem: could not create channel group effect");
			Debug::write(EDebugLevel::Info, "  created channel group: effect");
			result = m_effect->setVolume(0.5f);

			result = m_system->getMasterChannelGroup(&m_master);
			if (result != FMOD_OK)
				Debug::fatal("audiosystem: could not obtain channel group master");
			Debug::write(EDebugLevel::Info, "  get channel group: master");
			result = m_master->addGroup(m_music);
			result = m_master->addGroup(m_effect);

			m_muted = false;
		}

		// public
		AudioSystem::~AudioSystem()
		{
			for (auto it = m_sounds.begin(); it != m_sounds.end(); it++)
			{
				it->second->m_sound->release();
				it->second->m_channel->stop();
				delete it->second;

			}
			m_sounds.clear();

			m_music->release();
			m_music = nullptr;

			m_effect->release();
			m_effect = nullptr;

			m_master->release();
			m_master = nullptr;

			m_system->release();
			m_system = nullptr;

			Debug::write(EDebugLevel::Info, "audiosystem: shutdown");
		}

		void AudioSystem::process()
		{
			for (auto it = m_sounds.begin(); it != m_sounds.end(); it++)
			{
				if (it->second->m_channel)
				{
					bool playing = false;
					it->second->m_channel->isPlaying(&playing);
					if (!playing && it->second->m_looping) {
						play(it->first, true);
					}
					else if (!playing)
						it->second->m_channel = nullptr;
				}
			}

			m_system->update();
		}

		void AudioSystem::add_sound(const std::string &name, ChannelType channeltype, EAudioType audiotype) {
			Sound *sound = new Sound;
			
			if (audiotype == EAudioType::Stream) {
				m_system->createStream(name.c_str(), FMOD_CREATESTREAM, 0, &sound->m_sound);
			}
			else {
				m_system->createSound(name.c_str(), FMOD_CREATESAMPLE, 0, &sound->m_sound);
			}
			switch (channeltype) {
			case MUSIC:
				sound->m_channel->setChannelGroup(m_music);
				break;
			case SOUND:
				sound->m_channel->setChannelGroup(m_effect);
				break;
			case MASTER:
				sound->m_channel->setChannelGroup(m_master);
				break;
			}
			sound->m_looping = false;
			add_sound(name, *sound);
		}

		void AudioSystem::play(const std::string &name, bool looping) {
			if (m_muted) {
				return;
			}
			for (auto it = m_sounds.begin(); it != m_sounds.end(); it++) {
				if (it->first == name) {
					FMOD::ChannelGroup *group;
					it->second->m_channel->getChannelGroup(&group);
					it->second->m_looping = looping;
					m_system->playSound(it->second->m_sound, group, true, &it->second->m_channel);
					it->second->m_channel->setVolume(0.5f);
					it->second->m_channel->setPaused(false);
				}
			}
		}

		void AudioSystem::setpause(bool paused) {
			for (auto it = m_sounds.begin(); it != m_sounds.end(); it++) {
				bool playing = false;
				it->second->m_channel->isPlaying(&playing);
				if (playing) {
					it->second->m_channel->setPaused(paused);
				}
			}
		}

		void AudioSystem::setmute(bool mute) {
			for (auto it = m_sounds.begin(); it != m_sounds.end(); it++) {
				//m_sounds[i].m_channel->setPaused(true);
				it->second->m_channel->setMute(mute);
				//m_sounds[i].m_channel->setPaused(false);

			}
			m_muted = mute;
		}

		void AudioSystem::stop() {
			for (auto it = m_sounds.begin(); it != m_sounds.end(); it++) {
				bool playing = false;
				it->second->m_channel->isPlaying(&playing);
				if (playing) {
					it->second->m_channel->stop();
				}
			}
		}

		// private
		int AudioSystem::add_sound(const std::string identifier, Sound& sound) {
			unsigned index = (uint32_t)-1;
			auto it = m_sounds.begin();
			for (it = m_sounds.begin(); it != m_sounds.end(); it++, index++) {
				if (!it->second->m_sound) {
					if (index != -1) {
						it->second = &sound;
						return index;
					}
				}
			}
			m_sounds.insert(std::make_pair(identifier, &sound));
			return (int)m_sounds.size() - 1;
		}
	}
}
